function main()
{
	const elems = getAJAXElemsWithID();
	const fieldGroups = [
		{ 
			"ДИСТОРСИЯ": [
				"XDistX0Y0",
				"XDistX0Y1",
				"XDistX1Y0",
				"XDistX2Y0",
				"XDistX1Y1",
				"XDistX0Y2",
				"XDistX3Y0",
				"XDistX2Y1",
				"XDistX1Y2",
				"XDistX0Y3",
				"YDistX0Y0",
				"YDistX1Y0",
				"YDistX0Y1",
				"YDistX2Y0",
				"YDistX1Y1",
				"YDistX0Y2",
				"YDistX3Y0",
				"YDistX2Y1",
				"YDistX1Y2",
				"YDistX0Y3",
			]
		},

		{
			"БИТЫЕ ПИКСЕЛИ": [
				"NumOfHP",
				"HPx_0",
				"HPy_0",
				"HPx_1",
				"HPy_1",
				"HPx_2",
				"HPy_2",
				"HPx_3",
				"HPy_3",
				"HPx_4",
				"HPy_4",
				"HPx_5",
				"HPy_5",
				"HPx_6",
				"HPy_6",
				"HPx_7",
				"HPy_7",
				"HPx_8",
				"HPy_8",
				"HPx_9",
				"HPy_9",
				"HPx_10",
				"HPy_10",
				"HPx_11",
				"HPy_11",
				"HPx_12",
				"HPy_12",
				"HPx_13",
				"HPy_13",
				"HPx_14",
				"HPy_14",
				"HPx_15",
				"HPy_15",
				"HPx_16",
				"HPy_16",
				"HPx_17",
				"HPy_17",
				"HPx_18",
				"HPy_18",
				"HPx_19",
				"HPy_19",
				"HPx_20",
				"HPy_20",
				"HPx_21",
				"HPy_21",
				"HPx_22",
				"HPy_22",
				"HPx_23",
				"HPy_23",
				"HPx_24",
				"HPy_24",
				"HPx_25",
				"HPy_25",
				"HPx_26",
				"HPy_26",
				"HPx_27",
				"HPy_27",
				"HPx_28",
				"HPy_28",
				"HPx_29",
				"HPy_29",
				"HPx_30",
				"HPy_30",
				"HPx_31",
				"HPy_31"
			]
		},

		{
			"БИТЫЕ ПОЛОСКИ": [
				"NumOfHL",
				"HLx_0",
				"HLx_1",
				"HLx_2",
				"HLx_3",
				"HLx_4",
				"HLx_5",
				"HLx_6",
				"HLx_7",
				"HLx_8",
				"HLx_9",
				"HLx_10",
				"HLx_11",
				"HLx_12",
				"HLx_13",
				"HLx_14",
				"HLx_15"
			]
		},

		{
			"НАСТРОЙКИ": [
				"StarDetectON",
				"FocalLength",
				"PixelStepH",
				"PixelStepV",
				"PixelError",
				"Indent",
				"FoundPorog",
				"FoundPorogFinal",
				"Width",
				"Height",
				"MaxStarsSpeed",
				"flash_CRC_copy",
				"flash_CRC_actual",
				"Stars_OFF_ADDR",
				"moon_level",
				"moon_limit",
				"Default_settings",
				"reprogram_flag",
				"sectors_copy",
				"Stars_Shift",
				"hcounter_cell_Addr",
				"vcounter_cell_Addr",
				"bin_cell_Addr",
				"state_cell_Addr",
				"error_counter_Addr",
				"timeout_counter_Addr",
				"StarsOutputType"
			]
		},

		{
			"НАСТРОЙКИ ПОИСКА ОБЪЕКТА": [
				"SearchON",
				"MinVal",
				"MaxVal",
				"MinSize",
				"MaxSize",
				"MinAV",
				"MaxAV",
				"MinY",
				"MaxY",
				"MinZ",
				"MaxZ"
			]
		},

		{
			"КАМЕРА": [
				"FlgFPU",
				"Al",
				"Ah",
				"Fl",
				"Fh",
				"Ml",
				"Mh",
				"Gna_Cdsa",
				"Gnb_Cdsb",
				"Blk",
				"Cpb",
				"Cpe",
				"cam_shutter_0",
				"cam_shutter_1",
				"cam_shutter_2",
				"cam_shutter_3",
				"cam_shutter_4",
				"cam_shutter_5",
				"cam_shutter_6",
				"cam_shutter_7",
				"cam_shutter_8",
				"cam_shutter_9",
				"cam_shutter_10",
				"cam_shutter_11",
				"cam_shutter_12",
				"cam_shutter_13",
				"cam_shutter_14",
				"cam_shutter_15"
			]
		},
		
		{
			"ДОПОЛНИТЕЛЬНЫЕ НАСТРОЙКИ КАМЕРЫ": [
				"compr_params_Addr",
				"dQ0",
				"dQ1",
				"dQ2",
				"dQ3",
				"ThC",
				"Height_cam",
				"Width_cam",
				"ITy",
				"TTy",
				"AcT",
				"FrT",
				"AcStep",
				"AddTrTime",
				"on_TLK",
				"PLL_delay_cell",
				"TM_cnixm_header_Fa_0",
				"TM_cnixm_header_Fa_1",
				"TM_cnixm_header_Fa_2",
				"TM_cnixm_header_Fa_3",
				"TM_cnixm_timer_Fa_int",
				"TM_cnixm_timer_Fa_float",
				"cam_al_copy",
				"cam_ah_copy",
				"cam_fl_copy",
				"cam_fh_copy",
				"copy_shutter_0",
				"copy_shutter_1",
				"copy_shutter_2",
				"copy_shutter_3",
				"copy_shutter_4",
				"copy_shutter_5",
				"copy_shutter_6",
				"copy_shutter_7",
				"copy_shutter_8",
				"copy_shutter_9",
				"copy_shutter_10",
				"copy_shutter_11",
				"copy_shutter_12",
				"copy_shutter_13",
				"copy_shutter_14",
				"copy_shutter_15"
			]
		}
	];

	let fieldsToGetValue = [];
	let tdsByFieldNames = {};

	fieldGroups.forEach((group) =>
	{
		const trRoot = document.createElement("tr");
		const tdRoot = document.createElement("td");
		trRoot.appendChild(tdRoot);
		elems.tbodyRoot.appendChild(trRoot);

		

		const createFieldsTable = (names) => 
		{
			const table = document.createElement("table");
			const tbody = document.createElement("tbody");
			table.appendChild(tbody);
			table.className = "zebra-table table-nobordered table-kv-pair";

			names.forEach((name) => 
			{
				const tr = document.createElement("tr");
				const tds = createElemsArray("td", 2);
				tds.forEach((td) => { tr.appendChild(td); });
				tbody.appendChild(tr);

				tds[0].textContent = sprintf("%s:", name);
				tdsByFieldNames[name] = tds[1];
				fieldsToGetValue.push(name);
			});

			return table;
		};

		if (Array.isArray(group)) {
			tdRoot.appendChild(createFieldsTable(group));
		} else {
			for (let groupName in group) {
				const details = document.createElement("details");
				const summary = document.createElement("summary");
				details.appendChild(summary);
				tdRoot.appendChild(details);

				summary.textContent = groupName;
				details.appendChild(createFieldsTable(group[groupName]));
			}
		}
	});


	function setTKPeriod(period)
	{
		if (!checkNumberValue(period))
			return;

		Pilv.can.setArrayTypePeriod(0xFA, period);
	}


	function updateButtonSetTKPeriodState()
	{
		elems.buttonSetTKPeriod.disabled = !checkNumberValue(textboxSetTKPeriod_value, false);
	}


	elems.buttonSetTKPeriod.onclick = (e) => 
	{
		setTKPeriod(textboxSetTKPeriod_value);
	};


	elems.textboxSetTKPeriod.oninput = (e) =>
	{
		textboxSetTKPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTKPeriodState();
	};


	elems.textboxSetTKPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;
	
		setTKPeriod(textboxSetTKPeriod_value);
	};


	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_fields_value", 
			{ names: ["DNUMBER", "FACTNUMBER", "FWVER0", "FWVER1"] }, (j) =>
		{
			elems.tdDNUMBER.textContent 	= j[0].raw !== null ? j[0].formatted : emptyValueString;
			elems.tdFACTNUMBER.textContent 	= j[1].raw !== null ? j[1].formatted : emptyValueString;

			if (j[2].raw !== null) {
				const v = j[2].raw;
				elems.tdFWVER0.textContent = 
					sprintf("%02d.%02d.%05d", Math.trunc(v / 10000000), Math.trunc((v % 10000000) / 100000), Math.trunc(v % 100000));
			} else {
				elems.tdFWVER0.textContent = emptyValueString;
			}

			if (j[3].raw !== null) {
				const v = j[3].raw;
				elems.tdFWVER1.textContent = 
					sprintf("%02d.%02d.%05d", Math.trunc(v / 10000000), Math.trunc((v % 10000000) / 100000), Math.trunc(v % 100000));
			} else {
				elems.tdFWVER1.textContent = emptyValueString;
			}
		});

		batch.pushRequest("zd.cmd.get_fields_value", { names: fieldsToGetValue }, (j) =>
		{
			j.forEach((v, i) =>
			{
				tdsByFieldNames[fieldsToGetValue[i]].textContent = v && v.raw !== null ? v.formatted : emptyValueString;
			});
		});

		batch.flush(() =>
		{
			this.continueTimeout(global.telemUpdateInterval);
		})
	});

	textboxSetTKPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTKPeriod);
	updateButtonSetTKPeriodState();
	elems.fieldsetSetTKPeriod.hidden = videoOnly;
	loop.run();
}