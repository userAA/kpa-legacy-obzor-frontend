function main()
{
	const elems = getAJAXElemsWithID();
	let textboxSetTPPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTPPeriod);

	function setTPPeriod(period)
	{
		if (!checkNumberValue(period, true))
			return;

		Pilv.can.setArrayTypePeriod(0xFF, period);
	}


	function updateButtonSetTPPeriodState()
	{
		elems.buttonSetTPPeriod.disabled = !checkNumberValue(textboxSetTPPeriod_value, false);
	}


	elems.textboxSetTPPeriod.oninput = (e) =>
	{
		textboxSetTPPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTPPeriodState();
	};

	elems.textboxSetTPPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTPPeriod(textboxSetTPPeriod_value);
	};

	elems.buttonSetTPPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTPPeriod_value, true))
			return;
			
		setTPPeriod(textboxSetTPPeriod_value);
	};

	const elemsByFieldNames = (() =>
	{
		let result = {};

		for (let key in elems) {
			if (key.substring(0, 2) == "td") {
				let fieldName = key.substring(2);
				result[fieldName] = elems[key];
			}
		}

		return result;
	})();

	const fieldNames = objectToKeyArray(elemsByFieldNames);

	const updateFieldsValues = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("zd.cmd.get_fields_value", { names: fieldNames }, (j) =>
		{
			for (let i = 0; i < j.length; ++i) {
				const fieldValue = j[i];
				const name = fieldNames[i];
				const elem = elemsByFieldNames[name];
				elem.textContent = fieldValue && fieldValue.raw !== null ? fieldValue.formatted : emptyValueString;
			}

			this.continueTimeout(global.telemUpdateInterval);
		});
	});

	updateButtonSetTPPeriodState();
	updateFieldsValues.run();
}