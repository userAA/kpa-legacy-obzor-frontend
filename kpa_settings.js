function main()
{
	const elems = getAJAXElemsWithID();
	let txSenders = []; // адреса КПА
	let txReceivers = []; // адреса ЗД
	let txSenderIndex = null;
	let txReceiverIndex = null;

	const txSendersRadios = (() =>
	{
		let result = [];
		const prefix = "radioTxSender";

		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const index = Number.parseInt(key.substring(prefix.length));
				result[index] = elems[key];
			}
		}

		return result;
	})();

	const txReceiversRadios = (() =>
	{
		let result = [];
		const prefix = "radioTxReceiver";

		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const index = Number.parseInt(key.substring(prefix.length));
				result[index] = elems[key];
			}
		}

		return result;
	})();

	const txSendersTextboxes = (() =>
	{
		let result = [];
		const prefix = "textboxTxSender";

		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const index = Number.parseInt(key.substring(prefix.length));
				result[index] = elems[key];
			}
		}

		return result;
	})();

	const txReceiversTextboxes = (() =>
	{
		let result = [];
		const prefix = "textboxTxReceiver";

		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const index = Number.parseInt(key.substring(prefix.length));
				result[index] = elems[key];
			}
		}

		return result;
	})();

	txSendersTextboxes.forEach((elem, i) => { txSenders[i] = textboxIntValueGuard(elem, 1, false); });
	txReceiversTextboxes.forEach((elem, i) => { txReceivers[i] = textboxIntValueGuard(elem, 1, false); });

	function updateAddressList(onComplete = null, onConnectError = null)
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_allowed_rx_senders", null, (j) => 
		{ 
			j.forEach((v, i) => { txReceivers[i] = v; }); 
		});

		batch.pushRequest("zd.cmd.get_allowed_rx_receivers", null, (j) => 
		{ 
			j.forEach((v, i) => { txSenders[i] = v; }); 
		});
		
		batch.pushRequest("zd.cmd.get_tx_sender", null, (j) =>
		{
			let index = txSenders.indexOf(j);

			if (index < 0) index = txSenders.indexOf(0);
			if (index < 0) index = 0;

			txSendersRadios[index].checked = true;
			txSenders[index] = j;
			txSenderIndex = index;
			txSendersTextboxes.forEach((elem, i) => { elem.value = txSenders[i].toString(); });
		});

		batch.pushRequest("zd.cmd.get_tx_receiver", null, (j) =>
		{
			let index = txReceivers.indexOf(j);

			if (index < 0) index = txReceivers.indexOf(0);
			if (index < 0) index = 0;
				
			txReceiversRadios[index].checked = true;
			txReceivers[index] = j;
			txReceiverIndex = index;
			txReceiversTextboxes.forEach((elem, i) => { elem.value = txReceivers[i].toString(); });
		});

		batch.flush(onComplete, onConnectError);
	}


	function updateButtonApplyTxSettingsState()
	{
		elems.buttonApplyTxSettings.disabled = 
			txSenderIndex === null 
			|| txReceiverIndex === null 
			|| !checkNumberValue(txSenders[txSenderIndex], false) 
			|| !checkNumberValue(txReceivers[txReceiverIndex], false); 
	}


	function textboxTxReceiverOnInput(e)
	{
		const index = txReceiversTextboxes.indexOf(e.target);
		txReceivers[index] = textboxIntValueGuard(e.target, 1, false);
		updateButtonApplyTxSettingsState();
	}


	function textboxTxSenderOnInput(e)
	{
		const index = txSendersTextboxes.indexOf(e.target);
		txSenders[index] = textboxIntValueGuard(e.target, 1, false);
		updateButtonApplyTxSettingsState();
	}


	function radioTxReceiverOnInput(e)
	{
		const i = txReceiversRadios.indexOf(e.target);
		txReceiverIndex = i;
		updateButtonApplyTxSettingsState();
	}


	function radioTxSenderOnInput(e)
	{
		const i = txSendersRadios.indexOf(e.target);
		txSenderIndex = i;
		updateButtonApplyTxSettingsState();
	}


	function sendTXSettings()
	{
		if (txSenderIndex === null 
			|| txReceiverIndex === null 
			|| !checkNumberValue(txSenders[txSenderIndex], true) 
			|| !checkNumberValue(txReceivers[txReceiverIndex], true)
		) return;

		const batch = new JsonRPCBatch();
		batch.pushRequest("zd.cmd.set_tx_sender", [ txSenders[txSenderIndex] ]);
		batch.pushRequest("zd.cmd.set_tx_receiver", [ txReceivers[txReceiverIndex] ]);
		batch.flush();
	}

	
	elems.buttonApplyTxSettings.onclick = (e) => { sendTXSettings(); };
	elems.buttonClearCache.onclick = (e) => { sendJsonRPCRequest("clear_cache"); };
	
	
	elems.buttonSetFactorySettings.onclick = (e) => 
	{
		sendJsonRPCRequest("pilv.can.set_factory_settings"); 
	};


	function textboxesTxOnKeyUp(e)
	{
		if (e.key == "Enter") {
			sendTXSettings();
		}
	}


	txSendersTextboxes.forEach((elem) => 
	{
		elem.oninput = textboxTxSenderOnInput;
		elem.onkeyup = textboxesTxOnKeyUp;
	});


	txReceiversTextboxes.forEach((elem) => 
	{
		elem.oninput = textboxTxReceiverOnInput;
		elem.onkeyup = textboxesTxOnKeyUp;
	});

	const getVersion = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("zd.get_board_version", null, (j) =>
		{
			if (j !== null)
				elems.labelZDBoardVersion.textContent = sprintf("%02d.%02d.%d", j.year, j.month, j.version);

			this.continueTimeout(1000);
		});
	});

	const ipConfig = {
		ip: "",
		ipSplitted: [0, 0, 0, 0],
		subnetMask: 0,
		gateway: "",
		isDHCPEnabled: false
	};
	
	function updateButtonApplyIPConfigState()
	{
		let isCorrect = true;

		for (let i = 0; i < 4; i++) {
			if (!checkNumberValue(ipConfig.ipSplitted[i])) {
					isCorrect = false;
				break;
			}
		}

		isCorrect &= checkNumberValue(ipConfig.subnetMask);
		elems.buttonApplyIPConfig.disabled = !isCorrect && !ipConfig.isDHCPEnabled;
	}

	
	const ipTextboxes = (() =>
	{
		result = [];
		
		for (let i = 0; i < 4; i++) {
			const elem = elems[sprintf("textboxIP%d", i)];

			elem.oninput = e =>
			{
				if (!ipConfig.hasOwnProperty("ip"))
					return;

				ipConfig.ipSplitted[i] = textboxIntValueGuard(e.target, 1, false);
				updateButtonApplyIPConfigState();
			};

			result.push(elem);
		}

		return result;
	})();

	elems.textboxSubnetMask.oninput = e =>
	{
		ipConfig.subnetMask = textboxIntValueGuard(e.target, 1, false, null, 31, 0);
		updateButtonApplyIPConfigState();
	};


	elems.checkboxDHCP.oninput = e =>
	{
		if (!ipConfig.hasOwnProperty("isDHCPEnabled"))
			return;

		ipConfig.isDHCPEnabled = e.target.checked;
		elems.buttonApplyIPConfig.disabled = !ipConfig.isDHCPEnabled;

		for (let i = 0; i < 4; i++) {
			ipTextboxes[i].disabled = ipConfig.isDHCPEnabled;
			elems.textboxSubnetMask.disabled = ipConfig.isDHCPEnabled;
		}

		updateButtonApplyIPConfigState();
	};


	sendHttpGetRequest(`http://${location.host}:8999/__get_ep`, (response) => 
	{
		const j = JSON.parse(response);
		const hostname = j[1];
		ipConfig.ip = j[0].ip;
		ipConfig.isDHCPEnabled = j[0].isDHCPEnabled;
		ipConfig.subnetMask = j[0].subnetMask;
		ipConfig.gateway = j[0].gateway;
		ipConfig.name = j[0].name;
	
		elems.labelHostname.textContent = hostname;
		elems.labelIP.textContent = `${ipConfig.ip}/${ipConfig.subnetMask}`;
		elems.checkboxDHCP.checked = ipConfig.isDHCPEnabled;

		ipConfig.ip
			.split(".")
			.forEach((v, i) => 
			{ 
				ipTextboxes[i].value = v; 
				ipTextboxes[i].disabled = ipConfig.isDHCPEnabled;
				ipConfig.ipSplitted[i] = textboxIntValueGuard(ipTextboxes[i], 1); 
			});

		elems.textboxSubnetMask.value = ipConfig.subnetMask.toString();
		updateButtonApplyIPConfigState();
		//ipChecker.run();
	});

	elems.buttonApplyIPConfig.onclick = e =>
	{
		ipConfig.ip = sprintf("%d.%d.%d.%d", 
			ipConfig.ipSplitted[0], 
			ipConfig.ipSplitted[1], 
			ipConfig.ipSplitted[2], 
			ipConfig.ipSplitted[3]);

		disableConnectionErrorPopups();
		elems.buttonApplyIPConfig.disabled = true;

		sendHttpPostRequest(`http://${location.host}:8999/__set_ep`, JSON.stringify({ 
			ip: ipConfig.ip, 
			isDHCPEnabled: ipConfig.isDHCPEnabled,
			subnetMask: ipConfig.subnetMask,
			gateway: "0.0.0.0",
			name: ipConfig.name
		}), response =>
		{
			setTimeout(() => {
				enableConnectionErrorPopups();
				elems.buttonApplyIPConfig.disabled = false;
				AJAXLoadPage("kpa_settings.html");
			}, 5000);
		}, null, () =>
		{
			setTimeout(() => {
				elems.buttonApplyIPConfig.disabled = false;
				enableConnectionErrorPopups();
				AJAXLoadPage("kpa_settings.html");
			}, 5000);
		});
	};

	elems.fileIZN_KALIB.onchange = (e) =>
	{
		//let guardResult = inputFileMaxSizeGuard(e.target, kpaFileMaxSize);
		elems.buttonFileIZN_KALIB.disabled = false;
		//if (guardResult === null) 
		//	return;

		//elems.buttonFileIZN_KALIB.textContent = "Обновить";
	};

	elems.buttonFileIZN_KALIB.onclick = e => {
		if (elems.fileIZN_KALIB.files.length == 0) {
			Popup.showInformation("Ошибка!", "Файл не был выбран.");
			return;
		}

		var fr = new FileReader();
		fr.onloadend = (e) =>
		{
			if(e.target.readyState != FileReader.DONE) {
				switch (e.target.readyState) {
					case FileReader.EMPTY:
						Popup.showInformation("Ошибка!", "Выбран пустой файл.");
					break;
				}

				return;
			}

			sendJsonRPCRequest("izn.prepare_kalib_upload", null, (j) => 
			{
				sendHttpPostRequest(global.cmdEP, e.target.result, (r) =>
				{
					//sendJsonRPCRequest("zd.update.get_validation_status", null, (j) =>
					//{
					//	if (j) {
					//		onDeviceFileLoad();
					//	} else {
					//		Popup.showInformation("Ошибка обновления.", "Неизвестная ошибка...");
					//	}
					//});
					// todo: проверить, началось ли обновление...
				}, // !function(r)

				[new HttpHeader(j.http_header_name, j.http_header_value)], null, 

				(e) =>
				{
					//if (!e.lengthComputable || e.total == 0) 
					//	return;

					//var value = e.total / e.loaded;
					//setDeviceLoadingProgressSmooth(value);
				});
			});
		}; // !onloadend...

		let files = elems.fileIZN_KALIB.files;
		fr.readAsArrayBuffer(files[0]);
		showProgressDevice();
	};

	{
		const batch = new JsonRPCBatch();
		batch.pushRequest("is_kpa_log_available", null, j => elems.buttonDownloadKPALog.disabled = j);
		batch.pushRequest("is_can_log_available", null, j => elems.buttonDownloadCANLog.disabled = j);
		batch.flush();
	}


	elems.formDownloadIZN_KALIB.action = "KALIB.IZN";

	elems.formDownloadKPALog.action = `${global.cmdEP}/kpa.log`;
	elems.formDownloadCANLog.action = `${global.cmdEP}/can.log`;

	txSendersRadios.forEach((elem) => { elem.oninput = radioTxSenderOnInput; });
	txReceiversRadios.forEach((elem) => { elem.oninput = radioTxReceiverOnInput; });
	getVersion.run();
	updateAddressList();
}