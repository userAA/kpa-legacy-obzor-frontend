function main()
{
	const elems = getAJAXElemsWithID();
	const telemName = "priorityInfo";
	let textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);

	function setTelemPeriod(period)
	{
		if (!checkNumberValue(period, true))
			return;

		Pilv.mko.setTelemPeriod(telemName, period);
	}


	function updateButtonSetTelemPeriodState()
	{
		elems.buttonSetTelemPeriod.disabled = !checkNumberValue(textboxSetTelemPeriod_value, false);
	}


	elems.textboxSetTelemPeriod.oninput = (e) =>
	{
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTelemPeriodState();
	};

	elems.textboxSetTelemPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	elems.buttonSetTelemPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTelemPeriod_value, true))
			return;
			
		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	const elemsByFieldNames = (() =>
	{
		let result = {};

		for (let key in elems) {
			if (key.substring(0, 2) == "td") {
				let fieldName = key.substring(2);
				result[fieldName] = elems[key];
			}
		}

		return result;
	})();

	const fieldValues = {};

	function updateButtonApplyValuesState()
	{
		let success = true;

		for (let key in fieldValues) {
			if (fieldValues[key] === null) {
				success = false;
				break;
			}	
		}

		elems.buttonApplyValues.disabled = !success;
	}

	for (let key in elemsByFieldNames) {
		const textbox = elems[`textbox${key}`]; 
		fieldValues[key] = textboxFloatValueGuard(textbox);

		sendJsonRPCRequest("zd.cmd.get_field_info", { name: key }, j => {
			textbox.oninput = ({ currentTarget }) => {
				fieldValues[key] = textboxIntValueGuard(currentTarget, j.raw_size * 2);
				updateButtonApplyValuesState();
			};
		});
		// elemsByFieldNames[key].oninput = ({ currentTarget }) => {
		// };
	}

	updateButtonApplyValuesState();

	const fieldNames = objectToKeyArray(elemsByFieldNames);

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_fields_value", { names: fieldNames }, (j) =>
		{
			for (let i = 0; i < j.length; ++i) {
				const fieldValue = j[i];
				const name = fieldNames[i];
				const elem = elemsByFieldNames[name];
				elem.textContent = fieldValue === null ? "[Данного поля нет в базе]" : fieldValue.raw !== null ? `${fieldValue.formatted} (${fieldValue.raw})` : emptyValueString;
			}
		});

		batch.pushRequest("zd.cmd.mko.is_telem_enabled", [telemName], j => {
			elems.checkboxTelem.checked = j;
			elems.buttonSetTelemPeriod.disabled = !(j && checkNumberValue(textboxSetTelemPeriod_value));
			elems.textboxSetTelemPeriod.disabled = !j;
		});

		const cont = () => this.continueTimeout(global.telemUpdateInterval);
		batch.flush(cont, cont);
	});

	Pilv.mko.getTelemPeriod(telemName, period => {
		const seconds = period / 1000;
		elems.textboxSetTelemPeriod.value = seconds.toString();
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);
	});

	elems.checkboxTelem.oninput = e => (e.currentTarget.checked ? Pilv.mko.enableTelem : Pilv.mko.disableTelem)(telemName);

	elems.buttonApplyValues.onclick = e => {

	};

	updateButtonSetTelemPeriodState();
	loop.run();

	elems.buttonApplyValues.onclick = ({ currentTarget }) => {
		const batch = new JsonRPCBatch();
		const en = () => currentTarget.disabled = false;

		currentTarget.disabled = true;

		for (let key in fieldValues) {
			batch.pushRequest("zd.cmd.set_field_value", { name: key, value: fieldValues[key] });
		}

		batch.flush(en, en);
	};
}