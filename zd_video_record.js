function main()
{
	class Rect extends Checkable {
		constructor(x = 0, y = 0, w = 0, h = 0) 
		{
			super();
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}
	};

	const elems = getAJAXElemsWithID();
	const clienIDString = clientIDInt.toString();	

	const effectSlidersMouseState = {
		brightness: false, 
		contrast: false,
		gamma: false
	};

	let videoIsPlaying = true;
	let isVideoLoaded = false;
	let isCaptureRunning = null;
	let isTiffCompressionEnabled = null;
	let videoIP = null;
	let tiffPagesPerFile = textboxIntValueGuard(elems.textboxTiffPagesPerFile, 4, false);
	let frameSkip = textboxIntValueGuard(elems.textboxFrameSkipCount, 4);
	let frameContinuosly = textboxIntValueGuard(elems.textboxFrameСontinuouslyСount, 4);
	let uiImagePosition = new Vector2();
	let currentWeightSensorBounds = new Rect();
	let weightSensorBoundsToSet = new Rect();
	let weightBoundToSet = 0;
	let currentWeightBound = 0;
	const zdFrameSize = new Vector2(1704, 1264);
	let FlgFPU = null;

	
	//let weight = 0;


	elems.textboxFrameSkipCount.oninput = (e) =>
	{
		frameSkip = textboxIntValueGuard(e.target, 4);
		elems.buttonStartStopCapture.disabled = !checkNumberValue(frameSkip, false) || !checkNumberValue(frameContinuosly, false);
	};

	elems.textboxFrameСontinuouslyСount.oninput = (e) =>
	{
		frameContinuosly = textboxIntValueGuard(e.target, 4);
		elems.buttonStartStopCapture.disabled = !checkNumberValue(frameSkip, false) || !checkNumberValue(frameContinuosly, false);
	};

	const videoTryLoad = new AsyncLoopFunc(function()
	{
		const targetFrameSize = new Vector2(640, 480);
		let frameSize = new Vector2();
		const batch = new JsonRPCBatch();
		batch.pushRequest("video.disable_frame_size_as_a_source");
		batch.pushRequest("video.set_frame_size", targetFrameSize, (j) => 
		{
			frameSize = j;

			elems.video.width = frameSize.x;
			elems.video.height = frameSize.y;
		});

		batch.pushRequest("video.get_server_ip", null, (j) =>
		{
			j = j.substring(j.search(":") + 1);
			
			if (videoIP != j) {
				videoIP = j;
				elems.video.src = `http://${window.location.hostname}:${videoIP}/?id=${clienIDString}`;
			}
		});

		batch.flush();
		this.continueTimeout(1000);
	});

	sendJsonRPCRequest("zd.fillNewFrameByPrevEnabled", null, j => {
		elems.checkboxFillFrame.checked = j;
	});

	elems.checkboxFillFrame.oninput = e => {
		sendJsonRPCRequest("zd.fillNewFrameByPrev", [e.target.checked]);
	};


	function updateButtonTiffPagesPerFileState()
	{
		elems.buttonTiffPagesPerFile.disabled = !checkNumberValue(tiffPagesPerFile);
	}


	function updateButtonSetFlgFPUState()
	{
		elems.buttonSetFlgFPU.disabled = !checkNumberValue(FlgFPU);
	}
	

	function extractFlgFPU()
	{
		let FlgFPU_54 = 0;

		if (elems.radioFlgFPU_54_0.checked)
			FlgFPU_54 = 0;
		else if (elems.radioFlgFPU_54_1.checked)
			FlgFPU_54 = 1;
		else if (elems.radioFlgFPU_54_2.checked)
			FlgFPU_54 = 2;
		else if (elems.radioFlgFPU_54_3.checked)
			FlgFPU_54 = 3;

		FlgFPU = elems.checkboxFlgFPU_2.checked << 2 | FlgFPU_54 << 4 | 1 << 3;
		updateButtonSetFlgFPUState();
	}


	elems.buttonSetFlgFPU.onclick = e =>
	{
		if (!checkNumberValue(FlgFPU))
			return;

		sendJsonRPCRequest("zd.cmd.send_write_request", { name: "FlgFPU", value: FlgFPU });
	};


	elems.buttonTiffPagesPerFile.onclick = (e) =>
	{
		if (!checkNumberValue(tiffPagesPerFile, true))
			return;

		sendJsonRPCRequest("video.tiff.set_pages_per_file", [ tiffPagesPerFile ]);
	};


	elems.textboxTiffPagesPerFile.oninput = (e) =>
	{
		tiffPagesPerFile = textboxIntValueGuard(e.target, 4, false);
		updateButtonTiffPagesPerFileState();
	}


	elems.textboxTiffPagesPerFile.onkeyup = (e) => 
	{
		if (!checkNumberValue(tiffPagesPerFile, false))
			return;

		if (e.key == "Enter") {
			elems.buttonTiffPagesPerFile.onclick(undefined);
		}
	}


	function updateLabelPlayPauseState()
	{
		elems.labelPlayPause.textContent = videoIsPlaying ? "Остановить" : "Возобновить";
	}


	function updateLabelStartStopCaptureState()
	{
		elems.textboxFrameSkipCount.disabled = isCaptureRunning;
		elems.textboxFrameСontinuouslyСount.disabled = isCaptureRunning;
		elems.labelStartStopCapture.textContent = isCaptureRunning ? "Остановить" : "Запустить";
	}

	function updateVideoPlayingState()
	{
		sendJsonRPCRequest(
			sprintf("video.%s", videoIsPlaying ? "play" : "pause"), 
			{ id: clienIDString }
		);
	}


	function updateCheckboxTiffCompressionState()
	{
		elems.checkboxTiffCompression.checked = isTiffCompressionEnabled;
	}


	elems.checkboxTiffCompression.oninput = (e) =>
	{
		isTiffCompressionEnabled = e.target.checked;
	}


	elems.sliderBrightness.oninput = (e) => 
	{
		const val = e.target.valueAsNumber;
		sendJsonRPCRequest("video.set_brightness", [val / e.target.max]);
		elems.tdBrightness.textContent = e.target.value;
	};

	elems.sliderContrast.oninput = (e) =>
	{
		const val = e.target.valueAsNumber;
		sendJsonRPCRequest("video.set_contrast", [val / e.target.max]);
		elems.tdContrast.textContent = e.target.value;
	};


	elems.sliderGamma.oninput = (e) =>
	{
		const val = e.target.valueAsNumber;
		sendJsonRPCRequest("video.set_gamma", [val / e.target.max]);
		elems.tdGamma.textContent = e.target.value;
	};


	elems.sliderBrightness.onmousedown 	= (e) => { effectSlidersMouseState.brightness 	= true; };
	elems.sliderContrast.onmousedown 	= (e) => { effectSlidersMouseState.contrast 	= true; };
	elems.sliderGamma.onmousedown 		= (e) => { effectSlidersMouseState.gamma 		= true; };
	elems.sliderBrightness.onmouseup 	= (e) => { effectSlidersMouseState.brightness 	= false; };
	elems.sliderContrast.onmouseup 		= (e) => { effectSlidersMouseState.contrast 	= false; };
	elems.sliderGamma.onmouseup 		= (e) => { effectSlidersMouseState.gamma 		= false; };

	elems.radioFlgFPU_54_0.oninput = extractFlgFPU;
	elems.radioFlgFPU_54_1.oninput = extractFlgFPU;
	elems.radioFlgFPU_54_2.oninput = extractFlgFPU;
	elems.radioFlgFPU_54_3.oninput = extractFlgFPU;
	elems.checkboxFlgFPU_2.oninput = extractFlgFPU;

	const updateEffectSliders = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		if (!effectSlidersMouseState.brightness) {
			batch.pushRequest("video.get_brightness", null, (j) =>
			{
				elems.sliderBrightness.value = j * elems.sliderBrightness.max;
				elems.tdBrightness.textContent = elems.sliderBrightness.value;
			});
		}

		if (!effectSlidersMouseState.contrast) {
			batch.pushRequest("video.get_contrast", null, (j) =>
			{
				elems.sliderContrast.value = j * elems.sliderContrast.max;
				elems.tdContrast.textContent = elems.sliderContrast.value;
			});
		}

		if (!effectSlidersMouseState.gamma) {
			batch.pushRequest("video.get_gamma", null, (j) =>
			{
				elems.sliderGamma.value = j * elems.sliderGamma.max;
				elems.tdGamma.textContent = elems.sliderGamma.value;
			});
		}

		batch.flush(() =>
		{
			this.continueTimeout(100);
		});
	});

	elems.video.onerror = (e) =>
	{
		elems.videoError.hidden = false;
		elems.video.hidden = true;
		elems.buttonStartStopCapture.disabled = true;
		elems.buttonPlayPause.disabled = true;
		elems.sliderBrightness.disabled = true;
		elems.sliderContrast.disabled = true;
		videoIP = "";
	};

	elems.video.onload = (e) => 
	{
		elems.video.hidden = false;
		elems.videoError.hidden = true;
		videoErrorShowed = false;
		isVideoLoaded = true;
		elems.sliderBrightness.disabled = false;
		elems.sliderContrast.disabled = false;

		sendJsonRPCRequest("video.is_playing", { id: clienIDString }, (j) =>
		{
			elems.buttonStartStopCapture.disabled = false;
			elems.buttonPlayPause.disabled = false;
			videoIsPlaying = j;
			updateLabelStartStopCaptureState();
		})
	};


	elems.buttonPlayPause.onclick = (e) =>
	{
		if (!isVideoLoaded) 
			return;
		
		videoIsPlaying = !videoIsPlaying;
		updateLabelPlayPauseState();
		updateVideoPlayingState();
	};


	elems.buttonStartStopCapture.onclick = (e) =>
	{
		if (!isVideoLoaded) 
			return;

		const batch = new JsonRPCBatch();

		if (checkNumberValue(tiffPagesPerFile, false)) {
			batch.pushRequest("video.tiff.set_pages_per_file", [ tiffPagesPerFile ]);
		}

		batch.pushRequest(isCaptureRunning ? "video.tiff.stop_capture" : "video.tiff.start_capture", isCaptureRunning ? null : [frameSkip, frameContinuosly], (j) =>
		{
			isCaptureRunning = !isCaptureRunning;
			elems.checkboxTiffCompression.disabled = isCaptureRunning;
			updateLabelStartStopCaptureState();
		});

		batch.flush();
	}


	elems.checkboxTiffCompression.oninput = (e) =>
	{
		isTiffCompressionEnabled = e.target.checked;
		sendJsonRPCRequest(isTiffCompressionEnabled ? "video.tiff.enable_compression" : "video.tiff.disable_compression");
	}


	function updateButtonsState()
	{
		const batch = new JsonRPCBatch();
		
		batch.pushRequest("video.tiff.is_capture_running", null, (j) =>
		{
			isCaptureRunning = j;
			elems.checkboxTiffCompression.disabled = isCaptureRunning;
			updateLabelStartStopCaptureState();
		});

		batch.pushRequest("video.tiff.is_compression_enabled", null, (j) => 
		{
			isTiffCompressionEnabled = j;
			elems.checkboxTiffCompression.disabled = false;
			updateCheckboxTiffCompressionState();
		});

		batch.pushRequest("video.tiff.get_pages_per_file", null, (j) =>
		{
			elems.textboxTiffPagesPerFile.value = j.toString();
			tiffPagesPerFile = textboxIntValueGuard(elems.textboxTiffPagesPerFile, 4, false);
			updateButtonTiffPagesPerFileState();
		});

		batch.flush();
	}


	const checkCapture = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("video.tiff.is_capture_running", null, (j) =>
		{
			isCaptureRunning = j;
			elems.checkboxTiffCompression.disabled = isCaptureRunning;
			updateLabelStartStopCaptureState();
			this.continueTimeout(1000);
		});
	});



	function updateWeightSensorBoundsToSetImm()
	{
		const uiImageRect = elems.videoContainer.getClientRects()[0];
		const sizeQ = new Vector2(uiImageRect.width / zdFrameSize.x, uiImageRect.height / zdFrameSize.y);

		elems.divWeightBoundsToSet.style.left 		= sprintf("%fpx", scrollX + uiImageRect.x + (sizeQ.x * weightSensorBoundsToSet.x) - 2);
		elems.divWeightBoundsToSet.style.top 		= sprintf("%fpx", scrollY + uiImageRect.y + (sizeQ.y * weightSensorBoundsToSet.y) - 2);
		elems.divWeightBoundsToSet.style.width 		= sprintf("%fpx", sizeQ.x * weightSensorBoundsToSet.w);
		elems.divWeightBoundsToSet.style.height 	= sprintf("%fpx", sizeQ.y * weightSensorBoundsToSet.h);
	}


	const updateWeightSensorBoundsToSet = new AsyncLoopFunc(function()
	{
		updateWeightSensorBoundsToSetImm();
		this.continueTimeout(100);
	});


	function updateCurrentWeightSensorBoundsImm()
	{
		const uiImageRect = elems.videoContainer.getClientRects()[0];
		const sizeQ = new Vector2(uiImageRect.width / zdFrameSize.x, uiImageRect.height / zdFrameSize.y);

		elems.divCurrentWeightBounds.style.left 	= sprintf("%fpx", scrollX + uiImageRect.x + (sizeQ.x * currentWeightSensorBounds.x) - 2);
		elems.divCurrentWeightBounds.style.top 		= sprintf("%fpx", scrollY + uiImageRect.y + (sizeQ.y * currentWeightSensorBounds.y) - 2);
		elems.divCurrentWeightBounds.style.width 	= sprintf("%fpx", sizeQ.x * currentWeightSensorBounds.w);
		elems.divCurrentWeightBounds.style.height 	= sprintf("%fpx", sizeQ.y * currentWeightSensorBounds.h);

	}


	const updateCurrentWeightSensorBounds = new AsyncLoopFunc(function()
	{
		updateCurrentWeightSensorBoundsImm();
		this.continueTimeout(100);
	});


	function getWeightParams()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("video.get_weight_rect", null, (j) =>
		{
			currentWeightSensorBounds = j;
			elems.textboxWeightSensorX.value = weightSensorBoundsToSet.x.toString();
			elems.textboxWeightSensorY.value = weightSensorBoundsToSet.y.toString();
			elems.textboxWeightSensorW.value = weightSensorBoundsToSet.w.toString();
			elems.textboxWeightSensorH.value = weightSensorBoundsToSet.h.toString();

			//updateWeightSensorBoundsToSet();
			//updateCurrentWeightSensorBounds();
		});

		batch.pushRequest("video.get_weight_bound", null, (j) =>
		{
			weightBoundToSet = j;
			currentWeightBound = j;
			elems.labelWeightBound.textContent = j.toString();
			elems.textboxWeightBound.value = elems.labelWeightBound.textContent;
		});

		batch.flush();
	}


	const getCurrentWeight = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("video.get_weight", null, (j) =>
		{
			elems.tdCurrentWeight.textContent = sprintf("%.3f", j);
			this.continueTimeout(100);
		});
	});


	elems.buttonResetWeightSensorBounds.onclick = (e) =>
	{
		for (let key in currentWeightSensorBounds) {
			weightSensorBoundsToSet[key] = currentWeightSensorBounds[key];
		}

		elems.textboxWeightSensorX.value = weightSensorBoundsToSet.x.toString();
		elems.textboxWeightSensorY.value = weightSensorBoundsToSet.y.toString();
		elems.textboxWeightSensorW.value = weightSensorBoundsToSet.w.toString();
		elems.textboxWeightSensorH.value = weightSensorBoundsToSet.h.toString();
		textboxFloatValueGuard(elems.textboxWeightSensorX);
		textboxFloatValueGuard(elems.textboxWeightSensorY);
		textboxFloatValueGuard(elems.textboxWeightSensorW);
		textboxFloatValueGuard(elems.textboxWeightSensorH);
		//updateWeightSensorBoundsToSet();
		updateButtonAcceptWeightSettings();
	};


	elems.buttonResetWeightBound.onclick = (e) =>
	{
		elems.textboxWeightBound.value = currentWeightBound.toString();
		weightBoundToSet = textboxFloatValueGuard(elems.textboxWeightBound);
		updateButtonAcceptWeightSettings();
	};


	elems.buttonSetWeightSensorBoundsFullScreen.onclick = (e) =>
	{
		weightSensorBoundsToSet.x = 0;
		weightSensorBoundsToSet.y = 0;
		weightSensorBoundsToSet.w = zdFrameSize.x;
		weightSensorBoundsToSet.h = zdFrameSize.y;
		elems.textboxWeightSensorX.value = weightSensorBoundsToSet.x.toString();
		elems.textboxWeightSensorY.value = weightSensorBoundsToSet.y.toString();
		elems.textboxWeightSensorW.value = weightSensorBoundsToSet.w.toString();
		elems.textboxWeightSensorH.value = weightSensorBoundsToSet.h.toString();

		textboxFloatValueGuard(elems.textboxWeightSensorX);
		textboxFloatValueGuard(elems.textboxWeightSensorY);
		textboxFloatValueGuard(elems.textboxWeightSensorW);
		textboxFloatValueGuard(elems.textboxWeightSensorH);

		//updateWeightSensorBoundsToSet();
		updateButtonAcceptWeightSettings();
	};


	function updateButtonAcceptWeightSettings()
	{
		elems.buttonAcceptWeightSettings.disabled = !weightSensorBoundsToSet.checkValue() ||
			!checkNumberValue(weightBoundToSet);
	}


	elems.textboxWeightSensorX.oninput = (e) =>
	{
		weightSensorBoundsToSet.x = textboxFloatValueGuard(e.target, null, zdFrameSize.x, 0);
		updateButtonAcceptWeightSettings();
		//updateWeightSensorBoundsToSet();
	};


	elems.textboxWeightSensorY.oninput = (e) =>
	{
		weightSensorBoundsToSet.y = textboxFloatValueGuard(e.target, null, zdFrameSize.y, 0);
		updateButtonAcceptWeightSettings();
		//updateWeightSensorBoundsToSet();
	};


	elems.textboxWeightSensorW.oninput = (e) =>
	{
		weightSensorBoundsToSet.w = textboxFloatValueGuard(e.target, null, zdFrameSize.x, 0);
		updateButtonAcceptWeightSettings();
		//updateWeightSensorBoundsToSet();
	};


	elems.textboxWeightSensorH.oninput = (e) =>
	{
		weightSensorBoundsToSet.h = textboxFloatValueGuard(e.target, null, zdFrameSize.y, 0);
		updateButtonAcceptWeightSettings();
		//updateWeightSensorBoundsToSet();
	};


	elems.textboxWeightBound.oninput = (e) =>
	{
		weightBoundToSet = textboxFloatValueGuard(e.target);
		updateButtonAcceptWeightSettings();
		//updateWeightSensorBoundsToSet();
	};


	elems.buttonAcceptWeightSettings.onclick = (e) =>
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("video.set_weight_bound", [weightBoundToSet]);
		batch.pushRequest("video.set_weight_rect", weightSensorBoundsToSet);
		batch.flush(getWeightParams);
	};


	updateLabelStartStopCaptureState();
	updateLabelPlayPauseState();

	//elems.sliderBrightness.valueAsNumber = filterParams.sliderBrightness;
	//elems.sliderBrightness.oninput({ target: elems.sliderBrightness });
//
	//elems.sliderContrast.valueAsNumber = filterParams.sliderContrast;
	//elems.sliderContrast.oninput({ target: elems.sliderContrast });

	updateButtonsState();
	videoTryLoad.run();
	checkCapture.run();
	sendJsonRPCRequest("video.get_frame_skip_count", null, (j) =>
	{
		elems.textboxFrameSkipCount.value = j.toString();
		frameSkip = textboxIntValueGuard(elems.textboxFrameSkipCount, 4);
	});

	sendJsonRPCRequest("video.get_frame_continuosly_count", null, (j) =>
	{
		elems.textboxFrameСontinuouslyСount.value = j.toString();
		frameContinuosly = textboxIntValueGuard(elems.textboxFrameСontinuouslyСount, 4);
	});


	let isVideoUIClicked = false;
	

	elems.videoContainer.onmousedown = (e) =>
	{
		if (e.button === 0) {
			//const targetRect = e.target.getBoundingClientRect();
			const uiImageRect = elems.videoContainer.getBoundingClientRect();
			const sizeQ = new Vector2(zdFrameSize.x / uiImageRect.width, zdFrameSize.y / uiImageRect.height);
			isVideoUIClicked = true;
			weightSensorBoundsToSet.x = Math.trunc((e.x - uiImageRect.x) * sizeQ.x);
			weightSensorBoundsToSet.y = Math.trunc((e.y - uiImageRect.y) * sizeQ.y);
			weightSensorBoundsToSet.w = 0;
			weightSensorBoundsToSet.h = 0;

			elems.textboxWeightSensorX.value = weightSensorBoundsToSet.x.toString();
			elems.textboxWeightSensorY.value = weightSensorBoundsToSet.y.toString();
			elems.textboxWeightSensorW.value = weightSensorBoundsToSet.w.toString();
			elems.textboxWeightSensorH.value = weightSensorBoundsToSet.h.toString();

			textboxFloatValueGuard(elems.textboxWeightSensorX);
			textboxFloatValueGuard(elems.textboxWeightSensorY);
			textboxFloatValueGuard(elems.textboxWeightSensorW);
			textboxFloatValueGuard(elems.textboxWeightSensorH);
			updateWeightSensorBoundsToSetImm();
		}
	};


	document.onmouseup = (e) =>
	{
		if (e.button === 0 && isVideoUIClicked) {
			isVideoUIClicked = false;
			//console.log("всё");
		}
	};


	elems.videoContainer.onmousemove = (e) =>
	{
		if (isVideoUIClicked) {
			//const targetRect = e.target.getBoundingClientRect();
			const uiImageRect = elems.videoContainer.getBoundingClientRect();
			const sizeQ = new Vector2(zdFrameSize.x / uiImageRect.width, zdFrameSize.y / uiImageRect.height);
			weightSensorBoundsToSet.w = (e.x - uiImageRect.x) * sizeQ.x - weightSensorBoundsToSet.x;
			weightSensorBoundsToSet.h = (e.y - uiImageRect.y) * sizeQ.y - weightSensorBoundsToSet.y;

			if (weightSensorBoundsToSet.w < 0) {
				weightSensorBoundsToSet.x = (e.x - uiImageRect.x) * sizeQ.x;
				weightSensorBoundsToSet.w = Math.abs(weightSensorBoundsToSet.w);
			}

			if (weightSensorBoundsToSet.h < 0) {
				weightSensorBoundsToSet.y = (e.y - uiImageRect.y) * sizeQ.y;
				weightSensorBoundsToSet.h = Math.abs(weightSensorBoundsToSet.h);
			}

			for (let key in weightSensorBoundsToSet) {
				weightSensorBoundsToSet[key] = Math.trunc(weightSensorBoundsToSet[key]);
			}

			elems.textboxWeightSensorX.value = weightSensorBoundsToSet.x.toString();
			elems.textboxWeightSensorY.value = weightSensorBoundsToSet.y.toString();
			elems.textboxWeightSensorW.value = weightSensorBoundsToSet.w.toString();
			elems.textboxWeightSensorH.value = weightSensorBoundsToSet.h.toString();

			textboxFloatValueGuard(elems.textboxWeightSensorX);
			textboxFloatValueGuard(elems.textboxWeightSensorY);
			textboxFloatValueGuard(elems.textboxWeightSensorW);
			textboxFloatValueGuard(elems.textboxWeightSensorH);
			updateWeightSensorBoundsToSetImm();
			//console.log(weightSensorBoundsToSet);
		}
	};

	const batch = new JsonRPCBatch();

	batch.pushRequest("zd.cmd.get_field_value", { name: "FlgFPU" }, (j) => 
	{
		if (j.raw === null)
			return;

		switch ((j.raw >> 4) & 3) {
		case 0:
			elems.radioFlgFPU_54_0.checked = true;
			break;

		case 1:
			elems.radioFlgFPU_54_1.checked = true;
			break;
		
		case 2:
			elems.radioFlgFPU_54_2.checked = true;
			break;

		case 3:
			elems.radioFlgFPU_54_3.checked = true;
			break;
		}

		elems.checkboxFlgFPU_2.checked = j.raw & 4 !== 0;
	});

	batch.flush();

	updateEffectSliders.run();
	updateWeightSensorBoundsToSet.run();
	updateCurrentWeightSensorBounds.run();
	getWeightParams();
	getCurrentWeight.run();

	let logToAnotherEnabled = false;

	const checkLogToAnother = new AsyncLoopFunc(function () {
		sendJsonRPCRequest("zd.cmd.record_log_to_another_enabled", null, j => {
			logToAnotherEnabled = j;
			elems.labelChangeLogToAnother.textContent = logToAnotherEnabled ? "Выключить" : "Включить";
			this.continueTimeout(300);
		});
	});

	checkLogToAnother.run();

	buttonChangeLogToAnother.onclick = () => {
		if (logToAnotherEnabled) {
			sendJsonRPCRequest("zd.cmd.disable_record_log_to_another_file");
		} else {
			sendJsonRPCRequest("zd.cmd.enable_record_log_to_another_file");
		}
	};
}