function main()
{
	const elems = getAJAXElemsWithID();
	const telemName = "pmo";
	let textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);

	function setTelemPeriod(period)
	{
		if (!checkNumberValue(period, true))
			return;

		Pilv.mko.setTelemPeriod(telemName, period);
	}


	function updateButtonSetTelemPeriodState()
	{
		elems.buttonSetTelemPeriod.disabled = !checkNumberValue(textboxSetTelemPeriod_value, false);
	}


	elems.textboxSetTelemPeriod.oninput = (e) =>
	{
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTelemPeriodState();
	};

	elems.textboxSetTelemPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	elems.buttonSetTelemPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTelemPeriod_value, true))
			return;
			
		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	const elemsByFieldNames = (() =>
	{
		let result = {};

		for (let key in elems) {
			if (key.substring(0, 2) == "td") {
				let fieldName = key.substring(2);
				result[fieldName] = elems[key];
			}
		}

		return result;
	})();

	const fieldNames = objectToKeyArray(elemsByFieldNames);

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_fields_value", { names: fieldNames }, (j) =>
		{
			for (let i = 0; i < j.length; ++i) {
				const fieldValue = j[i];
				const name = fieldNames[i];
				const elem = elemsByFieldNames[name];
				elem.textContent = fieldValue === null ? "[Данного поля нет в базе]" : fieldValue.raw !== null ? fieldValue.formatted : emptyValueString;
			}
		});

		batch.pushRequest("zd.cmd.mko.is_telem_enabled", [telemName], j => {
			elems.checkboxTelem.checked = j;
			elems.buttonSetTelemPeriod.disabled = !(j && checkNumberValue(textboxSetTelemPeriod_value));
			elems.textboxSetTelemPeriod.disabled = !j;
		});

		const cont = () => this.continueTimeout(global.telemUpdateInterval);
		batch.flush(cont, cont);
	});

	Pilv.mko.getTelemPeriod(telemName, period => {
		const seconds = period / 1000;
		elems.textboxSetTelemPeriod.value = seconds.toString();
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);
	});

	elems.checkboxTelem.oninput = e => (e.currentTarget.checked ? Pilv.mko.enableTelem : Pilv.mko.disableTelem)(telemName);

	updateButtonSetTelemPeriodState();
	loop.run();
}