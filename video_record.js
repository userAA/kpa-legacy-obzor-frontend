function main()
{
	const elems = getAJAXElemsWithID();
	const flashParams = new FlashParams();
	let isLvdsDecodingRunning = false;
	let doubleSendCommands = true;
	let Exp_frame_sel = null;

	const FL_MASK_bitNumsByCheckboxIDs = (() => 
	{
		let result = {};
		const prefix = "checkboxFL_MASK_";
		
		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const bitNum = Number.parseInt(elems[key].name);
				result[key] = bitNum;
			}
		}

		return result;
	})();

	const FL_MASK_checkboxIDsByBitNums = (() => 
	{
		let result = [];

		for (let key in FL_MASK_bitNumsByCheckboxIDs) {
			const bitNum = FL_MASK_bitNumsByCheckboxIDs[key];
			result[bitNum] = key;
		}

		return result;
	})();
	
	const textboxesByFieldNames = {
		"FL_MASK": elems.textboxFL_MASK, 
		"FL_FRAMES_ADDR": elems.textboxFL_FRAMES,
		"FL_FRAME_DEC_ADDR": elems.textboxFL_FRAME_DEC,
		"FL_FRAME_REC_ADDR": elems.textboxFL_FRAME_REC,
		"compr_params_Addr": elems.textboxcompr_params_Addr,
		"FL_LVDS_MPI": elems.textboxFL_LVDS_MPI
	}

	const generalFieldsToRead = objectToKeyArray(textboxesByFieldNames);
	generalFieldsToRead.push("Exp_frame_sel");

	const flashParamsByFieldNames = {
		"FL_MASK": "FL_MASK",
		"FL_FRAMES_ADDR": "FL_FRAMES",
		"FL_FRAME_DEC_ADDR": "FL_FRAME_DEC",
		"FL_FRAME_REC_ADDR": "FL_FRAME_REC",
		"Exp_frame_sel": "Exp_frame_sel",
		"compr_params_Addr": "compr_params_Addr",
		"FL_LVDS_MPI": "FL_LVDS_MPI"
	};

	const shutterTextboxes = (() => 
	{
		let result = [];
		const prefix = "textboxShutter";

		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const shutterNum = Number.parseInt(key.substring(prefix.length));
				result[shutterNum] = elems[key];
			}
		}

		return result;
	})();

	const shutterIndecesByShutterTextboxIDs = (() => 
	{
		let result = {};

		for (let i = 0; i < shutterTextboxes.length; ++i) {
			result[shutterTextboxes[i].id] = i;
		}

		return result;
	})();

	const shutter = (() => 
	{
		let result = [];

		for (let i = 0; i < shutterTextboxes.length; ++i) {
			const elem = shutterTextboxes[i];
			const value = textboxIntValueGuard(elem, 2, false);
			result[i] = value;
		}

		return result;
	})();

	if (elems.radioFL_LVDS_MPI_LVDS.checked) {
		flashParams.FL_LVDS_MPI = 0; 
	} else if (elems.radioFL_LVDS_MPI_KTI.checked) {
		flashParams.FL_LVDS_MPI = 1;
	}


	elems.buttonClearMemory.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			batch.flush(en);
			Pilv.flashVideo.clearMemory(en, en);
		//});
	};


	elems.buttonStartRecord.onclick = (e) => 
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x61, data: [] });
			
			batch.pushRequest("pilv.flash_video.start_record");	
			batch.flush(en);
			//Pilv.flashVideo.startRecord(en, en);
		//});
	};


	elems.buttonStopOperation.onclick = (e) => 
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x62, data: [] });
			
			batch.pushRequest("pilv.flash_video.stop_operation");	
			batch.flush(en);
			//Pilv.flashVideo.stopOperation(en, en);
	//	});
	};


	elems.buttonPauseRecord.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x63, data: [] });
			
			batch.pushRequest("pilv.flash_video.pause_record");	
			batch.flush(en);
			//Pilv.flashVideo.pauseRecord(en, en);
		//});
	};


	elems.buttonResumeRecord.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };
	
		//setIRQ_MASK_REQto0And(() => 
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x64, data: [] });
			
			batch.pushRequest("pilv.flash_video.resume_record");	
			batch.flush(en);
			//Pilv.flashVideo.resumeRecord(en, en);
		//});
	};


	elems.buttonReadMemory.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() => 
		//{
			const batch = new JsonRPCBatch();

			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x61, data: [] });
			
			batch.pushRequest("pilv.flash_video.read_memory");		
			batch.flush(en);
			//Pilv.flashVideo.readMemory(en, en);
		//});
	};


	function checkboxFL_MASK_oninput()
	{
		let FL_MASK = 0;

		for (let key in FL_MASK_bitNumsByCheckboxIDs) {
			const elem = elems[key];
			const bitNum = FL_MASK_bitNumsByCheckboxIDs[key];
			FL_MASK |= elem.checked ? (1 << bitNum) : 0;
		}

		elems.textboxFL_MASK.value = sprintf(stringIsHex(elems.textboxFL_MASK.value) ? "0x%04X" : "%d", FL_MASK);
		flashParams.FL_MASK = FL_MASK;
	}


	elems.checkboxFL_MASK_0.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_1.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_2.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_3.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_4.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_5.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_6.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_7.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_8.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_9.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_10.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_11.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_12.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_13.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_14.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_15.oninput = checkboxFL_MASK_oninput;


	function updateFL_MASKCheckboxes(FL_MASK)
	{
		for (let i = 0; i < 16; ++i) {
			const bitValue = FL_MASK & (1 << i);
			const elem = elems[FL_MASK_checkboxIDsByBitNums[i]];
			
			elem.checked = bitValue != 0;
		}
	}


	function updateButtonSetMaskState()
	{
		elems.buttonSetMask.disabled = !(checkNumberValue(flashParams.FL_MASK, false) 
			&& checkNumberValue(flashParams.FL_FRAMES, false));
	}


	function setFL_MASK(FL_MASK)
	{
		flashParams.FL_MASK = FL_MASK;
		updateFL_MASKCheckboxes(FL_MASK);
		updateButtonSetMaskState();
		updateButtonSendParamsState();
	}


	function setFL_FRAMES(FL_FRAMES)
	{
		flashParams.FL_FRAMES = FL_FRAMES;
		updateButtonSetMaskState();
		updateButtonSendParamsState();
	}


	function setFL_START_PAGE(FL_START_PAGE)
	{
		flashParams.FL_START_PAGE = FL_START_PAGE;
		const isFL_START_PAGECorrect = checkNumberValue(flashParams.FL_START_PAGE, false);
		elems.buttonSetStartPage.disabled = !isFL_START_PAGECorrect;
		updateButtonSendParamsState();
	}


	function setFL_NUM_PAGE(FL_NUM_PAGE)
	{
		flashParams.FL_NUM_PAGE = FL_NUM_PAGE;
		updateButtonSendParamsState();
	}


	function setFL_FRAME_DEC(FL_FRAME_DEC)
	{
		flashParams.FL_FRAME_DEC = FL_FRAME_DEC;
		updateButtonSetFrameDecRecState();
		updateButtonSendParamsState();
	}


	function setFL_FRAME_REC(FL_FRAME_REC)
	{
		flashParams.FL_FRAME_REC = FL_FRAME_REC;
		updateButtonSetFrameDecRecState();
		updateButtonSendParamsState();
	}


	function setcompr_params_Addr(compr_params_Addr)
	{
		flashParams.compr_params_Addr = compr_params_Addr;
		updateButtonSendParamsState();
	}


	const flashParamsSetterFuncs = {
		FL_MASK: setFL_MASK,
		FL_FRAMES: setFL_FRAMES,
		FL_FRAME_DEC: setFL_FRAME_DEC,
		FL_FRAME_REC: setFL_FRAME_REC,
		compr_params_Addr: setcompr_params_Addr,
		FL_LVDS_MPI: (FL_LVDS_MPI) => 
		{
			flashParams.FL_LVDS_MPI = FL_LVDS_MPI;
		}
	};


	function updateButtonSendParamsState()
	{
		let isCorrect = true;

		for (let key in flashParams) {
			if (!checkNumberValue(flashParams[key], false)) {
				isCorrect = false;
				break;
			}
		}

		isCorrect &&= checkNumberValue(Exp_frame_sel, false);

		elems.buttonSendParams.disabled = !isCorrect;
	}


	function updateButtonSetFrameDecRecState()
	{
		elems.buttonSetFrameDecRec.disabled = !(checkNumberValue(flashParams.FL_FRAME_DEC, false) 
			&& checkNumberValue(flashParams.FL_FRAME_REC, false));
	}


	function updateButtonSendShutter()
	{
		let isCorrect = true;

		for (let i = 0; i < shutterTextboxes.length; ++i) {
			const elem = shutterTextboxes[i];

			if (!checkNumberValue(shutter[i], false)) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonSendShutter.disabled = !isCorrect;
	}


	elems.textboxFL_MASK.oninput = (e) =>
	{
		const FL_MASK = textboxIntValueGuard(e.target, 2, false);
		setFL_MASK(FL_MASK);
	};


	elems.textboxFL_FRAMES.oninput = (e) => 
	{
		const FL_FRAMES = textboxIntValueGuard(e.target, 2, false);
		setFL_FRAMES(FL_FRAMES);
	};


	elems.textboxFL_START_PAGE.oninput = (e) =>
	{
		const FL_START_PAGE = textboxIntValueGuard(e.target, 4, false);
		setFL_START_PAGE(FL_START_PAGE);
	};
		

	elems.textboxFL_NUM_PAGE.oninput = (e) =>
	{
		const FL_NUM_PAGE = textboxIntValueGuard(e.target, 4, false);
		setFL_NUM_PAGE(FL_NUM_PAGE);
	};


	elems.textboxFL_FRAME_DEC.oninput = (e) =>
	{
		const FL_FRAME_DEC = textboxIntValueGuard(e.target, 2, false);
		setFL_FRAME_DEC(FL_FRAME_DEC);
	};


	elems.textboxFL_FRAME_REC.oninput = (e) =>
	{
		const FL_FRAME_REC = textboxIntValueGuard(e.target, 2, false);
		setFL_FRAME_REC(FL_FRAME_REC);
	};


	elems.textboxcompr_params_Addr.oninput = (e) =>
	{
		const compr_params_Addr = textboxIntValueGuard(e.target, 2, false);
		setcompr_params_Addr(compr_params_Addr);
	};


	elems.radioFL_LVDS_MPI_LVDS.oninput = (e) =>
	{
		flashParams.FL_LVDS_MPI = 0;
	};


	elems.radioFL_LVDS_MPI_KTI.oninput = (e) =>
	{
		flashParams.FL_LVDS_MPI = 1;
	};


	elems.textboxExp_frame_sel.oninput = e => {
		Exp_frame_sel = textboxIntValueGuard(e.target, 2, false);
	};


	elems.buttonSetMask.onclick = (e) =>
	{
		Pilv.flashVideo.setMask(flashParams.FL_MASK, flashParams.FL_FRAMES);

		//batch.pushRequest("zd.cmd.send_read_request", { names: ["FL_START_PAGE_L", "FL_START_PAGE_H"] }, (j) =>
		//{
		//	setFL_START_PAGE(MAKELONG(j[0].raw, j[1].raw));
		//	elems.textboxFL_START_PAGE.value = flashParams.FL_START_PAGE.toString();
		//}, null);
	};


	elems.buttonSetStartPage.onclick = (e) => 
	{
		Pilv.flashVideo.setStartPage(flashParams.FL_START_PAGE);

		//batch.pushRequest("zd.cmd.send_read_request", { names: ["FL_NUM_PAGE_L", "FL_NUM_PAGE_H"] }, (j) =>
		//{
		//	setFL_NUM_PAGE(MAKELONG(j[0].raw, j[1].raw));
		//	elems.textboxFL_NUM_PAGE.value = flashParams.FL_NUM_PAGE.toString();
		//}, null);
	};


	elems.buttonSetFrameDecRec.onclick = (e) => 
	{
		Pilv.flashVideo.setFrameDecRec(flashParams.FL_FRAME_DEC, flashParams.FL_FRAME_REC);

		if (!sendReadFields.isRunning) {
			sendReadFields.run();
		}
	};


	elems.buttonSendParams.onclick = (e) => 
	{
		sendJsonRPCRequest("zd.cmd.send_write_request", { name: "Exp_frame_sel", value: Exp_frame_sel }, j => {
			
			Pilv.flashVideo.sendParams(flashParams);
	
			if (!sendReadFields.isRunning) {
				sendReadFields.run();
			}
		});
	};


	elems.buttonSendShutter.onclick = (e) =>
	{
		Pilv.sendShutter(shutter);
	};


	function textboxShutterOnInput(e) 
	{
		const shutterIndex = shutterIndecesByShutterTextboxIDs[e.target.id];
		const value = textboxIntValueGuard(e.target, 2, false);

		shutter[shutterIndex] = value;
		updateButtonSendShutter();
	}


	for (let i = 0; i < shutterTextboxes.length; ++i) {
		const elem = shutterTextboxes[i];
		elem.oninput = textboxShutterOnInput;
	}


	const sendReadFields = new AsyncLoopFunc(function()
	{
		elems.trFieldsReading.hidden = false;
		const cont = () => { this.continueTimeout(1000); };
		const fieldArray = (() => 
		{
			let result = [];
			generalFieldsToRead.forEach((v) => { result.push(v); });
			result.push("FL_START_PAGE_L");
			result.push("FL_START_PAGE_H");
			result.push("FL_NUM_PAGE_L");
			result.push("FL_NUM_PAGE_H");
			result.push("Exp_frame_sel");
			return result;
		})();


		setIRQ_MASK_REQto0And((setIRQ_MASK_REQRrev) => 
		{
			sendJsonRPCRequest("zd.cmd.send_read_request", { names: fieldArray }, (j) =>
			{
				for (let i = 0; i < j.length; ++i) {
					if (j[i] === null) {
						this.continueTimeout(1000);
						return;
					}
				}

				Exp_frame_sel = j[j.length - 1].raw;
				elems.textboxExp_frame_sel.value = Exp_frame_sel.toString();
				Exp_frame_sel = textboxIntValueGuard(elems.textboxExp_frame_sel, 2, false);
				elems.textboxExp_frame_sel.disabled = false;
	
				for (let i = 0; i < generalFieldsToRead.length; ++i) {
					const fieldValue = j[i];
					const fieldName = generalFieldsToRead[i];
					const elem = textboxesByFieldNames[fieldName];
					const flashParamName = flashParamsByFieldNames[fieldName];
					const setter = flashParamsSetterFuncs[flashParamName];
	
					if (elem === undefined)
						continue;
	
					(setter ?? ((v) => 
					{ 
						flashParams[flashParamName] = v; 
					}))(fieldValue.raw);
	
					elem.value = fieldValue.formatted;
					elem.disabled = false;
				}
	
				for (let i = 0; i < 16; ++i) {
					const elem = elems[FL_MASK_checkboxIDsByBitNums[i]];				
					elem.disabled = false;
				}
	
				updateFL_MASKCheckboxes(flashParams.FL_MASK);
				elems.trFieldsReading.hidden = true;
	
				const FL_START_PAGE_L 	= j[generalFieldsToRead.length + 0];
				const FL_START_PAGE_H 	= j[generalFieldsToRead.length + 1];
				const FL_NUM_PAGE_L 	= j[generalFieldsToRead.length + 2];
				const FL_NUM_PAGE_H 	= j[generalFieldsToRead.length + 3];
	
				setFL_START_PAGE(MAKELONG(FL_START_PAGE_L.raw, FL_START_PAGE_H.raw));
				elems.textboxFL_START_PAGE.disabled = false;
				elems.textboxFL_START_PAGE.value = flashParams.FL_START_PAGE.toString();
				
				setFL_NUM_PAGE(MAKELONG(FL_NUM_PAGE_L.raw, FL_NUM_PAGE_H.raw));
				elems.textboxFL_NUM_PAGE.disabled = false;
				elems.textboxFL_NUM_PAGE.value = flashParams.FL_NUM_PAGE.toString();
	
				setFL_MASK(stringToInt(elems.textboxFL_MASK.value));
				setFL_FRAMES(stringToInt(elems.textboxFL_FRAMES.value));
				setFL_START_PAGE(stringToInt(elems.textboxFL_START_PAGE.value));
				setFL_NUM_PAGE(stringToInt(elems.textboxFL_NUM_PAGE.value));
				setFL_FRAME_DEC(stringToInt(elems.textboxFL_FRAME_DEC.value));
				setFL_FRAME_REC(stringToInt(elems.textboxFL_FRAME_REC.value));
				setcompr_params_Addr(stringToInt(elems.textboxcompr_params_Addr.value));
				updateButtonSendParamsState();
				//setIRQ_MASK_REQRrev();
			}, cont);
		}, cont);
	});

		
	

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_fields_value", { names: ["DeviceStatus_LSW", "CUR_FLASH_page"] }, (j) => 
		{
			const [DeviceStatus_LSW, CUR_FLASH_page] = j;
			const CUR_FLASH_pageMax = 0x200000;
			const val = (DeviceStatus_LSW.raw >> 1) & 0x0F;

			switch (val) {
				case 6:
					elems.trReadPercentage.hidden = false;
					elems.labelFlashReadPercentage.textContent = CUR_FLASH_page !== null ? sprintf("%0.3f", (CUR_FLASH_page.raw / CUR_FLASH_pageMax) * 100) : emptyValueString;
					elems.labelFlashReadRawValue.textContent = CUR_FLASH_page !== null ? sprintf("0x%08X", CUR_FLASH_page.raw) : emptyValueString;
					break;	

				default:
					elems.trReadPercentage.hidden = true;
					break;
			}
			switch (val) {
				case 2:
					elems.labelDeviceStatus.textContent = "ОЧИСТКА!";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = true;
					break;
				case 4:
					elems.labelDeviceStatus.textContent = "ЗАПИСЬ!";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = true;
					break;
				case 6:
					elems.labelDeviceStatus.textContent = "ЧТЕНИЕ";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = true;
					break;
				case 1:
					elems.labelDeviceStatus.textContent = "ОЖИДАНИЕ";
					elems.buttonClearMemory.disabled = false;
					elems.buttonStartRecord.disabled = false;
					elems.buttonStopOperation.disabled = true;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = isLvdsDecodingRunning;
					break;
				case 10:
				case 12:
				case 14:
					elems.labelDeviceStatus.textContent = "ПАУЗА";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = true;
					elems.buttonResumeRecord.disabled = false;
					elems.buttonReadMemory.disabled = true;
					break;
			}
		});

		batch.flush(() => {
			this.continueTimeout(global.telemUpdateInterval);
		})
	});


	const checkLvdsDecodingProgress = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("lvds.decoder.get_progress", null, (j) =>
		{
			const hasError = j.hasOwnProperty("error");
			elems.trLvdsProgress.hidden = !j.is_running;
			isLvdsDecodingRunning = j.is_running;

			if (!j.is_running) {
				if (!loop.isRunning) {
					loop.run();
				}
				
				elems.radio_remove_repeating_telem_enabled.disabled = false;
				this.continueTimeout(global.telemUpdateInterval);
				return;
			} else {
				if (loop.isRunning) {
					loop.stop();
				}
			}


			if (hasError) {
				elems.tdLvdsDecodingState.textContent = sprintf("Ошибка при декодировании flash видео: %s", j.error);
			} else {
				elems.tdLvdsDecodingState.textContent = "Запись flash-видео декодируется...";
			}

			elems.radio_remove_repeating_telem_enabled.disabled = true;

			sendJsonRPCRequest("lvds.decoder.disableRemoveRepeatingTelemetryEnabled", null, (j) => {
				elems.radio_remove_repeating_telem_enabled.checked = j;
			});

			elems.progressLvdsDecoding.value = j.completed * elems.progressLvdsDecoding.max;
			elems.tdLvdsChunksCounter.textContent = j.counter.chunk;
			elems.tdLvdsPkgVideoCounter.textContent = j.counter.video;
			elems.tdLvdsTelemCounter.textContent = j.counter.telem;
			elems.tdLvdsPkgCounter.textContent = j.counter.total_packages;
			elems.tdLvdsFramesCounter.textContent = j.counter.frames;
			this.continueTimeout(global.telemUpdateInterval);
		});
	});

	// todo: КАКОЕ ЗНАЧЕНИЕ УСТАНАВЛИВАТЬ?
	flashParams.Exp_frame_sel = 0;
	sendReadFields.run();
	checkLvdsDecodingProgress.run();

	elems.radio_remove_repeating_telem_enabled.oninput = (e) => {
		if (e.currentTarget.checked) {
			sendJsonRPCRequest("lvds.decoder.enableRemoveRepeatingTelemetry");
		} else {
			sendJsonRPCRequest("lvds.decoder.disableRemoveRepeatingTelemetry");
		}
	};
}