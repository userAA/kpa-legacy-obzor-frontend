function main()
{
	let adjObjsCount = 0;
	let adjObjsInput = [];
	let adjObjs = [];
	let adjObjsRATextboxes = [];
	let adjObjsDecTextboxes = [];
	let adjObjsMagTextboxes = [];
	let adjObjsVelTextboxes = [];
	let adjObjsRATds = [];
	let adjObjsDecTds = [];
	let adjObjsMagTds = [];
	let adjObjsVelTds = [];

	const elems = getAJAXElemsWithID();
	let checkAngles = elems.checkboxAngles.checked;
	let checkQuat = elems.checkboxQuat.checked;
	let rotationTime = textboxIntValueGuard(elems.textboxRotationTime, 4, false);

	adjObjs.length = adjObjsCount;

	for (let i = 0; i < adjObjsCount; ++i)
		adjObjs[i] = new AdjObj();

	const rotation = new RDR();

	elems.rangeIznHue.oninput = e => {
		sendJsonRPCRequest("izn.set_hue", [Number(e.target.value) / 100]);
	};

	const rotationDegs = new RDR(
		textboxIntValueGuard(elems.textboxRADeg, 		null, true, null, 359, -359),
		textboxIntValueGuard(elems.textboxDecDeg, 		null, true, null, 90, -90),
		textboxIntValueGuard(elems.textboxRollDeg,		null, true, null, 359, -359)
	);

	const rotationMins = new RDR(
		textboxIntValueGuard(elems.textboxRAMin, 		null, false, null, 59, 0),
		textboxIntValueGuard(elems.textboxDecMin, 		null, false, null, 59, 0),
		textboxIntValueGuard(elems.textboxRollMin,		null, false, null, 59, 0)
	);

	const rotationSecs = new RDR(
		textboxFloatValueGuard(elems.textboxRASec, 		null, null, 0, 60, null),
		textboxFloatValueGuard(elems.textboxDecSec, 	null, null, 0, 60, null),
		textboxFloatValueGuard(elems.textboxRollSec,	null, null, 0, 60, null)
	);

	const quat = new Quaternion();
	let qNorm = 0;

	let showStars 	= elems.checkboxStars.checked;
	let showMoon 	= elems.checkboxMoon.checked;
	let showSun 	= elems.checkboxSun.checked;
	let showAdjObjs = elems.checkboxAdjObjs.checked;

	const moonPositionInput = new RD(
		textboxFloatValueGuard(elems.textboxMoonRA, 	null, null, 0, 360, null),
		textboxFloatValueGuard(elems.textboxMoonDec, 	null, null, 0, 360, null)
	);

	const sunPositionInput = new RD(
		textboxFloatValueGuard(elems.textboxSunRA, 	null, null, 0, 360, null),
		textboxFloatValueGuard(elems.textboxSunDec, null, null, 0, 360, null)
	);

	const moonPosition = new RD();
	const sunPosition = new RD();

	let angleSpeed = textboxFloatValueGuard(elems.textboxAngleSpeed, null, 100, 0);
	let angleAcceleration = textboxFloatValueGuard(elems.textboxAngleAcceleration, null, 100, 0);

	let axisRotatingSpeed = textboxFloatValueGuard(elems.textboxSetAxisRotatingSpeed);
	let axisRotating = new Vector3(
		textboxFloatValueGuard(elems.textboxSetAxisRotatingX), 
		textboxFloatValueGuard(elems.textboxSetAxisRotatingY),
		textboxFloatValueGuard(elems.textboxSetAxisRotatingZ)
	);

	function degGetDegs(deg)
	{
		return Math.trunc(deg);
	}


	function degGetMins(deg) 
	{
		return (deg < 0 ? -1 : 1) * Math.trunc((deg - Math.trunc(deg)) * 60); 
	}


	function degGetSecs(deg) 
	{
		let result = ((deg < 0 ? -1 : 1) * (((deg - Math.trunc(deg)) * 3600) % 60));
		result = Math.round(result * 100) / 100;
		return result;
	}


	function degtorad(deg)
	{
		return deg * (Math.PI / 180);
	}


	function radtodeg(rad)
	{
		return rad * (180 / Math.PI);
	}


	function degAbs(deg)
	{
		return deg < 0 ? deg + 360 : deg;
	}


	function updateQuatFromDegs()
	{
		if (!rotation.checkValue())
			return;

		const radRotation2 = new RDR(
			degtorad(degAbs(rotation.ra)) / 2,
			degtorad(degAbs(rotation.dec)) / 2,
			degtorad(degAbs(rotation.roll)) / 2
		);

		const sinRotation2 = new RDR(
			Math.sin(radRotation2.ra),
			Math.sin(radRotation2.dec),
			Math.sin(radRotation2.roll)
		);

		const cosRotation2 = new RDR(
			Math.cos(radRotation2.ra),
			Math.cos(radRotation2.dec),
			Math.cos(radRotation2.roll)
		);

		quat.w = cosRotation2.dec * cosRotation2.ra * cosRotation2.roll - sinRotation2.dec * sinRotation2.ra * sinRotation2.roll;
	    quat.x = cosRotation2.dec * cosRotation2.ra * sinRotation2.roll + sinRotation2.dec * sinRotation2.ra * cosRotation2.roll;
	    quat.y = cosRotation2.dec * sinRotation2.ra * sinRotation2.roll - sinRotation2.dec * cosRotation2.ra * cosRotation2.roll;
	    quat.z = cosRotation2.dec * sinRotation2.ra * cosRotation2.roll + sinRotation2.dec * cosRotation2.ra * sinRotation2.roll;

		if (quat.w < 0) {
			quat.w *= -1;
			quat.x *= -1;
			quat.y *= -1;
			quat.z *= -1;
		}

		elems.textboxQ0.value = quat.w.toString();
		elems.textboxQ1.value = quat.x.toString();
		elems.textboxQ2.value = quat.y.toString();
		elems.textboxQ3.value = quat.z.toString();
		updateQNorm();
	}


	function updateDegsFromQuat()
	{
		if (!quat.checkValue()) 
			return;

		const SinCos2Angle = (SinA, CosA) => 
		{
			if (Math.abs(CosA) > Math.abs(SinA)) {
				a = Math.atan(SinA / CosA);

				if (CosA < 0) {
					if (a > 0)
						a = a - Math.PI;
					else
						a = a + Math.PI;
				}
			} else {
				a = Math.PI / 2 - Math.atan(CosA / SinA);

				if (SinA < 0) {
					a = a - Math.PI;
				}
			}

			return a;
		};

		elems.trQuatError.hidden = qNorm != 0;

		if (qNorm === 0)
			return;

		qn = new Quaternion(quat.w / qNorm, quat.x / qNorm, quat.y / qNorm, quat.z / qNorm);

		const Sdec = 2.0 * (qn.x * qn.z - qn.w * qn.y);

		if (Sdec > 1)
			Sdec = 1;

		if (Sdec < -1)
			Sdec = -1;

		const QDec = Math.asin(Sdec);
		const SraCdec = 2.0 * (qn.w * qn.z + qn.x * qn.y);
		const CraCdec = qn.w * qn.w + qn.x * qn.x - qn.y * qn.y - qn.z * qn.z;
		const Cdec = Math.sqrt(SraCdec * SraCdec + CraCdec * CraCdec);
		const FLT_EPSILON = 3.0e-16;
		
		let Sra = 0;
		let Cra = 0;
		let QRa = 0;
		let Sphi = 0;
		let Cphi = 0;

		if (Cdec > FLT_EPSILON) {
			const invCdec = 1.0 / Math.sqrt(SraCdec * SraCdec + CraCdec * CraCdec);
			
			Sra = SraCdec * invCdec;
			Cra = CraCdec * invCdec;
			QRa = SinCos2Angle(Sra, Cra);

			if (QRa < 0)
			    QRa = QRa + 2.0 * Math.PI;

			Sphi = 2.0 * (qn.w * qn.x + qn.y * qn.z) * invCdec;
			Cphi = (qn.w * qn.w - qn.x * qn.x - qn.y * qn.y + qn.z * qn.z) * invCdec;
		} else {
			QRa = 0.0;
			Sphi = 2.0 * (-qn.y * qn.z + qn.w * qn.x);
			Cphi = (2.0 * (qn.w * qn.w + qn.y * qn.y) - 1.0);
		}

		const QPhi = SinCos2Angle(Sphi, Cphi);

		rotation.ra = degAbs(radtodeg(QRa));
		rotation.dec = radtodeg(QDec); 
		rotation.roll = degAbs(radtodeg(QPhi)); 

		console.log(sprintf("qra = %f, qdec = %f, qphi = %f\n", 
			radtodeg(QRa), radtodeg(QDec), radtodeg(QPhi)));

		rotationDegs.ra = degGetDegs(rotation.ra);
		rotationMins.ra = degGetMins(rotation.ra);
		rotationSecs.ra = degGetSecs(rotation.ra);

		rotationDegs.dec = degGetDegs(rotation.dec);
		rotationMins.dec = degGetMins(rotation.dec);
		rotationSecs.dec = degGetSecs(rotation.dec);

		rotationDegs.roll = degGetDegs(rotation.roll);
		rotationMins.roll = degGetMins(rotation.roll);
		rotationSecs.roll = degGetSecs(rotation.roll);

		elems.textboxRADeg.value = rotationDegs.ra.toString();
		elems.textboxRAMin.value = rotationMins.ra.toString();
		elems.textboxRASec.value = rotationSecs.ra.toString();

		elems.textboxDecDeg.value = rotationDegs.dec.toString();
		elems.textboxDecMin.value = rotationMins.dec.toString();
		elems.textboxDecSec.value = rotationSecs.dec.toString();

		elems.textboxRollDeg.value = rotationDegs.roll.toString();
		elems.textboxRollMin.value = rotationMins.roll.toString();
		elems.textboxRollSec.value = rotationSecs.roll.toString();
	}


	function updateQNorm()
	{
		if (!quat.checkValue())
			return;

		qNorm = Math.sqrt(quat.w * quat.w + quat.x * quat.x + quat.y * quat.y + quat.z * quat.z);
	}


	function calculateAngleFromDegMinSec(deg, min, sec)
	{
		const minQ = 1 / 60;
		const secQ = 1 / 3600;
		const sign = deg < 0 ? -1 : 1;
		return (!checkNumberValue(deg) 
			|| !checkNumberValue(min)
			|| !checkNumberValue(sec)) ? null : 
			sign * (Math.abs(deg) + min * minQ + sec * secQ);
	}


	function updateButtonSetSpeedStateState()
	{
		elems.buttonSetSpeedState.disabled = !checkNumberValue(angleSpeed, false) 
			|| !checkNumberValue(angleAcceleration, false);
	}


	const checkCalibProgress = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("izn.is_calib_running", null, (j) =>
		{
			elems.trCalibProgress.hidden = !j;
			elems.buttonStarsCalib.disabled = j;

			if (!j) {
				this.continueTimeout(global.telemUpdateInterval);
				return;
			}

			sendJsonRPCRequest("izn.get_calib_progress", null, (j) =>
			{
				elems.progressCalib.value = elems.progressCalib.max * j;
				elems.labelCalibProgress.textContent = sprintf("%0.3d", Math.trunc(j * 100));
				this.continueTimeout(global.telemUpdateInterval);
			});
		});
	});


	const getCurrentState = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("izn.get_current_position", null, (j) =>
		{
			j.ra = degAbs(j.ra);
			j.dec = degAbs(j.dec);
			j.roll = degAbs(j.roll);

			elems.labelRADeg.textContent = sprintf("%03d", degGetDegs(j.ra));
			elems.labelRAMin.textContent = sprintf("%02d", degGetMins(j.ra));
			elems.labelRASec.textContent = sprintf("%02d", degGetSecs(j.ra));

			elems.labelDecDeg.textContent = sprintf("%03d", degGetDegs(j.dec));
			elems.labelDecMin.textContent = sprintf("%02d", degGetMins(j.dec));
			elems.labelDecSec.textContent = sprintf("%02d", degGetSecs(j.dec));

			elems.labelRollDeg.textContent = sprintf("%03d", degGetDegs(j.roll));
			elems.labelRollMin.textContent = sprintf("%02d", degGetMins(j.roll));
			elems.labelRollSec.textContent = sprintf("%02d", degGetSecs(j.roll));
		});

		batch.pushRequest("izn.get_current_acceleration", null, (j) =>
		{
			elems.tdAcceleration.textContent = j;
		});

		batch.pushRequest("izn.get_current_speed", null, (j) =>
		{
			elems.tdSpeed.textContent = j;
		});

		batch.pushRequest("izn.get_moon_position", null, (j) =>
		{
			moonPosition.ra 	= degAbs(j.ra);
			moonPosition.dec 	= degAbs(j.dec);
			elems.tdMoonRA.textContent 	= moonPosition.ra.toString();
			elems.tdMoonDec.textContent = moonPosition.dec.toString();
		});

		batch.pushRequest("izn.get_sun_position", null, (j) =>
		{
			sunPosition.ra 	= degAbs(j.ra);
			sunPosition.dec = degAbs(j.dec);
			elems.tdSunRA.textContent 	= sunPosition.ra.toString();
			elems.tdSunDec.textContent 	= sunPosition.dec.toString();
		});

		batch.pushRequest("izn.get_adjobjs_state", null, (j) =>
		{
			j.forEach((v, i) =>
			{
				v.position.ra = degAbs(v.position.ra);
				v.position.dec = degAbs(v.position.dec);

				adjObjs[i] = v;

				adjObjsRATds[i].textContent 	= v.position.ra.toString();
				adjObjsDecTds[i].textContent 	= v.position.dec.toString();
				adjObjsMagTds[i].textContent 	= v.mag.toString();
				adjObjsVelTds[i].textContent 	= v.vel.toString();
			});
		});

		batch.flush(() => 
		{
			this.continueTimeout(global.telemUpdateInterval); 
		});
	});


	elems.buttonStarsCalib.onclick = (e) => 
	{
		sendJsonRPCRequest("izn.start_calib", null, (j) => 
		{ 
		//	checkCalibProgress.run(); 
		});
	};


	elems.textboxAngleSpeed.oninput = (e) => 
	{
		angleSpeed = textboxFloatValueGuard(e.target, null, 100, 0);
		updateButtonSetSpeedStateState();
	};


	elems.textboxAngleAcceleration.oninput = (e) => 
	{
		angleAcceleration = textboxFloatValueGuard(e.target, null, 100, 0);
		updateButtonSetSpeedStateState();
	};


	elems.buttonSetSpeedState.onclick = (e) => 
	{
		if (!checkNumberValue(angleSpeed, true) 
			|| !checkNumberValue(angleAcceleration, true))
			return;

		sendJsonRPCRequest("izn.set_speed_state", { speed: angleSpeed, acceleration: angleAcceleration });
	};


	function updateTextboxesAngleState()
	{
		elems.textboxRADeg.disabled = !checkAngles;
		elems.textboxRAMin.disabled = !checkAngles;
		elems.textboxRASec.disabled = !checkAngles;
		elems.textboxDecDeg.disabled = !checkAngles;
		elems.textboxDecMin.disabled = !checkAngles;
		elems.textboxDecSec.disabled = !checkAngles;
		elems.textboxRollDeg.disabled = !checkAngles;
		elems.textboxRollMin.disabled = !checkAngles;
		elems.textboxRollSec.disabled = !checkAngles;
	}


	function updateTextboxesQuatState()
	{
		elems.textboxQ0.disabled = !checkQuat;
		elems.textboxQ1.disabled = !checkQuat;
		elems.textboxQ2.disabled = !checkQuat;
		elems.textboxQ3.disabled = !checkQuat;
	}


	function updateRA()
	{
		rotation.ra = calculateAngleFromDegMinSec(rotationDegs.ra, rotationMins.ra, rotationSecs.ra);
		updateButtonRotateStartState();
	}


	function updateDec()
	{
		rotation.dec = calculateAngleFromDegMinSec(rotationDegs.dec, rotationMins.dec, rotationSecs.dec);
		updateButtonRotateStartState();
	}


	function updateRoll()
	{
		rotation.roll = calculateAngleFromDegMinSec(rotationDegs.roll, rotationMins.roll, rotationSecs.roll);
		updateButtonRotateStartState();
	}


	function updateButtonApplyMoonState()
	{
		elems.buttonApplyMoon.disabled = !moonPositionInput.checkValue();
	}


	function updateButtonApplySunState()
	{
		elems.buttonApplySun.disabled = !sunPositionInput.checkValue();
	}


	function updateButtonApplyAdjObjsState()
	{
		let isCorrect = true;

		for (let i = 0; i < adjObjsCount; ++i) {
			if (!adjObjsInput[i].checkValue()) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonApplyAdjObjs.disabled = !isCorrect;
	}


	elems.checkboxAngles.oninput = (e) => 
	{
		checkAngles = e.target.checked;
		updateTextboxesAngleState();
	};


	elems.checkboxQuat.oninput = (e) => 
	{
		checkQuat = e.target.checked;
		updateTextboxesQuatState();
	};


	elems.textboxRADeg.oninput = (e) => 
	{
		rotationDegs.ra = textboxIntValueGuard(e.target, null, true, null, 359, -359);
		updateRA();
		updateQuatFromDegs();
	};


	elems.textboxRAMin.oninput = (e) => 
	{
		rotationMins.ra = textboxIntValueGuard(e.target, null, false, null, 59, 0);
		updateRA();
		updateQuatFromDegs();
	};


	elems.textboxRASec.oninput = (e) => 
	{
		rotationSecs.ra = textboxFloatValueGuard(e.target, null, null, 0, 60, null);
		updateRA();
		updateQuatFromDegs();
	};


	elems.textboxDecDeg.oninput = (e) => 
	{
		rotationDegs.dec = textboxIntValueGuard(e.target, null, false, null, 90, -90);

		if (Math.abs(rotationDegs.dec) === 90) {
			elems.textboxDecMin.value = "0";
			elems.textboxDecSec.value = "0";
			elems.textboxDecMin.disabled = true;
			elems.textboxDecSec.disabled = true;
			rotationMins.dec = 0;
			rotationSecs.dec = 0;
		} else {
			elems.textboxDecMin.disabled = false;
			elems.textboxDecSec.disabled = false;
		}

		updateDec();
		updateQuatFromDegs();
	};


	elems.textboxDecMin.oninput = (e) => 
	{
		rotationMins.dec = textboxIntValueGuard(e.target, null, false, null, 59, 0);
		updateDec();
		updateQuatFromDegs();
	};


	elems.textboxDecSec.oninput = (e) => 
	{
		rotationSecs.dec = textboxFloatValueGuard(e.target,	null, null, 0, 60, null);
		updateDec();
		updateQuatFromDegs();
	};


	elems.textboxRollDeg.oninput = (e) => 
	{
		rotationDegs.roll = textboxIntValueGuard(e.target, null, true, null, 359, -359);
		updateRoll();
		updateQuatFromDegs();
	};


	elems.textboxRollMin.oninput = (e) => 
	{
		rotationMins.roll = textboxIntValueGuard(e.target, null, false, null, 59, 0);
		updateRoll();
		updateQuatFromDegs();
	};


	elems.textboxRollSec.oninput = (e) => 
	{
		rotationSecs.roll = textboxFloatValueGuard(e.target, null, null, 0, 60, null);
		updateRoll();
		updateQuatFromDegs();
	};


	elems.textboxQ0.oninput = (e) => 
	{
		quat.w = textboxFloatValueGuard(e.target, null, 1, -1);
		updateQNorm();
		updateDegsFromQuat();
		updateButtonRotateStartState();
	};


	elems.textboxQ1.oninput = (e) => 
	{
		quat.x = textboxFloatValueGuard(e.target, null, 1, -1);
		updateQNorm();
		updateDegsFromQuat();
		updateButtonRotateStartState();
	};


	elems.textboxQ2.oninput = (e) => 
	{
		quat.y = textboxFloatValueGuard(e.target, null, 1, -1);
		updateQNorm();
		updateDegsFromQuat();
		updateButtonRotateStartState();
	};


	elems.textboxQ3.oninput = (e) => 
	{
		quat.z = textboxFloatValueGuard(e.target, null, 1, -1);
		updateQNorm();
		updateDegsFromQuat();
		updateButtonRotateStartState();
	};


	function updateButtonRotateStartState()
	{
		elems.buttonRotateStart.disabled = qNorm === 0 
			|| !checkNumberValue(rotationTime, false) 
			|| !rotation.checkValue();
	}


	elems.textboxRotationTime.oninput = (e) => 
	{
		rotationTime = textboxIntValueGuard(e.target, 4, false);
		updateButtonRotateStartState();
	};


	elems.buttonRotateStart.onclick = (e) => 
	{
		if (!rotation.checkValue())
			return;

		sendJsonRPCRequest("izn.rotate_to", { angle: rotation, time: rotationTime });
	};


	elems.textboxMoonRA.oninput = (e) => 
	{
		moonPositionInput.ra = textboxFloatValueGuard(e.target, null, null, 0, 360, null);
		updateButtonApplyMoonState();
	};


	elems.textboxMoonDec.oninput = (e) => 
	{
		moonPositionInput.dec = textboxFloatValueGuard(e.target, null, null, 0, 360, null);
		updateButtonApplyMoonState();
	};


	elems.buttonApplyMoon.onclick = (e) => {};

	elems.textboxSunRA.oninput = (e) => 
	{
		sunPositionInput.ra = textboxFloatValueGuard(e.target, null, null, 0, 360, null);
		updateButtonApplySunState();
	};


	elems.textboxSunDec.oninput = (e) => 
	{
		sunPositionInput.dec = textboxFloatValueGuard(e.target, null, null, 0, 360, null);
		updateButtonApplySunState();
	};


	function updateButtonSetAxisRotatingState()
	{
		elems.buttonSetAxisRotating.disabled = !axisRotating.checkValue()
			|| !checkNumberValue(axisRotatingSpeed); 
	};


	elems.textboxSetAxisRotatingX.oninput = (e) =>
	{
		axisRotating.x = textboxFloatValueGuard(e.target);
		updateButtonSetAxisRotatingState();
	};


	elems.textboxSetAxisRotatingY.oninput = (e) =>
	{
		axisRotating.y = textboxFloatValueGuard(e.target);
		updateButtonSetAxisRotatingState();
	};


	elems.textboxSetAxisRotatingZ.oninput = (e) =>
	{
		axisRotating.z = textboxFloatValueGuard(e.target);
		updateButtonSetAxisRotatingState();
	};


	elems.textboxSetAxisRotatingSpeed.oninput = (e) =>
	{
		axisRotatingSpeed = textboxFloatValueGuard(e.target);
		updateButtonSetAxisRotatingState();
	};


	elems.buttonSetAxisRotating.onclick = (e) =>
	{
		if (!axisRotating.checkValue() || !checkNumberValue(axisRotatingSpeed))
			return;

		sendJsonRPCRequest("izn.set_axis_rotating", { axis: axisRotating, angle_speed: axisRotatingSpeed });
	};


	elems.checkboxStars.oninput = (e) =>
	{
		showStars = e.target.checked;

		sendJsonRPCRequest(showStars ? "izn.enable_malevich" : "izn.disable_malevich", null, null, (j) =>
		{
			showStars = !showStars;
			e.target.checked = showStars;
		});
	};


	elems.checkboxMoon.oninput = (e) =>
	{
		showMoon = e.target.checked;
		elems.trMoon.hidden = !showMoon;

		sendJsonRPCRequest(showMoon ? "izn.enable_moon" : "izn.disable_moon", null, null, (j) =>
		{
			showMoon = !showMoon;
			e.target.checked = showMoon;
			elems.trMoon.hidden = !showMoon;
		});
	};


	elems.checkboxSun.oninput = (e) =>
	{
		showSun = e.target.checked;
		elems.trSun.hidden = !showSun;

		sendJsonRPCRequest(showSun ? "izn.enable_sun" : "izn.disable_sun", null, null, (j) =>
		{
			showSun = !showSun;
			e.target.checked = showSun;
			elems.trSun.hidden = !showSun;
		});
	};


	elems.checkboxAdjObjs.oninput = (e) =>
	{
		showAdjObjs = e.target.checked;
		elems.trAdjObjs.hidden = !showAdjObjs;

		sendJsonRPCRequest(showAdjObjs ? "izn.enable_odjobj" : "izn.disable_odjobj", null, null, (j) =>
		{
			showAdjObjs = !showAdjObjs;
			e.target.checked = showAdjObjs;
			elems.trAdjObjs.hidden = !showAdjObjs;
		});
	};


	elems.buttonApplyMoon.onclick = (e) => 
	{
		if (!moonPositionInput.checkValue()) 
			return;

		sendJsonRPCRequest("izn.apply_moon", moonPositionInput);
	};


	elems.buttonApplySun.onclick = (e) => 
	{
		if (!sunPositionInput.checkValue()) 
			return;

		sendJsonRPCRequest("izn.apply_sun", sunPositionInput);
	};


	elems.buttonApplyAdjObjs.onclick = (e) =>
	{
		for (let i = 0; i < adjObjsCount; ++i) {
			if (!adjObjsInput[i].checkValue()) {
				return;
			}
		}

		sendJsonRPCRequest("izn.apply_adjobjs", adjObjsInput);
	};


	elems.buttonAdjObjsCurrentToTextboxes.onclick = (e) => 
	{
		adjObjsRATextboxes.forEach((v, i) =>
		{
			const oi = adjObjsInput[i];
			const o = adjObjs[i];

			oi.position.ra = o.position.ra;
			v.value = oi.position.ra.toString();
			oi.position.ra = textboxFloatValueGuard(v);
		});

		adjObjsDecTextboxes.forEach((v, i) =>
		{
			const oi = adjObjsInput[i];
			const o = adjObjs[i];

			oi.position.dec = o.position.dec;
			v.value = oi.position.dec.toString();
			oi.position.dec = textboxFloatValueGuard(v);
		});

		adjObjsMagTextboxes.forEach((v, i) => 
		{
			const oi = adjObjsInput[i];
			const o = adjObjs[i];

			oi.mag = o.mag;
			v.value = oi.mag.toString();
			oi.mag = textboxFloatValueGuard(v);
		});

		adjObjsVelTextboxes.forEach((v, i) =>
		{
			const oi = adjObjsInput[i];
			const o = adjObjs[i];

			oi.vel = o.vel;
			v.value = oi.vel.toString();
			oi.vel = textboxFloatValueGuard(v);
		});

		updateButtonApplyAdjObjsState();
	};

	updateRA();
	updateDec();
	updateRoll();
	updateQuatFromDegs();
	updateButtonRotateStartState();
	updateTextboxesAngleState();
	updateTextboxesQuatState();
	updateButtonSetSpeedStateState();
	updateButtonApplyMoonState();
	updateButtonApplySunState();
	updateButtonSetAxisRotatingState();

	const checkMalevichState = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("izn.is_malevich_enabled", null, (j) =>
		{
			if (j === null) {
				this.continueTimeout(300);
			} else {
				const elem = elems.checkboxStars;
				// разблокировать, если нужно дать возможность включать\выключать звёзды
				elem.disabled = false;
				elem.checked = j;
			}
		});
	});


	const checkMoonState = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("izn.is_moon_enabled", null, (j) =>
		{
			if (j === null) {
				this.continueTimeout(300);
			} else {
				const elem = elems.checkboxMoon;
				elem.disabled = false;
				elem.checked = j;
				elems.trMoon.hidden = !j;
			}
		});
	});


	const checkSunState = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("izn.is_sun_enabled", null, (j) =>
		{
			if (j === null) {
				this.continueTimeout(300);
			} else {
				const elem = elems.checkboxSun;
				elem.disabled = false;
				elem.checked = j;
				elems.trSun.hidden = !j;
			}
		});
	});


	const checkAdjObjsState = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("izn.are_adjobjs_enabled", null, (j) =>
		{
			j = true;

			if (j === null) {
				this.continueTimeout(300);
			} else {
				const elem = elems.checkboxAdjObjs;
				elem.disabled = false;
				elem.checked = j;
				elems.trAdjObjs.hidden = !j;
			}
		});
	});


	const checkIzn = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("izn.is_available", null, (j) => 
		{
			j = true;

			elems.trAll.hidden = !j;
			elems.trIznIsNotAvailableMessage.hidden = j;

			if (j) {
				checkCalibProgress.run();
				checkMalevichState.run();
				checkMoonState.run();
				getCurrentState.run();
				checkSunState.run();
				checkAdjObjsState.run();
			} else {
				this.continueTimeout(300);
			}
		});
	});

	function initializeAdjObjsHTML(adjObjCount)
	{
		const container = elems.tbodyAdjObjs;

		const addTrCounter = (root, i) =>
		{
			const tr = document.createElement("tr");
			const tds = createElemsArray("td", 4);

			tds[0].style.textAlign = "left";
			tds[0].style.fontSize = "18px";
			tds[0].style.fontWeight = "bold";
			tds[0].textContent = sprintf("%d)", i + 1);
			tds.forEach((v) => 
			{
				v.style.fontFamily = "girloy-bold";
				v.style.backgroundColor = "white"; 
				tr.appendChild(v);
			});
			root.appendChild(tr);
		};

		const addAdjObjTrGeneric = (root, head, units, textboxOnInput, processTextbox = null, processTd = null) =>
		{
			const tr = document.createElement("tr");
			const tds = createElemsArray("td", 4);

			tds[0].textContent = head;
			tds[0].style.zidth  = "18ch";
		
			const textbox = document.createElement("input");

			textbox.type = "text";
			textbox.value = "0";
			textbox.oninput = textboxOnInput;
			tds[1].appendChild(textbox);
			tds[2].textContent = "0.000";
			tds[3].textContent = units;
			tds[3].style.zidth = "5ch";
			tds.forEach((v) => { tr.appendChild(v); });
			root.appendChild(tr);

			if (processTextbox) {
				processTextbox(textbox);
			}

			if (processTd) {
				processTd(tds[2]);
			}
		};

		const addAdjObjTrRA = (root, i) =>
		{
			addAdjObjTrGeneric(root, "R.A.:", "°", (e) =>
			{
				adjObjsInput[i].position.ra = textboxFloatValueGuard(e.target);
				updateButtonApplyAdjObjsState();
			}, (textbox) =>
			{
				adjObjsRATextboxes[i] = textbox;
			}, (td) => 
			{
				adjObjsRATds[i] = td;	
			});
		};

		const addAdjObjTrDec = (root, i) =>
		{
			addAdjObjTrGeneric(root, "Dec.:", "°", (e) =>
			{
				adjObjsInput[i].position.dec = textboxFloatValueGuard(e.target);
				updateButtonApplyAdjObjsState();
			}, (textbox) =>
			{
				adjObjsDecTextboxes[i] = textbox;
			}, (td) => 
			{
				adjObjsDecTds[i] = td;	
			});
		};

		const addAdjObjTrMag = (root, i) =>
		{
			addAdjObjTrGeneric(root, "Mag.:", "", (e) =>
			{
				adjObjsInput[i].mag = textboxFloatValueGuard(e.target);
				updateButtonApplyAdjObjsState();
			}, (textbox) =>
			{
				adjObjsMagTextboxes[i] = textbox;
			}, (td) => 
			{
				adjObjsMagTds[i] = td;	
			});
		};

		const addAdjObjTrVel = (root, i) =>
		{
			addAdjObjTrGeneric(root, "Vel.:", "°/с", (e) =>
			{
				adjObjsInput[i].vel = textboxFloatValueGuard(e.target);
				updateButtonApplyAdjObjsState();
			}, (textbox) =>
			{
				adjObjsVelTextboxes[i] = textbox;
			}, (td) => 
			{
				adjObjsVelTds[i] = td;	
			});
		};

		adjObjsInput.length = adjObjCount;

		for (let i = 0; i < adjObjCount; ++i) {
			adjObjsInput[i] = new AdjObj();
			addTrCounter(container, i);
			addAdjObjTrRA(container, i);
			addAdjObjTrDec(container, i);
			addAdjObjTrMag(container, i);
			addAdjObjTrVel(container, i);
		}
	}

	sendJsonRPCRequest("izn.get_adjobj_count", null, (j) =>
	{
		adjObjsCount = j;
		initializeAdjObjsHTML(j);
		checkIzn.run();
	});

	updateQNorm();

	
}

// код из старого сервера App
function shcolsend() {

	shidMas = ['shks', 'shka', 'shke'];
	shvalMas = [0, 0, 0];
	//shstr = '';

	for (var i = 0; i < shidMas.length; ++i) {
		shvalMas[i] = parseFloat(document.getElementById(shidMas[i]).value.replace(',', '.'));
		if (isNaN(shvalMas[i])) {
			shvalMas[i] = 0;
		}
		document.getElementById(shidMas[i]).value = 0;
		document.getElementById(shidMas[i]).value = shvalMas[i];
		//xzxz = (new Int32Array([Math.floor(shvalMas[i] * 131072.0 + 0.5)]))[0];

		//shstr += (xzxz).toString() + ' ';
		//shstr += (shvalMas[i]).toString() + ' ';
	}

	const [specular, diffuse, emissive] = shvalMas;

	sendJsonRPCRequest("izn.setup_shuttle_lighting", { specular, diffuse, emissive });
}

function shcoordsend() {

	shidMas = ['shcx', 'shcy', 'shcz', 'shvx', 'shvy', 'shvz', 'shq0', 'shq1', 'shq2', 'shq3', 'shwx', 'shwy', 'shwz'];
	shvalMas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	//shstr = '';

	for (var i = 0; i < shidMas.length; ++i) {
		shvalMas[i] = parseFloat(document.getElementById(shidMas[i]).value.replace(',', '.'));

		if (isNaN(shvalMas[i])) {
			shvalMas[i] = 0;
		}
		document.getElementById(shidMas[i]).value = 0;
		document.getElementById(shidMas[i]).value = shvalMas[i];
	}

	sendJsonRPCRequest("izn.setup_shuttle_position", { 
		coords: new Vector3(shvalMas[0], shvalMas[1], shvalMas[2]),
		moveSpeed: new Vector3(shvalMas[3], shvalMas[4], shvalMas[5]),
		quaternion: new Quaternion(shvalMas[6], shvalMas[7], shvalMas[8], shvalMas[9]),
		rotationSpeed: new Vector3(shvalMas[10], shvalMas[11], shvalMas[12]),
	});
}