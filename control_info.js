function main()
{
	const elems = getAJAXElemsWithID();
	const telemName = "controlInfo";
	let textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);

	let DeviceStatus = {
		MSW: null,
		LSW: null,
		full: null
	};

	function updateVMEM_or_VADC(elem, j) {
		elem.textContent = j.raw !== null ? sprintf("%.2f", j.raw * 0.0234375) : emptyValueString;
	}
	
	
	function updateDeviceStatus()
	{
		if (DeviceStatus.LSW == null || DeviceStatus.MSW == null)
			return;
	
		var val = MAKELONG(DeviceStatus.LSW, DeviceStatus.MSW);

		elems.tdDeviceStatus_LSW_0.textContent = boolToHasNoString(DeviceStatus.LSW & 1);
		elems.tdDeviceStatus_LSW_1.textContent = DeviceStatus_2_toString(DeviceStatus.LSW);
		elems.tdDeviceStatus_LSW_2.textContent = ((DeviceStatus.LSW >> 5) & 1) === 0 ? 
			"Штатное функционирование конфигурации ПЛИС" :
			"Не более 10 секунд назад был зафиксирован сбой конфигурации ПЛИСс последующей коррекцией";

		elems.tdDeviceStatus_MSW_0.textContent = boolToYNString(DeviceStatus.MSW & 1);
		elems.tdDeviceStatus_MSW_1.textContent = boolToYNString((DeviceStatus.MSW >> 1) & 1);
		elems.tdDeviceStatus_MSW_2.textContent = boolToHasNoString((DeviceStatus.MSW >> 2) & 1);
		elems.tdDeviceStatus_MSW_3.textContent = ((DeviceStatus.MSW >> 8) & 0xFF).toString();
	}


	function updateQStatus(j)
	{
		if (j.raw !== null) {
			const raw = j.raw;

			elems.tdQStatus_0.textContent = j.formatted;
			elems.tdQStatus_1.textContent = boolToYNString((raw >> 0) & 1 == 1);
			elems.tdQStatus_2.textContent = boolToYNString((raw >> 1) & 1 == 1);
			elems.tdQStatus_3.textContent = boolToYNString((raw >> 2) & 1 == 1);
			elems.tdQStatus_4.textContent = boolToYNString((raw >> 3) & 1 == 1);
			elems.tdQStatus_5.textContent = boolToYNString((raw >> 4) & 1 == 1);
			elems.tdQStatus_6.textContent = ((raw >> 5) & 15).toString();
		} else {
			elems.tdQStatus_0.textContent = emptyValueString;
			elems.tdQStatus_1.textContent = emptyValueString;
			elems.tdQStatus_2.textContent = emptyValueString;
			elems.tdQStatus_3.textContent = emptyValueString;
			elems.tdQStatus_4.textContent = emptyValueString;
			elems.tdQStatus_5.textContent = emptyValueString;
			elems.tdQStatus_6.textContent = emptyValueString;
		}
	}

	function setTelemPeriod(period)
	{
		if (!checkNumberValue(period, true))
			return;

		Pilv.mko.setTelemPeriod(telemName, period);
	}


	function updateButtonSetTelemPeriodState()
	{
		elems.buttonSetTelemPeriod.disabled = !checkNumberValue(textboxSetTelemPeriod_value, false);
	}


	elems.textboxSetTelemPeriod.oninput = (e) =>
	{
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTelemPeriodState();
	};

	elems.textboxSetTelemPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	elems.buttonSetTelemPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTelemPeriod_value, true))
			return;
			
		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	const elemsByFieldNames = (() =>
	{
		let result = {};

		for (let key in elems) {
			if (key.substring(0, 2) == "td") {
				let fieldName = key.substring(2);
				result[fieldName] = elems[key];
			}
		}

		return result;
	})();

	const fieldNames = objectToKeyArray(elemsByFieldNames);

	function updateElemTextByJsonRPCResponse(elem, j)
	{
		if (j.formatted) {
			elem.textContent = elem === elems.tdFWVER0 || elem === elems.tdFWVER1 ? j.raw : j.formatted;
		}
	}

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		const updateAllPart1 = () => // нет condition_variable или yield return, чтоб приостановить поток, приходится выкручиваться. Пы.Сы. в 2022 не актуально
		{
			//batch.pushRequest("zd.cmd.can.get_last_preambule", { array_type: 0xFC }, updateWorldTime);
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "PowerUpCnt" },
				function (j) { updateElemTextByJsonRPCResponse(elems.tdPowerUpCnt, j); });
		
				batch.pushRequest("zd.cmd.get_field_value", { name: "ErPowerUpCnt" }, (j) =>
				{
					if (j.raw !== null) {
						let value = j.raw;
						
						elems.tdErPowerUpCnt_user.textContent = LOBYTE(value).toString();
						elems.tdErPowerUpCnt_self.textContent = HIBYTE(value).toString();
					} else {
						elems.tdErPowerUpCnt_user.textContent = emptyValueString;
						elems.tdErPowerUpCnt_self.textContent = emptyValueString;
					}
				});
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM0" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM0, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM1" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM1, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM2" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM2, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM3" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM3, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "DNUMBER" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdDNUMBER, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "FACTNUMBER" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdFACTNUMBER, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "FWVER0" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdFWVER0, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "FWVER1" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdFWVER1, j); });

			batch.pushRequest("zd.cmd.get_field_value", { name: "Project_Status" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdProject_Status, j); });
			
			batch.pushRequest("zd.cmd.get_field_value", { name: "Error_cnt" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdError_cnt, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "CUR_FLASH_page" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdCUR_FLASH_page, j); });

			batch.pushRequest("zd.cmd.get_field_value", { name: "TCMOS" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdTCMOS, j); });

			batch.pushRequest("zd.cmd.get_field_value", { name: "QStatus" }, (j) => 
				{
					if (j.raw !== null) {
						const raw = j.raw;
		
						elems.tdQStatus_0.textContent = j.formatted;
						elems.tdQStatus_1.textContent = boolToYNString((raw >> 0) & 1 == 1);
						elems.tdQStatus_2.textContent = boolToYNString((raw >> 1) & 1 == 1);
						elems.tdQStatus_3.textContent = boolToYNString((raw >> 2) & 1 == 1);
						elems.tdQStatus_4.textContent = boolToYNString((raw >> 3) & 1 == 1);
						elems.tdQStatus_5.textContent = boolToYNString((raw >> 4) & 1 == 1);
						elems.tdQStatus_6.textContent = ((raw >> 5) & 15).toString();
						elems.tdQStatus_7.textContent = ((raw >> 9) & 15).toString();
					} else {
						elems.tdQStatus_0.textContent = emptyValueString;
						elems.tdQStatus_1.textContent = emptyValueString;
						elems.tdQStatus_2.textContent = emptyValueString;
						elems.tdQStatus_3.textContent = emptyValueString;
						elems.tdQStatus_4.textContent = emptyValueString;
						elems.tdQStatus_5.textContent = emptyValueString;
						elems.tdQStatus_6.textContent = emptyValueString;
						elems.tdQStatus_7.textContent = emptyValueString;
					}
				});

			batch.flush(updateAllPart2);
		}

		let updateAllPart2 = () =>
		{
			batch.pushRequest("zd.cmd.get_field_value", { name: "DeviceStatus_MSW" }, (j) =>
			{
				DeviceStatus.MSW = j.raw;
			});
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "DeviceStatus_LSW" }, (j) =>
			{
				DeviceStatus.LSW = j.raw;
			});

			batch.pushRequest("zd.cmd.mko.is_telem_enabled", [telemName], j => {
				elems.checkboxTelem.checked = j;
				elems.buttonSetTelemPeriod.disabled = !(j && checkNumberValue(textboxSetTelemPeriod_value));
				elems.textboxSetTelemPeriod.disabled = !j;
			});

			batch.flush(() =>
			{
				updateDeviceStatus();
				this.continueTimeout(global.telemUpdateInterval);
			});
		}

		updateAllPart1();
	});

	Pilv.mko.getTelemPeriod(telemName, period => {
		const seconds = period / 1000;
		elems.textboxSetTelemPeriod.value = seconds.toString();
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);
	});

	elems.checkboxTelem.oninput = e => (e.currentTarget.checked ? Pilv.mko.enableTelem : Pilv.mko.disableTelem)(telemName);

	updateButtonSetTelemPeriodState();
	loop.run();
}