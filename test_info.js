function main()
{
	const tstDataFieldsCount = 30;
	const elems = getAJAXElemsWithID();
	let tstDataTextboxes = [];
	let tstDataTds = [];
	let tstData = [];
	let tstDataFieldNames = [];
	let textboxSetTIPPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTIPPeriod);

	function initializeTstDataHTML()
	{
		const container = elems.tbodyTstData;

		tstDataTextboxes.length = tstDataFieldsCount;
		tstDataTds.length = tstDataFieldsCount;
		tstData.length = tstDataFieldsCount;
		tstDataFieldNames.length = tstDataFieldsCount;

		for (let i = 0; i < tstDataFieldsCount; ++i) {
			const tr = document.createElement("tr");
			const tds = createElemsArray("td", 3);
			const textbox = document.createElement("input");
			textbox.type = "text";
			tds[0].textContent = sprintf("TST_DATA_%d", i);
			textbox.value = "0";
			textbox.maxLength = 10;
			tds[1].appendChild(textbox);
			tds.forEach((v) => { tr.appendChild(v); });
			container.appendChild(tr);
			tstDataTextboxes[i] = textbox;
			tstData[i] = textboxIntValueGuard(textbox, 2, false);
			tstDataTds[i] = tds[2];
			tstDataFieldNames[i] = tds[0].textContent;

			textbox.oninput = (e) =>
			{
				tstData[i] = textboxIntValueGuard(e.target, 2, false);
			};
		}
	}

	
	function updateButtonSendTIZState()
	{
		let isCorrect = true;

		for (let i = 0; i < tstData.length; ++i) {
			if (!checkNumberValue(tstData[i], false)) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonSendTIZ.disabled = !isCorrect;
	}


	function updateButtonSetTIPPeriodState()
	{
		elems.buttonSetTIPPeriod.disabled = !checkNumberValue(textboxSetTIPPeriod_value, false);
	}


	elems.buttonSendTIZ.onclick = (e) =>
	{
		for (let i = 0; i < tstData.length; ++i) {
			if (!checkNumberValue(tstData[i])) {
				return;
			}
		}

		Pilv.sendTestData(tstData);
	}


	elems.textboxSetTIPPeriod.oninput = (e) =>
	{
		textboxSetTIPPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTIPPeriodState();
	};


	textboxSetTIPPeriod.onkeyup = (e) => 
	{
		if (e.key === "Enter" && checkNumberValue(textboxSetTIPPeriod_value, false)) {
			Pilv.can.setArrayTypePeriod(0xF3, textboxSetTIPPeriod_value);
		}
	};


	elems.buttonSetTIPPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTIPPeriod_value))
			return;

		Pilv.can.setArrayTypePeriod(0xF3, textboxSetTIPPeriod_value);
	};


	const loop = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("zd.cmd.get_fields_value", { names: tstDataFieldNames }, (j) =>
		{
			for (let i = 0; i < j.length; ++i) {
				tstDataTds[i].textContent = j[i].raw !== null ? j[i].formatted : emptyValueString;
			}
		});

		this.continueTimeout(global.telemUpdateInterval);
	});

	initializeTstDataHTML();
	updateButtonSendTIZState();
	updateButtonSetTIPPeriodState();
	loop.run();
}