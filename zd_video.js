function main()
{
	const elems = getAJAXElemsWithID();
	const elemFoundObject = elems.stars.children[0];
	const isVideoRecordingAvailable = false;
	let IRQ_MASK_REQ = null;
	let localClientIDString = null;

	let textboxSetTZPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTZPeriod);
	let textboxStarsCount_value = textboxIntValueGuard(elems.textboxStarsCount, 2, false);
	let videoIsPlaying = true;
	let isVideoLoaded = false;
	let starsCount = checkNumberValue(textboxStarsCount_value, false) ? textboxStarsCount_value : 0;
	let maxStarsCount = 80;
	let videoIP = null;	
	let zdFrameSize = null;

	const effectSlidersMouseState = {
		brightness: false, 
		contrast: false,
		gamma: false
	};

	const videoTryLoad = new AsyncLoopFunc(function()
	{
		const targetFrameSize = new Vector2(640, 480);
		let frameSize = new Vector2();
		const batch = new JsonRPCBatch();
		batch.pushRequest("video.disable_frame_size_as_a_source");
		batch.pushRequest("video.set_frame_size", targetFrameSize, (j) => 
		{
			frameSize = j;

			elems.video.width = frameSize.x;
			elems.video.height = frameSize.y;
		});

		batch.pushRequest("video.get_server_ip", null, (j) =>
		{
			if (localClientIDString === null) {
				localClientIDString = generateStringID();
			}

			j = j.substring(j.search(":") + 1);
			
			if (j !== videoIP) {
				videoIP = j;
				elems.video.src = `http://${window.location.hostname}:${videoIP}/?id=${localClientIDString}`;
			}
			//elems.video.src = sprintf("http://%s/?id=%s", videoIP, localClientIDString);
		});

		batch.flush();
		this.continueTimeout(1000);
	}, false, () =>
	{
		if (localClientIDString === null)
			return;

		sendJsonRPCRequest("video.disconnect_client", { id: localClientIDString }, null, null, null, false);
		localClientIDString = null;
	});


	function updateStarText(elemStar, jStar, isObject)
	{
		if (!elemStar)
			return;

		const elemSao 	= elemStar.children[1];
		const elemName 	= elemStar.children[2];
		if (!isObject) {
			if (jStar && jStar.info) {
				elemSao.textContent 	= jStar.info.sao.toString();
				elemName.textContent 	= jStar.info.name;
			} else {
				elemSao.textContent 	= null;
				elemName.textContent 	= null;
			}
		} else {
			elemSao.textContent 	= jStar.num;
		}
	}


	function rebuildStarsTable()
	{
		const t = elems.starsTable;

		if (starsCount > (t.rows.length + 1)) {
			const toAdd = starsCount - (t.rows.length - 1);

			for (let i = 0; i < toAdd; ++i) {
				const row = t.insertRow(t.rows.length - 1);
				const cellNo = row.insertCell(0);
				const cellX = row.insertCell(1);
				const cellY = row.insertCell(2);
				const cellVal = row.insertCell(3);
				const cellNum = row.insertCell(4);
	
				cellNo.textContent = sprintf("%d", t.rows.length - 1);
				cellX.textContent = emptyValueString;
				cellY.textContent = emptyValueString;
				cellVal.textContent = emptyValueString;

				const star = document.createElement("div");
				const labelStarIcon = document.createElement("label");
				const labelStarSao = document.createElement("label");
				const labelStarName = document.createElement("label");

				const labelStarSaoCell = document.createElement("label");
				const labelStarNameCell = document.createElement("label");
				
				labelStarIcon.className = "not-star-icon";
				labelStarSao.className = "star-sao";
				labelStarName.className = "star-name";

				labelStarSaoCell.className = "star-sao";
				labelStarNameCell.className = "star-name-cell";

				labelStarIcon.textContent = "+";

				star.className = "star";
				star.hidden = true;

				star.appendChild(labelStarIcon);
				star.appendChild(labelStarSao);
				star.appendChild(labelStarName);

				cellNum.appendChild(labelStarSaoCell);
				cellNum.appendChild(labelStarNameCell);
				
				elems.stars.appendChild(star);
			}
		} else {
			const toRemove = t.rows.length - starsCount - 1;

			for (let i = 0; i < toRemove; ++i) {
				t.deleteRow(t.rows.length - 2);
				elems.stars.removeChild(elems.stars.lastChild);
			}
		}
	}


	const updateStars = new AsyncLoopFunc(function()
	{
		const t = elems.starsTable;
		const cont = () => { this.continueTimeout(global.telemUpdateInterval); }
		
		const batch = new JsonRPCBatch();
		const uiImageSize = new Vector2(elems.video.clientWidth || elems.videoContainer.clientWidth, elems.video.clientHeight || elems.videoContainer.clientHeight);
		const uiImagePosition = new Vector2(
			elems.video.x === 0 ? elems.videoError.x : elems.video.x,
			elems.video.y === 0 ? elems.videoError.y : elems.video.y
		);

		const sizeQ = new Vector2(uiImageSize.x / zdFrameSize.x, uiImageSize.y / zdFrameSize.y);

		if (starsCount !== 0) {
			batch.pushRequest("zd.cmd.get_fields_value", {names: [ "NumOfObj", "NumOfRecObj" ]}, (j) =>
			{
				elems.tdNumOfObj.textContent 	= j[0].raw !== null ? j[0].formatted : emptyValueString;
				elems.tdNumOfRecObj.textContent = j[1].raw !== null ? j[1].formatted : emptyValueString;
			});

			batch.pushRequest("zd.cmd.get_stars", null, (j) =>
			{
				if (zdFrameSize.x === null || zdFrameSize.y === null) {
					if (!getZDFrameSize.isRunning)
						getZDFrameSize.run();

					return;
				}

				maxStarsCount = j.length;

				const toIterate = Math.min(j.length, t.rows.length - 1);

				for (let i = 0; i < toIterate; ++i) {
					const star = j[i];
					const row = t.rows[i];
					const cell = {
						x: row.cells[1],
						y: row.cells[2],
						val: row.cells[3],
						num: row.cells[4],
					};

					const zdPosition = (star.x > zdFrameSize.x) || 
						(star.x < 0) || 
						(star.y > zdFrameSize.y) || 
						(star.y < 0) ? null : new Vector2(star.x, star.y);

					cell.x.textContent 	= sprintf("%.2f", star.x);
					cell.y.textContent 	= sprintf("%.2f", star.y);
					cell.val.textContent = sprintf("%d", star.val);

					if (star.info) {
						cell.num.children[0].textContent = star.info.sao.toString();
						cell.num.children[1].textContent = star.info.name;
					} else {
						cell.num.children[0].textContent = emptyValueString;
						cell.num.children[1].textContent = emptyValueString;
					}

					const elemStar = elems.stars.children[i + 1];
					elemStar.children[0].className = star.info ? "star-icon" : "not-star-icon";

					if (zdPosition != null) {
						elemStar.hidden = false;
						elemStar.style.left = sprintf("%dpx", uiImagePosition.x + scrollX + (zdPosition.x * sizeQ.x));
						elemStar.style.top 	= sprintf("%dpx", uiImagePosition.y + scrollY + (zdPosition.y * sizeQ.y));
					} else {
						elemStar.hidden = true;	
					}

					updateStarText(elemStar, star, false);
				}
			});
		}

		batch.pushRequest("zd.cmd.get_found_object", null, (j) =>
		{
			const row = elems.starsTable.rows[elems.starsTable.rows.length - 1];
			const cell = {
				x: row.cells[1],
				y: row.cells[2],
				val: row.cells[3],
				num: row.cells[4]
			};

			if (j === null || j.val === 0) {
				cell.x.textContent = null;
				cell.y.textContent = null;
				cell.val.textContent = null;
				elemFoundObject.hidden = true;

				for (let i = 0; i < cell.num.children.length; ++i) {
				    cell.num.children[i].textContent = null;	
				}

				return;
			}

			const zdPosition = (j.x > zdFrameSize.x) || 
					(j.x < 0) || 
					(j.y > zdFrameSize.y) || 
					(j.y < 0) ? null : new Vector2(j.x, j.y);

			cell.x.textContent 	= sprintf("%.2f", j.x);
			cell.y.textContent 	= sprintf("%.2f", j.y);
			cell.val.textContent = sprintf("%d",  j.val);
			cell.num.textContent = j.num.toString();

			if (zdPosition != null) {
				elemFoundObject.hidden = false;
				elemFoundObject.style.left = sprintf("%dpx", uiImagePosition.x + scrollX + (zdPosition.x * sizeQ.x));
				elemFoundObject.style.top 	= sprintf("%dpx", uiImagePosition.y + scrollY + (zdPosition.y * sizeQ.y));
			} else {
				elemFoundObject.hidden = true;	
			}

			updateStarText(elemFoundObject, j, true);
		});

		batch.flush(cont);
	});


	function updateLabelPlayPause()
	{
		elems.labelPlayPause.textContent = videoIsPlaying ? "Остановить" : "Возобновить";
	}


	function updateVideoPlayingState()
	{
		if (localClientIDString === null)
			return;

		sendJsonRPCRequest(
			sprintf("video.%s", videoIsPlaying ? "play" : "pause"), 
			{ id: localClientIDString }, null, null, null, false
		);
	}

	elems.sliderBrightness.oninput = (e) => 
	{
		const val = e.target.valueAsNumber;
		sendJsonRPCRequest("video.set_brightness", [val / e.target.max]);
		elems.tdBrightness.textContent = e.target.value;
	};

	elems.sliderContrast.oninput = (e) =>
	{
		const val = e.target.valueAsNumber;
		sendJsonRPCRequest("video.set_contrast", [val / e.target.max]);
		elems.tdContrast.textContent = e.target.value;
	};


	elems.sliderGamma.oninput = (e) =>
	{
		const val = e.target.valueAsNumber;
		sendJsonRPCRequest("video.set_gamma", [val / e.target.max]);
		elems.tdGamma.textContent = e.target.value;
	};


	elems.sliderBrightness.onmousedown 	= (e) => { effectSlidersMouseState.brightness 	= true; };
	elems.sliderContrast.onmousedown 	= (e) => { effectSlidersMouseState.contrast 	= true; };
	elems.sliderGamma.onmousedown 		= (e) => { effectSlidersMouseState.gamma 		= true; };
	elems.sliderBrightness.onmouseup 	= (e) => { effectSlidersMouseState.brightness 	= false; };
	elems.sliderContrast.onmouseup 		= (e) => { effectSlidersMouseState.contrast 	= false; };
	elems.sliderGamma.onmouseup 		= (e) => { effectSlidersMouseState.gamma 		= false; };


	const updateEffectSliders = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		if (!effectSlidersMouseState.brightness) {
			batch.pushRequest("video.get_brightness", null, (j) =>
			{
				elems.sliderBrightness.value = j * elems.sliderBrightness.max;
				elems.tdBrightness.textContent = elems.sliderBrightness.value;
			});
		}

		if (!effectSlidersMouseState.contrast) {
			batch.pushRequest("video.get_contrast", null, (j) =>
			{
				elems.sliderContrast.value = j * elems.sliderContrast.max;
				elems.tdContrast.textContent = elems.sliderContrast.value;
			});
		}

		if (!effectSlidersMouseState.gamma) {
			batch.pushRequest("video.get_gamma", null, (j) =>
			{
				elems.sliderGamma.value = j * elems.sliderGamma.max;
				elems.tdGamma.textContent = elems.sliderGamma.value;
			});
		}

		batch.flush(() =>
		{
			this.continueTimeout(100);
		});
	});


	elems.video.onerror = (e) =>
	{
		elems.videoError.hidden = false;
		elems.video.hidden = true;
		elems.buttonPlayPause.disabled = true;
		elems.sliderBrightness.disabled = true;
		elems.sliderContrast.disabled = true;
		elems.sliderGamma.disabled = true;
		videoIP = "";
	};

	elems.video.onload = (e) => 
	{
		elems.video.hidden = false;
		elems.videoError.hidden = true;
		videoErrorShowed = false;
		isVideoLoaded = true;
		elems.buttonPlayPause.disabled = false;
		elems.sliderBrightness.disabled = false;
		elems.sliderContrast.disabled = false;
		elems.sliderGamma.disabled = false;
		updateVideoPlayingState();
	};


	function updateButtonStarsCountState()
	{
		elems.buttonStarsCount.disabled = !checkNumberValue(textboxStarsCount_value, false);
	}


	function updateStarsCount()
	{
		if (!checkNumberValue(textboxStarsCount_value, false))
			return;

		if (textboxStarsCount_value > maxStarsCount) {
			textboxStarsCount_value = maxStarsCount;
			starsCount = maxStarsCount;
			elems.textboxStarsCount.value = sprintf("%d", maxStarsCount);
		} else {
			starsCount = textboxStarsCount_value;
		}

		rebuildStarsTable();	
	}


	elems.textboxStarsCount.oninput = (e) =>
	{
		textboxStarsCount_value = textboxIntValueGuard(e.target, 2);
		updateButtonStarsCountState();
	};


	elems.textboxStarsCount.onkeyup = (e) =>
	{
		if (e.key == "Enter") {
			updateStarsCount();
		}
	};


	elems.buttonStarsCount.onclick = (e) =>
	{
		updateStarsCount();
	};


	elems.buttonPlayPause.onclick = (e) =>
	{
		if (!isVideoLoaded) 
			return;
		
		videoIsPlaying = !videoIsPlaying;
		updateLabelPlayPause();
		updateVideoPlayingState();
	};


	elems.buttonEnableVideo.onclick = (e) =>
	{
		e.target.disabled = true;
		const enableElem = (j) => { e.target.disabled = false; };

		sendJsonRPCRequest("zd.cmd.enable_video", null, enableElem, enableElem, enableElem);
	};


	elems.buttonDisableVideo.onclick = (e) =>
	{
		e.target.disabled = true;
		const enableElem = (j) => { e.target.disabled = false; };

		sendJsonRPCRequest("zd.cmd.disable_video", null, enableElem, enableElem, enableElem);
	};


	function updateButtonSetTZPeriodState()
	{
		elems.buttonSetTZPeriod.disabled = !checkNumberValue(textboxSetTZPeriod_value, false);
	}


	function setTZPeriod(period)
	{
		if (!checkNumberValue(period, false))
			return;

		Pilv.can.setArrayTypePeriod(0xF0, period);
	}


	elems.buttonSetTZPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTZPeriod_value, true))
			return;

		setTZPeriod(textboxSetTZPeriod_value);
	};


	elems.textboxSetTZPeriod.oninput = (e) =>
	{
		textboxSetTZPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTZPeriodState();
	};


	elems.textboxSetTZPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTZPeriod(textboxSetTZPeriod_value);
	};


	const getZDFrameSize = new AsyncLoopFunc(function()
	{
		const params = { names: [ "Width", "Height" ] };
		const cont = () => { this.continueTimeout(1000); };
		const onGotFrameSize = (j) =>
		{
			zdFrameSize = new Vector2(j[0].raw, j[1].raw);
					
			if (!updateStars.isRunning) {
				updateStars.run();
			}

			if (!videoTryLoad.isRunning) {
				videoTryLoad.run();
			}
		};

		setIRQ_MASK_REQto0And((setIRQ_MASK_REQRrev) => 
		{
			sendJsonRPCRequest("zd.cmd.send_read_request", params, (j) =>
			{
				if (j[0] === null || j[1] === null) {
					printErrorMessage("Ошибка запроса на чтение", "Не удалось считать поля Width и Height");
					cont();
					return;
				}
		
				onGotFrameSize(j);
				sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: 0x46 });
				//setIRQ_MASK_REQRrev();
			}, cont);
		}, cont);

		//sendJsonRPCRequest("zd.get_frame_size", null, (j) =>
		//{
		//	zdFrameSize = j;
//
		//	if (j.x === 0 || j.y === 0) {
		//		cont();
		//		return;
		//	}
		//			
		//	if (!updateStars.isRunning) {
		//		updateStars.run();
		//	}
//
		//	if (!videoTryLoad.isRunning) {
		//		videoTryLoad.run();
		//	}
		//}, cont);
	});

	updateLabelPlayPause();
	updateButtonStarsCountState();

	if (!videoOnly) {
		getZDFrameSize.run();
		rebuildStarsTable();
	}

	updateEffectSliders.run();

	sendJsonRPCRequest("zd.cmd.send_read_request", { name: "IRQ_MASK_REQ" }, j => {
		IRQ_MASK_REQ = j.raw;
		elems.buttonStarsOff.disabled = false;
		elems.buttonStarsOn.disabled = false;
	}, () => {
		elems.buttonStarsOff.disabled = true;
		elems.buttonStarsOn.disabled = true;
	});

	elems.buttonStarsOff.onclick = e => {
		if (!checkNumberValue(IRQ_MASK_REQ, true))
			return;

		const en = () => {
			elems.buttonStarsOff.disabled = false;
			elems.buttonStarsOn.disabled = false;
		};

		const newIMR = IRQ_MASK_REQ & ~2;

		sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: newIMR }, () => {
			IRQ_MASK_REQ = newIMR;
		}, en);
	};


	elems.buttonStarsOn.onclick = e => {
		if (!checkNumberValue(IRQ_MASK_REQ, true))
			return;

		const en = () => {
			elems.buttonStarsOff.disabled = false;
			elems.buttonStarsOn.disabled = false;
		};
		
		const newIMR = IRQ_MASK_REQ | 2;

		sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: newIMR }, () => {
			IRQ_MASK_REQ = newIMR;
		}, en);
	};


	elems.trVideoRecording.hidden = !isVideoRecordingAvailable;
	elems.tdStars.hidden = videoOnly;
	elems.trSetTZPeriod.hidden = videoOnly;
}