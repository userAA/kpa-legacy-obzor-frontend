function main()
{
	const elems = getAJAXElemsWithID();
	const flashParams = {
		FL_MASK: null,
		FL_FRAMES: null,
		FL_START_PAGE: null,
		FL_NUM_PAGE: null,
		FL_FRAME_DEC: null,
		FL_FRAME_REC: null,
	};

	let isLvdsDecodingRunning = false;
	let doubleSendCommands = true;

	let textboxTrans_ena_value = null;
	let textboxTrans_src_select_value = null;

	const FL_MASK_bitNumsByCheckboxIDs = (() => 
	{
		let result = {};
		const prefix = "checkboxFL_MASK_";
		
		for (let key in elems) {
			if (key.startsWith(prefix)) {
				const bitNum = Number.parseInt(elems[key].name);
				result[key] = bitNum;
			}
		}

		return result;
	})();

	const FL_MASK_checkboxIDsByBitNums = (() => 
	{
		let result = [];

		for (let key in FL_MASK_bitNumsByCheckboxIDs) {
			const bitNum = FL_MASK_bitNumsByCheckboxIDs[key];
			result[bitNum] = key;
		}

		return result;
	})();
	
	const textboxesByFieldNames = {
		"FL_MASK": elems.textboxFL_MASK, 
		"FL_FRAMES_ADDR": elems.textboxFL_FRAMES,
		"FL_FRAME_DEC_ADDR": elems.textboxFL_FRAME_DEC,
		"FL_FRAME_REC_ADDR": elems.textboxFL_FRAME_REC,
	}

	const generalFieldsToRead = objectToKeyArray(textboxesByFieldNames);

	const flashParamsByFieldNames = {
		"FL_MASK": "FL_MASK",
		"FL_FRAMES_ADDR": "FL_FRAMES",
		"FL_FRAME_DEC_ADDR": "FL_FRAME_DEC",
		"FL_FRAME_REC_ADDR": "FL_FRAME_REC",
	};

	let shutter = textboxIntValueGuard(elems.textboxShutter, 2);

	elems.buttonClearMemory.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			batch.flush(en);
			Pilv.flashVideo.clearMemory(en, en);
		//});
	};


	elems.buttonStartRecord.onclick = (e) => 
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x61, data: [] });
			
			batch.pushRequest("pilv.flash_video.start_record");	
			batch.flush(en);
			//Pilv.flashVideo.startRecord(en, en);
		//});
	};


	elems.buttonStopOperation.onclick = (e) => 
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x62, data: [] });
			
			batch.pushRequest("pilv.flash_video.stop_operation");	
			batch.flush(en);
			//Pilv.flashVideo.stopOperation(en, en);
	//	});
	};


	elems.buttonPauseRecord.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() =>
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x63, data: [] });
			
			batch.pushRequest("pilv.flash_video.pause_record");	
			batch.flush(en);
			//Pilv.flashVideo.pauseRecord(en, en);
		//});
	};


	elems.buttonResumeRecord.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };
	
		//setIRQ_MASK_REQto0And(() => 
		//{
			const batch = new JsonRPCBatch();
			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x64, data: [] });
			
			batch.pushRequest("pilv.flash_video.resume_record");	
			batch.flush(en);
			//Pilv.flashVideo.resumeRecord(en, en);
		//});
	};


	elems.buttonReadMemory.onclick = (e) =>
	{
		const statePrev = e.target.disabled;
		e.target.disabled = true;
		const en = () => { e.target.disabled = statePrev; };

		//setIRQ_MASK_REQto0And(() => 
		//{
			const batch = new JsonRPCBatch();

			//if (doubleSendCommands)
			//	batch.pushRequest("zd.cmd.can.send_direct", { command: 0x61, data: [] });
			
			batch.pushRequest("pilv.flash_video.read_memory");		
			batch.flush(en);
			//Pilv.flashVideo.readMemory(en, en);
		//});
	};


	function checkboxFL_MASK_oninput()
	{
		let FL_MASK = 0;

		for (let key in FL_MASK_bitNumsByCheckboxIDs) {
			const elem = elems[key];
			const bitNum = FL_MASK_bitNumsByCheckboxIDs[key];
			FL_MASK |= elem.checked ? (1 << bitNum) : 0;
		}

		elems.textboxFL_MASK.value = sprintf(stringIsHex(elems.textboxFL_MASK.value) ? "0x%04X" : "%d", FL_MASK);
		textboxIntValueGuard(elems.textboxFL_MASK, 2);
		flashParams.FL_MASK = FL_MASK;
	}


	elems.checkboxFL_MASK_0.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_1.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_2.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_3.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_4.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_5.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_6.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_7.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_8.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_9.oninput  = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_10.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_11.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_12.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_13.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_14.oninput = checkboxFL_MASK_oninput;
	elems.checkboxFL_MASK_15.oninput = checkboxFL_MASK_oninput;


	function updateFL_MASKCheckboxes(FL_MASK)
	{
		for (let i = 0; i < 16; ++i) {
			const bitValue = FL_MASK & (1 << i);
			const elem = elems[FL_MASK_checkboxIDsByBitNums[i]];
			
			elem.checked = bitValue != 0;
		}
	}


	function setFL_MASK(FL_MASK)
	{
		flashParams.FL_MASK = FL_MASK;
		updateFL_MASKCheckboxes(FL_MASK);
		updateButtonSendParamsState();
	}


	function setFL_FRAMES(FL_FRAMES)
	{
		flashParams.FL_FRAMES = FL_FRAMES;
		updateButtonSendParamsState();
	}


	function setFL_START_PAGE(FL_START_PAGE)
	{
		flashParams.FL_START_PAGE = FL_START_PAGE;
		updateButtonSendParamsState();
	}


	function setFL_NUM_PAGE(FL_NUM_PAGE)
	{
		flashParams.FL_NUM_PAGE = FL_NUM_PAGE;
		updateButtonSendParamsState();
	}


	function setFL_FRAME_DEC(FL_FRAME_DEC)
	{
		flashParams.FL_FRAME_DEC = FL_FRAME_DEC;
		updateButtonSendParamsState();
	}


	function setFL_FRAME_REC(FL_FRAME_REC)
	{
		flashParams.FL_FRAME_REC = FL_FRAME_REC;
		updateButtonSendParamsState();
	}


	function updateButtonSendParamsState()
	{
		let isCorrect = true;

		for (let key in flashParams) {
			if (!checkNumberValue(flashParams[key], false)) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonSendParams.disabled = !isCorrect;
	}


	function updateButtonSendShutter()
	{
		elems.buttonSendShutter.disabled = !checkNumberValue(shutter);
	}


	elems.textboxFL_MASK.oninput = (e) =>
	{
		const FL_MASK = textboxIntValueGuard(e.target, 2, false);
		setFL_MASK(FL_MASK);
	};


	elems.textboxFL_FRAMES.oninput = (e) => 
	{
		const FL_FRAMES = textboxIntValueGuard(e.target, 2, false);
		setFL_FRAMES(FL_FRAMES);
	};


	elems.textboxFL_START_PAGE.oninput = (e) =>
	{
		const FL_START_PAGE = textboxIntValueGuard(e.target, 4, false);
		setFL_START_PAGE(FL_START_PAGE);
	};
		

	elems.textboxFL_NUM_PAGE.oninput = (e) =>
	{
		const FL_NUM_PAGE = textboxIntValueGuard(e.target, 4, false);
		setFL_NUM_PAGE(FL_NUM_PAGE);
	};


	elems.textboxFL_FRAME_DEC.oninput = (e) =>
	{
		const FL_FRAME_DEC = textboxIntValueGuard(e.target, 2, false);
		setFL_FRAME_DEC(FL_FRAME_DEC);
	};


	elems.textboxFL_FRAME_REC.oninput = (e) =>
	{
		const FL_FRAME_REC = textboxIntValueGuard(e.target, 2, false);
		setFL_FRAME_REC(FL_FRAME_REC);
	};


	elems.buttonSendParams.onclick = (e) => 
	{
		Pilv.flashVideo.sendParams(flashParams);

		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_MASK", value: flashParams.FL_MASK });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_FRAMES_ADDR", value: flashParams.FL_FRAMES });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_START_PAGE_L", value: (flashParams.FL_START_PAGE & 0xFFFF) });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_START_PAGE_H", value: (flashParams.FL_START_PAGE >> 16) & 0xFFFF });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_NUM_PAGE_L", value: (flashParams.FL_NUM_PAGE & 0xFFFF) });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_NUM_PAGE_H", value: (flashParams.FL_NUM_PAGE >> 16) & 0xFFFF });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_FRAME_DEC_ADDR", value: flashParams.FL_FRAME_DEC });
		batch.pushRequest("zd.cmd.set_field_value", { name: "FL_FRAME_REC_ADDR", value: flashParams.FL_FRAME_REC });

		batch.flush();
	};


	elems.buttonSendShutter.onclick = ({ currentTarget }) =>
	{
		const en = () => currentTarget.disabled = false;
		currentTarget.disabled = true;
		sendJsonRPCRequest("pilv.mko.set_exposure_time", [shutter], en, en);
	};


	elems.textboxShutter.oninput = ({ currentTarget }) => {
		shutter = textboxIntValueGuard(currentTarget, 2, false);
		updateButtonSendShutter();
	};

	function updateButtonSendSpeedLineState()
	{
		elems.buttonSendSpeedLine.disabled = 
			!checkNumberValue(textboxTrans_ena_value) || 
			!checkNumberValue(textboxTrans_src_select_value);
	}

	elems.textboxTrans_ena.oninput = ({ currentTarget }) => {
		textboxTrans_ena_value = textboxIntValueGuard(currentTarget, 2);
		updateButtonSendSpeedLineState();
	};

	elems.textboxTrans_src_select.oninput = ({ currentTarget }) => {
		textboxTrans_src_select_value = textboxIntValueGuard(currentTarget, 2);
		updateButtonSendSpeedLineState();
	};


	const sendReadFields = new AsyncLoopFunc(function()
	{
		const fieldArray = (() => 
		{
			let result = [];
			generalFieldsToRead.forEach((v) => { result.push(v); });
			result.push("FL_START_PAGE_L");
			result.push("FL_START_PAGE_H");
			result.push("FL_NUM_PAGE_L");
			result.push("FL_NUM_PAGE_H");
			return result;
		})();

		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_fields_value", { names: fieldArray }, j => {
			for (let i = 0; i < 16; ++i) {
				const elem = elems[FL_MASK_checkboxIDsByBitNums[i]];				
				elem.disabled = false;
			}

			flashParams.FL_MASK = j[0].raw;
			flashParams.FL_FRAMES = j[1].raw;
			flashParams.FL_FRAME_DEC = j[2].raw;
			flashParams.FL_FRAME_REC = j[3].raw;

			
			const FL_START_PAGE_L 	= j[generalFieldsToRead.length + 0];
			const FL_START_PAGE_H 	= j[generalFieldsToRead.length + 1];
			const FL_NUM_PAGE_L 	= j[generalFieldsToRead.length + 2];
			const FL_NUM_PAGE_H 	= j[generalFieldsToRead.length + 3];
			
			elems.textboxFL_MASK.value = flashParams.FL_MASK?.toString() ?? "";

			updateFL_MASKCheckboxes(flashParams.FL_MASK);
			elems.textboxFL_FRAMES.value = flashParams.FL_FRAMES?.toString() ?? "";
			elems.textboxFL_FRAME_DEC.value = flashParams.FL_FRAME_DEC?.toString() ?? "";
			elems.textboxFL_FRAME_REC.value = flashParams.FL_FRAME_REC?.toString() ?? "";
	
			setFL_START_PAGE(MAKELONG(FL_START_PAGE_L.raw, FL_START_PAGE_H.raw));
			elems.textboxFL_START_PAGE.disabled = false;
			elems.textboxFL_START_PAGE.value = flashParams.FL_START_PAGE?.toString() ?? "";
			
			setFL_NUM_PAGE(MAKELONG(FL_NUM_PAGE_L.raw, FL_NUM_PAGE_H.raw));
			elems.textboxFL_NUM_PAGE.disabled = false;
			elems.textboxFL_NUM_PAGE.value = flashParams.FL_NUM_PAGE?.toString() ?? "";
	
			setFL_MASK(textboxFloatValueGuard(elems.textboxFL_MASK));
			setFL_FRAMES(textboxFloatValueGuard(elems.textboxFL_FRAMES));
			setFL_START_PAGE(textboxFloatValueGuard(elems.textboxFL_START_PAGE));
			setFL_NUM_PAGE(textboxFloatValueGuard(elems.textboxFL_NUM_PAGE));
			setFL_FRAME_DEC(textboxFloatValueGuard(elems.textboxFL_FRAME_DEC));
			setFL_FRAME_REC(textboxFloatValueGuard(elems.textboxFL_FRAME_REC));
			updateButtonSendParamsState();
		});

		batch.pushRequest("zd.cmd.get_fields_value", { names: [
			"Trans_ena",
			"Trans_src_select"
		] }, ([Trans_ena, Trans_src_select]) => {
			elems.textboxTrans_ena.value = Trans_ena?.raw ?? "";
			elems.textboxTrans_src_select.value = Trans_src_select?.raw ?? "";
			textboxTrans_ena_value = textboxIntValueGuard(elems.textboxTrans_ena, 2);
			textboxTrans_src_select_value = textboxIntValueGuard(elems.textboxTrans_src_select, 2);
			updateButtonSendSpeedLineState();
		});

		batch.flush();
	});

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.get_field_value", { name: "DeviceStatus_LSW" }, (j) => 
		{
			const val = (j.raw >> 1) & 0x0F;

			switch (val) {
				case 2:
					elems.labelDeviceStatus.textContent = "ОЧИСТКА!";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = true;
					break;
				case 4:
					elems.labelDeviceStatus.textContent = "ЗАПИСЬ!";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = true;
					break;
				case 6:
					elems.labelDeviceStatus.textContent = "ЧТЕНИЕ";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = true;
					break;
				case 1:
					elems.labelDeviceStatus.textContent = "ОЖИДАНИЕ";
					elems.buttonClearMemory.disabled = false;
					elems.buttonStartRecord.disabled = false;
					elems.buttonStopOperation.disabled = true;
					elems.buttonPauseRecord.disabled = false;
					elems.buttonResumeRecord.disabled = true;
					elems.buttonReadMemory.disabled = isLvdsDecodingRunning;
					break;
				case 10:
				case 12:
				case 14:
					elems.labelDeviceStatus.textContent = "ПАУЗА";
					elems.buttonClearMemory.disabled = true;
					elems.buttonStartRecord.disabled = true;
					elems.buttonStopOperation.disabled = false;
					elems.buttonPauseRecord.disabled = true;
					elems.buttonResumeRecord.disabled = false;
					elems.buttonReadMemory.disabled = true;
					break;
			}
		});

		batch.flush(() => {
			this.continueTimeout(global.telemUpdateInterval);
		})
	});


	const checkLvdsDecodingProgress = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("lvds.decoder.get_progress", null, (j) =>
		{
			const hasError = j.hasOwnProperty("error");
			elems.trLvdsProgress.hidden = !j.is_running;
			isLvdsDecodingRunning = j.is_running;

			if (!j.is_running) {
				if (!loop.isRunning) {
					loop.run();
				}
				
				this.continueTimeout(global.telemUpdateInterval);
				return;
			} else {
				if (loop.isRunning) {
					loop.stop();
				}
			}


			if (hasError) {
				elems.tdLvdsDecodingState.textContent = sprintf("Ошибка при декодировании flash видео: %s", j.error);
			} else {
				elems.tdLvdsDecodingState.textContent = "Запись flash-видео декодируется...";
			}

			elems.progressLvdsDecoding.value = j.completed * elems.progressLvdsDecoding.max;
			elems.tdLvdsChunksCounter.textContent = j.counter.chunk;
			elems.tdLvdsPkgVideoCounter.textContent = j.counter.video;
			elems.tdLvdsTelemCounter.textContent = j.counter.telem;
			elems.tdLvdsPkgCounter.textContent = j.counter.total_packages;
			elems.tdLvdsFramesCounter.textContent = j.counter.frames;
			this.continueTimeout(global.telemUpdateInterval);
		});
	});

	sendReadFields.run();
	checkLvdsDecodingProgress.run();
}