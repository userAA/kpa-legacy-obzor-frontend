function main()
{
	const elems = getAJAXElemsWithID();
	const clockPageOpened = (new Date()) - (new Date(1970, 0, 1, 0, 0, 0, 0));
	let texboxSetTSPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTSPeriod);
	const searchParams = new SearchParams();

	function updateWorldTime(preambule)
	{
		if (preambule === null)
			return;

		elems.tdWorldTime.textContent = getWorldTimeStringFromPreambule(preambule, clockPageOpened);
	}


	function setTSPeriod(period)
	{
		if (!checkNumberValue(period))
			return;

		Pilv.can.setArrayTypePeriod(0xFD, period);
	}


	function updateButtonSetTSPeriodState()
	{
		elems.buttonSetTSPeriod.disabled = !checkNumberValue(texboxSetTSPeriod_value, false);
	}


	function updateButtonSendSearchParamsState()
	{
		let isCorrect = true;

		for (let key in searchParams) {
			if (!checkNumberValue(searchParams[key], false)) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonSendSearchParams.disabled = !isCorrect;

		updateButtonSendSearchState();
	}


	function updateButtonSendSearchState() 
	{
		elems.buttonSearchModeOn.disabled = !(checkNumberValue(searchParams.SearchON, false) && checkNumberValue(searchParams.MinVal));
	}


	elems.buttonSetTSPeriod.onclick = (e) => 
	{
		setTSPeriod(texboxSetTSPeriod_value);
	};


	elems.textboxSetTSPeriod.oninput = (e) =>
	{
		texboxSetTSPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTSPeriodState();
	};


	elems.textboxSetTSPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;
	
		setTSPeriod(texboxSetTSPeriod_value);
	};


	elems.buttonSendSearchParams.onclick = (e) =>
	{
		for (let key in searchParams) {
			if (!checkNumberValue(searchParams[key])) {
				return;
			}
		}

		Pilv.search.sendParams(searchParams);
	};


	elems.buttonSearchModeOn.onclick = (e) => 
	{
		Pilv.search.modeOn(searchParams.SearchON, searchParams.MinVal);
	};

	function searchParamsTextbox_oninput(e)
	{
		const paramName = e.target.id.substring("textbox".length);
		const value = textboxIntValueGuard(e.target, 2);
		searchParams[paramName] = value;
		updateButtonSendSearchParamsState();
	}


	const updateSearchParams = new AsyncLoopFunc(function()
	{
		const cont = () => { this.continueTimeout(1000); };
		elems.trFieldsReading.hidden = false;
		elems.buttonSendReadSearchParams.disabled = true;
		elems.buttonSendSearchParams.disabled = true;
		setIRQ_MASK_REQto0And((setIRQ_MASK_REQRrev) => 
		{
			sendJsonRPCRequest("zd.cmd.send_read_request", { names: searchParamsNames }, (j) =>
			{
				for (let i = 0; i < j.length; ++i) {
					if (j[i] === null) {
						cont();
						return;
					}
				}
			
				for (let i = 0; i < j.length; ++i) {
					const fieldName = searchParamsNames[i];
					const fieldValue = j[i];
					const elem = searchParamsTextboxesByFieldNames[fieldName];
					elem.disabled = false;
					elem.value = fieldValue.formatted;
					searchParams[fieldName] = fieldValue.raw;
				}
			
				elems.trFieldsReading.hidden = true;
				elems.buttonSendReadSearchParams.disabled = false;
				updateButtonSendSearchParamsState();
				setIRQ_MASK_REQRrev();
			}, cont);
		}, cont);
		
		//sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IQR_MASK_REQ", value: 0; })
	});

	const searchParamsTextboxesByFieldNames = (() => 
	{
		let result = {};

		for (let key in searchParams) {
			const id = sprintf("textbox%s", key);
			const elem = elems[id];
			result[key] = elem;
		}

		return result;
	})();


	for (let key in searchParamsTextboxesByFieldNames) {
		const elem = searchParamsTextboxesByFieldNames[key];

		sendJsonRPCRequest("zd.cmd.get_field_info", { name: key }, (j) =>
		{
			elem.oninput = (e) => 
			{
				const value = textboxIntValueGuard(e.target, 2 * j.raw_size, j.is_signed);
				searchParams[key] = value;
				updateButtonSendSearchParamsState();
			};
		});

		elem.oninput = searchParamsTextbox_oninput;
	}


	elems.buttonSendReadSearchParams.onclick = (e) => 
	{
		e.target.disabled = true;
		elems.trFieldsReading.hidden = false;
		updateSearchParams.run();
	};

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();
		const arrayType = 0xFD;

		batch.pushRequest("zd.cmd.can.get_last_preambule", { array_type: arrayType }, updateWorldTime);
		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: arrayType, names: [
			"Y_dn1", 
			"Z_dn1", 
			"Val_dn1", 
			"Size_dn1", 
			"AV_dn1", 
			"AVx_dn1", 
			"AVy_dn1", 
			"AVz_dn1", 
			"p0_dn1", 
			"p1_dn1", 
			"ID_dn1",
			"DELTIME_dn1",
			"QFrameNumber_dn1" ]}, (j) =>
		{
			elems.tdY_dn1.textContent 			= j[ 0].raw !== null ? j[ 0].formatted : emptyValueString;
			elems.tdZ_dn1.textContent 			= j[ 1].raw !== null ? j[ 1].formatted : emptyValueString;
			elems.tdVal_dn1.textContent 		= j[ 2].raw !== null ? j[ 2].formatted : emptyValueString;
			elems.tdSize_dn1.textContent 		= j[ 3].raw !== null ? j[ 3].formatted : emptyValueString;
			elems.tdAV_dn1.textContent 			= j[ 4].raw !== null ? j[ 4].formatted : emptyValueString;
			elems.tdAVx_dn1.textContent 		= j[ 5].raw !== null ? j[ 5].formatted : emptyValueString;
			elems.tdAVy_dn1.textContent 		= j[ 6].raw !== null ? j[ 6].formatted : emptyValueString;
			elems.tdAVz_dn1.textContent 		= j[ 7].raw !== null ? j[ 7].formatted : emptyValueString;
			elems.tdp0_dn1.textContent 			= j[ 8].raw !== null ? j[ 8].formatted : emptyValueString;
			elems.tdp1_dn1.textContent 			= j[ 9].raw !== null ? j[ 9].formatted : emptyValueString;
			elems.tdID_dn1.textContent 			= j[10].raw !== null ? j[10].formatted : emptyValueString;
			elems.labelDELTIME_dn1.textContent 	= j[11].raw !== null ? j[11].formatted : emptyValueString;
			elems.tdQFrameNumber.textContent 	= j[12].raw !== null ? j[12].formatted : emptyValueString;
		});

		batch.pushRequest("zd.cmd.can.get_field_value", { array_type: arrayType, name: "DNStatus" }, (j) =>
		{
			if (j.raw !== null) {
				const raw = j.raw;
				elems.tdDNStatus_0.textContent = j.formatted;
				elems.tdDNStatus_1.textContent = boolToYNString(((raw >> 0) & 1) == 1);
				elems.tdDNStatus_2.textContent = boolToYNString(((raw >> 1) & 1) == 1);
				elems.tdDNStatus_3.textContent = boolToYNString(((raw >> 2) & 1) == 1);
				elems.tdDNStatus_4.textContent = boolToYNString(((raw >> 3) & 1) == 1);
				elems.tdDNStatus_5.textContent = boolToYNString(((raw >> 4) & 1) == 1);
				elems.tdDNStatus_6.textContent = boolToOnOffString(((raw >> 5) & 1) == 1);
				elems.tdDNStatus_7.textContent = boolToOnOffString(((raw >> 6) & 1) == 1);
			} else {
				elems.tdDNStatus_0.textContent = emptyValueString;
				elems.tdDNStatus_1.textContent = emptyValueString;
				elems.tdDNStatus_2.textContent = emptyValueString;
				elems.tdDNStatus_3.textContent = emptyValueString;
				elems.tdDNStatus_4.textContent = emptyValueString;
				elems.tdDNStatus_5.textContent = emptyValueString;
				elems.tdDNStatus_6.textContent = emptyValueString;
				elems.tdDNStatus_7.textContent = emptyValueString;
			}
		});

		batch.flush(() =>
		{
			this.continueTimeout(global.telemUpdateInterval);
		})
	});

	const searchParamsNames = [
		"SearchON",
		"MinVal",
		"MaxVal",
		"MinSize",
		"MaxSize",
		"MinAV",
		"MaxAV",
		"MinY",
		"MaxY",
		"MinZ",
		"MaxZ"
	];

	for (let key in searchParamsTextboxesByFieldNames) {
		const elem = searchParamsTextboxesByFieldNames[key];
		searchParams[key] = textboxIntValueGuard(elem, 2);
	}

	elems.fieldsetSetSearchParams.hidden = videoOnly;
	elems.fieldsetSetTSPeriod.hidden = videoOnly;
	updateButtonSetTSPeriodState();
	updateButtonSendSearchParamsState();

	if (!videoOnly) {
		updateSearchParams.run();
	}
	
	loop.run();
}