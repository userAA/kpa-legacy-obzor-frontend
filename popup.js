// todo: РЕФАКТОРИНГ
const Popup = {
	_initializeElems: function()
	{
		var containerID 			= "popup-container";
		var contentID 				= "popup-content";
		var headerID 				= "popup-header";
		var bodyID 					= "popup-body";
		var buttonOKID 				= "popup-button-ok";
		var buttonCancelID 			= "popup-button-cancel";

		var elemStyle 				= document.createElement("link");
		elemStyle.href 				= "popup.css";
		elemStyle.rel 				= "stylesheet";

		this._elems.container 		= document.createElement("div");
		this._elems.content 		= document.createElement("div");
		this._elems.header 			= document.createElement("p");
		this._elems.body 			= document.createElement("p");
		this._elems.buttonOK 		= document.createElement("button");
		this._elems.buttonCancel	= document.createElement("button");

		this._elems.container.id 	= containerID;
		this._elems.content.id 		= contentID;
		this._elems.header.id 		= headerID;
		this._elems.body.id 		= bodyID;
		this._elems.buttonOK.id 	= buttonOKID;
		this._elems.buttonCancel.id	= buttonCancelID;

		this._elems.container.classList.add("popup-container-hidden");

		this._elems.buttonOK.textContent = "ОК";
		this._elems.buttonCancel.textContent = "Отмена";

		this._elems.content.appendChild(this._elems.header);
		this._elems.content.appendChild(this._elems.body);
		this._elems.content.appendChild(this._elems.buttonOK);
		this._elems.content.appendChild(this._elems.buttonCancel);
		this._elems.container.appendChild(this._elems.content);
		document.body.insertBefore(this._elems.container, document.body.firstChild);
		document.body.insertBefore(elemStyle, document.body.firstChild);
	},

	initialize: function(onShow = null, onClose = null)
	{
		this._initializeElems();
		this._onShow					= onShow;
		this._onClose					= onClose;
		this._elems.buttonOK.onclick 	= function(e) 
		{
			Popup.close();

			if (Popup._onOK) {
				Popup._onOK();
			}
		};

		this._elems.buttonCancel.onclick = function(e) 
		{
			Popup.close();

			if (Popup._onCancel) {
				Popup._onCancel();
			}
		};
	},

	close: function() 
	{
		if (!this._opened)
			return;
		
		this._elems.container.className = "popup-container-hidden popup-container-close";
		document.onkeydown = this._document_onkeydown_prev;
		document.activeElement.onkeydown = this._body_onkeydown_prev;
		document.body.classList.remove("non-scrollable");
		this._opened = false;
		
		if (this._onClose) {
			this._onClose();
		}

		setTimeout(function() 
		{ 
			if (!Popup._opened) {
				Popup._elems.container.className = "popup-container-hidden";
				setTimeout(function() { Popup._activeElement_prev.focus(); }, 50);
			}
		}, 250);
	},

	showLocked: function(headerString, bodyString)
	{
		if (!this._opened) {
			this._initializeShowing(headerString, bodyString);
		}
		
		this._elems.buttonOK.hidden = true;
		this._elems.buttonCancel.hidden = true;
		document.onkeydown = this._onkeydown_lockTab;
		this._elems.container.onclick = null;

		if (this._onShow) {
			this._onShow();
		}
	},

	showInformation: function(headerString, bodyString)
	{
		if (!this._opened) {
			this._initializeShowing(headerString, bodyString);
		}
		
		this._elems.buttonOK.hidden = true;
		this._elems.buttonCancel.hidden = true;
		document.onkeydown = this._onkeydown_lockTab;
		document.activeElement.onkeydown = function(e)
		{
			if (e.key == "Enter" || e.key == "Escape") {
				Popup.close();
			}
		};

		this._elems.container.onclick = function(e)
		{
			if (e.toElement != Popup._elems.container)
				return;

			Popup.close();
		};

		if (this._onShow) {
			this._onShow();
		}
	},

	showOK: function(headerString, bodyString, onOK)
	{
		if (!this._opened) {
			this._initializeShowing(headerString, bodyString);
		}

		this._elems.buttonOK.hidden = false;
		this._elems.buttonCancel.hidden = true;
		this._elems.buttonOK.focus();
		this._elems.container.onclick = null;
		
		document.onkeydown = function(e) 
		{
			if (e.key == "Tab") {
				Popup._elems.buttonOK.focus();
				e.preventDefault();
			}
		};

		this._onOK = onOK;

		if (this._onShow) {
			this._onShow();
		}
	},

	showOKCancel: function(headerString, bodyString, onOK, onCancel, okText = "ОК", cancelText = "Отмена")
	{
		if (!this._opened) {
			this._initializeShowing(headerString, bodyString);
		}

		this._elems.buttonCancel.focus();
		this._elems.buttonOK.textContent = okText;
		this._elems.buttonCancel.textContent = cancelText;
		this._elems.buttonOK.hidden = false;
		this._elems.buttonCancel.hidden = false;
		this._onOK = onOK;
		this._onCancel = onCancel;
		this._elems.container.onclick = null;

		document.onkeydown = function(e)
		{
			if (e.key == "Escape") {
				Popup._elems.buttonCancel.onclick();
				return;
			} else if (e.key == "Tab") {
				(document.activeElement == Popup._elems.buttonCancel ? 
					Popup._elems.buttonOK :
					Popup._elems.buttonCancel).focus();
				e.preventDefault();
			}
		};


		if (this._onShow) {
			this._onShow();
		}
	},

	_opened: false,
	_document_onkeydown_prev: null,
	_body_onkeydown_prev: null,

	_elems: {
		container: null,
		content: null,
		header: null,
		body: null,
		buttonOK: null,
		buttonCancel: null,
	},

	_onShow: null,
	_onClose: null,
	_onOK: null,
	_onCancel: null,


	_update: function(headerString, bodyString)
	{
		this._elems.header.textContent = headerString;
		this._elems.body.textContent = bodyString;
	},

	_onkeydown_lockTab: function(e)
	{
		if (e.key == "Tab") {
			e.preventDefault();
		}
	},

	_initializeShowing: function(headerString, bodyString)
	{
		this._elems.container.className = "popup-container-show";
		this._document_onkeydown_prev = document.onkeydown;
		this._activeElement_prev = document.activeElement;
		document.activeElement.blur();
		document.body.focus();
		document.body.classList.add("non-scrollable");
		this._body_onkeydown_prev = document.activeElement.onkeydown;
		this._update(headerString, bodyString);
		this._opened = true;
	}
};