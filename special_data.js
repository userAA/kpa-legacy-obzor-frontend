function main()
{
	const elems = getAJAXElemsWithID();
	const clockPageOpened = (new Date()) - (new Date(1970, 0, 1, 0, 0, 0, 0));

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();
		const arrayType = 0xB1;

		//batch.pushRequest("zd.cmd.can.get_last_preambule", { array_type: arrayType }, updateWorldTime);

		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: arrayType, names: ["QTime_INT_dn1", "QTime_FR_dn1"] }, (j) => {
			elems.tdWorldTime.textContent = getWorldTimeString(j[0]?.raw, j[1]?.raw);
		});  

		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: arrayType, names: [
			"Y_dn1", 
			"Z_dn1", 
			"Val_dn1", 
			"Size_dn1", 
			"AV_dn1", 
			"AVx_dn1", 
			"AVy_dn1", 
			"AVz_dn1", 
			"p0_dn1", 
			"p1_dn1", 
			"ID_dn1",
			"DELTIME_dn1",
			"QFrameNumber_dn1" ]}, (j) =>
		{
			elems.tdY_dn1.textContent 			= j[ 0].raw !== null ? j[ 0].formatted : emptyValueString;
			elems.tdZ_dn1.textContent 			= j[ 1].raw !== null ? j[ 1].formatted : emptyValueString;
			elems.tdVal_dn1.textContent 		= j[ 2].raw !== null ? j[ 2].formatted : emptyValueString;
			elems.tdSize_dn1.textContent 		= j[ 3].raw !== null ? j[ 3].formatted : emptyValueString;
			elems.tdAV_dn1.textContent 			= j[ 4].raw !== null ? j[ 4].formatted : emptyValueString;
			elems.tdAVx_dn1.textContent 		= j[ 5].raw !== null ? j[ 5].formatted : emptyValueString;
			elems.tdAVy_dn1.textContent 		= j[ 6].raw !== null ? j[ 6].formatted : emptyValueString;
			elems.tdAVz_dn1.textContent 		= j[ 7].raw !== null ? j[ 7].formatted : emptyValueString;
			elems.tdp0_dn1.textContent 			= j[ 8].raw !== null ? j[ 8].formatted : emptyValueString;
			elems.tdp1_dn1.textContent 			= j[ 9].raw !== null ? j[ 9].formatted : emptyValueString;
			elems.tdID_dn1.textContent 			= j[10].raw !== null ? j[10].formatted : emptyValueString;
			elems.labelDELTIME_dn1.textContent 	= j[11].raw !== null ? j[11].formatted : emptyValueString;
			elems.tdQFrameNumber.textContent 	= j[12].raw !== null ? j[12].formatted : emptyValueString;
		});

		batch.pushRequest("zd.cmd.can.get_field_value", { array_type: arrayType, name: "DNStatus" }, (j) =>
		{
			if (j.raw !== null) {
				const raw = j.raw;
				elems.tdDNStatus_0.textContent = j.formatted;
				elems.tdDNStatus_1.textContent = boolToYNString(((raw >> 0) & 1) == 1);
				elems.tdDNStatus_2.textContent = boolToYNString(((raw >> 1) & 1) == 1);
				elems.tdDNStatus_3.textContent = boolToYNString(((raw >> 2) & 1) == 1);
				elems.tdDNStatus_4.textContent = boolToYNString(((raw >> 3) & 1) == 1);
				elems.tdDNStatus_5.textContent = boolToYNString(((raw >> 4) & 1) == 1);
				elems.tdDNStatus_6.textContent = boolToOnOffString(((raw >> 5) & 1) == 1);
				elems.tdDNStatus_7.textContent = boolToOnOffString(((raw >> 6) & 1) == 1);
			} else {
				elems.tdDNStatus_0.textContent = emptyValueString;
				elems.tdDNStatus_1.textContent = emptyValueString;
				elems.tdDNStatus_2.textContent = emptyValueString;
				elems.tdDNStatus_3.textContent = emptyValueString;
				elems.tdDNStatus_4.textContent = emptyValueString;
				elems.tdDNStatus_5.textContent = emptyValueString;
				elems.tdDNStatus_6.textContent = emptyValueString;
				elems.tdDNStatus_7.textContent = emptyValueString;
			}
		});

		batch.flush(() =>
		{
			this.continueTimeout(global.telemUpdateInterval);
		})
	});

	loop.run();
}