function main()
{
	const elems = getAJAXElemsWithID();
	const clockPageOpened = (new Date()) - (new Date(1970, 0, 1, 0, 0, 0, 0));
	let IRQ_MASK_REQ = null;

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();
		const arrayType = 0xB0;

		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: arrayType, names: ["QTime_INT", "QTime_FR"] }, (j) =>
		{
			elems.tdWorldTime.textContent = j[0].raw === null || j[1].raw === null ? emptyValueString : getWorldTimeString(j[0].raw, j[1].raw, clockPageOpened);
		});

		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: 0xB2, names: ["QTime_INT_vdo", "QTime_FR_vdo"] }, (j) =>
		{
			elems.tdWorldTime_DDO.textContent = j[0].raw === null || j[1].raw === null ? emptyValueString : getWorldTimeString(j[0].raw, j[1].raw, clockPageOpened);
		});
		
		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: arrayType, names: ["Q0", "Q1", "Q2", "Q3", "DELTIME"] }, (j) =>
		{
			elems.tdQ0.textContent = 		j[0].raw !== null ? j[0].formatted : emptyValueString;
			elems.tdQ1.textContent = 		j[1].raw !== null ? j[1].formatted : emptyValueString;
			elems.tdQ2.textContent = 		j[2].raw !== null ? j[2].formatted : emptyValueString;
			elems.tdQ3.textContent = 		j[3].raw !== null ? j[3].formatted : emptyValueString;
			elems.tdDELTIME.textContent = 	j[4].raw !== null ? j[4].formatted : emptyValueString;
		});

		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: 0xB2, names: [
			"QFrameNumber_vdo", 
			"Y_Sun", 
			"Z_Sun", 
			"Y_Earth", 
			"Z_Earth", 
			"Y_Moon", 
			"Z_Moon", 
			"DELTIME_vdo"
		] }, (j) =>
		{
			elems.tdQFrameNumber_vdo.textContent = j[0].raw !== null ? j[0].formatted : emptyValueString;
			elems.tdY_Sun.textContent = j[1].raw !== null ? j[1].formatted : emptyValueString;
			elems.tdZ_Sun.textContent = j[2].raw !== null ? j[2].formatted : emptyValueString;
			elems.tdY_Earth.textContent = j[3].raw !== null ? j[3].formatted : emptyValueString;
			elems.tdZ_Earth.textContent = j[4].raw !== null ? j[4].formatted : emptyValueString;
			elems.tdY_Moon.textContent = j[5].raw !== null ? j[5].formatted : emptyValueString;
			elems.tdZ_Moon.textContent = j[6].raw !== null ? j[6].formatted : emptyValueString;
			elems.tdDELTIME_vdo.textContent = j[7].raw !== null ? j[7].formatted : emptyValueString;
		});

		batch.pushRequest("zd.cmd.can.get_fields_value", { array_type: arrayType, names: 
			["QFrameNumber_L", "QFrameNumber_H"]}, (j) =>
		{
			elems.tdQFrameNumber.textContent = (j[0].raw === null || j[1].raw === null) ? emptyValueString : MAKELONG(j[0].raw, j[1].raw).toString();
		});

		batch.pushRequest("zd.cmd.can.get_field_value", { array_type: arrayType, name: "Qstatus_SI" }, (j) =>
		{
			if (j.raw !== null) {
				const raw = j.raw;

				elems.tdQStatus_0.textContent = j.formatted;
				elems.tdQStatus_1.textContent = boolToYNString((raw >> 0) & 1 == 1);
				elems.tdQStatus_2.textContent = boolToYNString((raw >> 1) & 1 == 1);
				elems.tdQStatus_3.textContent = boolToYNString((raw >> 2) & 1 == 1);
				elems.tdQStatus_4.textContent = boolToYNString((raw >> 3) & 1 == 1);
				elems.tdQStatus_5.textContent = boolToYNString((raw >> 4) & 1 == 1);
				elems.tdQStatus_6.textContent = ((raw >> 5) & 15).toString();
			} else {
				elems.tdQStatus_0.textContent = emptyValueString;
				elems.tdQStatus_1.textContent = emptyValueString;
				elems.tdQStatus_2.textContent = emptyValueString;
				elems.tdQStatus_3.textContent = emptyValueString;
				elems.tdQStatus_4.textContent = emptyValueString;
				elems.tdQStatus_5.textContent = emptyValueString;
				elems.tdQStatus_6.textContent = emptyValueString;
			}
		});


		batch.pushRequest("zd.cmd.can.get_field_value", { array_type: arrayType, name: "RSpeed" }, (j) =>
		{
			if (j.raw === null) {
				elems.tdRSpeed_0.textContent = emptyValueString;
				elems.tdRSpeed_1.textContent = emptyValueString;
				elems.tdRSpeed_2.textContent = emptyValueString;
				elems.tdRSpeed_3.textContent = emptyValueString;
				return;
			}
			
			const raw = j.raw;
			const getInt = (input) =>
			{
				let input0 = 0;
				
				if ((input >> 9) == 1) {
					input0 = input & 0x1FF;
					input0 ^= 0x1FF;
					input0 = -1.0 * (input0 + 1);
				} else {
					input0 = 1.0 * input;
				}

				input0 = input0 * Math.pow(2.0,-12) * 180.0 / 3.14;
				return input0.toFixed(3);
			};

			const getInt2 = (input) =>
			{
				let input0 = 0;

				if ((input >> 10) == 1) {
				    input0 = input & 0x3FF;
				    input0 ^= 0x3FF;
				    input0 = -1.0 * (input0 + 1);
				} else {
				    input0 = 1.0 * input;
				}

				input0 = input0 * Math.pow(2.0, -13) * 180.0 / 3.14;
				return input0.toFixed(3);
			};

			elems.tdRSpeed_0.textContent = j.formatted;
			elems.tdRSpeed_1.textContent = getInt((raw >> 0) & 1023);
			elems.tdRSpeed_2.textContent = getInt2((raw >> 10) & 2047);
			elems.tdRSpeed_3.textContent = getInt2((raw >> 21) & 2047);
		});

		batch.flush(() =>
		{
			this.continueTimeout(global.telemUpdateInterval);
		});
	});

	sendJsonRPCRequest("zd.cmd.send_read_request", { name: "IRQ_MASK_REQ" }, j => {
		IRQ_MASK_REQ = j.raw;
		elems.buttonDAOff.disabled = false;
		elems.buttonDAOn.disabled = false;
	}, () => {
		elems.buttonDAOff.disabled = true;
		elems.buttonDAOn.disabled = true;
	});

	elems.buttonDAOff.onclick = e => {
		if (!checkNumberValue(IRQ_MASK_REQ, true))
			return;

		const en = () => { 
			elems.buttonDAOff.disabled = false;
			elems.buttonDAOn.disabled = false;
		};

		const newIMR = IRQ_MASK_REQ & ~4;

		sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: newIMR }, () => {
			IRQ_MASK_REQ = newIMR;
		}, en);
	};

	elems.buttonDAOn.onclick = e => {
		if (!checkNumberValue(IRQ_MASK_REQ, true))
			return;

		const en = () => { 
			elems.buttonDAOff.disabled = false;
			elems.buttonDAOn.disabled = false;
		};

		const newIMR = IRQ_MASK_REQ | 4;

		sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: newIMR }, () => {
			IRQ_MASK_REQ = newIMR;
		}, en);
	};

	loop.run();
}