function main()
{
	const elems = getAJAXElemsWithID();

	let DeviceStatus = {
		MSW: null,
		LSW: null,
		full: null
	};
	
	const clockPageOpened = (new Date()) - (new Date(1970, 0, 1, 0, 0, 0, 0));
	
	function updateWorldTime(preambule)
	{
		if (preambule === null)
			return;

		elems.tdWorldTime.textContent = getWorldTimeStringFromPreambule(preambule, clockPageOpened);
	}
	
	
	function updateVMEM_or_VADC(elem, j) {
		elem.textContent = j.raw !== null ? sprintf("%.2f", j.raw * 0.0234375) : emptyValueString;
	}
	
	
	function updateTCCD(elem, j) {
		// T(oC) = 0,0625* TCCD – 0,25
		elem.textContent = j.raw !== null ? sprintf("%.2f", (0.0625 * j.raw) - 0.25) : emptyValueString;
	}
	
	
	function updateTDSP(elem, j) {
		//T(oC) = (160 * 48 / 4096) * TDSP – 67.84
		elem.textContent = j.raw !== null ? sprintf("%.2f", (160 * 48 / 4096) * j.raw - 67.84) : emptyValueString;
	}
	
	
	function DeviceStatus_11_toString(DeviceStatus)
	{
		var value = (DeviceStatus >> 21) & 7;
		var result = null;
		var possibleValues = [
			"Прошивка завершена",
			"Старт прошивки микросхемы управления (ожидается прошивка микросхемы управления и вычислительного устройства)",
			"Старт прошивки микросхемы управления (ожидается прошивка микросхемы)",
			"Старт прошивки вычислительного устройства (ожидается прошивка вычислительного устройства)",
			"Инициализация настроек"
		];
	
		if (value > possibleValues.length) {
			result = sprintf("НЕВЕРНЫЙ СТАТУС: %d", value);
		} else {
			result = possibleValues[value];
		}
		
		return result;
	}
	
	
	function updateDeviceStatus()
	{
		if (DeviceStatus.LSW == null || DeviceStatus.MSW == null)
			return;
	
		var val = MAKELONG(DeviceStatus.LSW, DeviceStatus.MSW);
		DeviceStatus.full = val;

		elems.tdDeviceStatus_0.textContent = val.toString();
		elems.tdDeviceStatus_1.textContent = boolToHasNoString(val & 1);
		elems.tdDeviceStatus_2.textContent = DeviceStatus_2_toString(val);
		elems.tdDeviceStatus_3.textContent = ((val >> 5) & 0xFF).toString();
		elems.tdDeviceStatus_4.textContent = ((val >> 13) & 3).toString();
		elems.tdDeviceStatus_5.textContent = ((val >> 15) & 3).toString();
		elems.tdDeviceStatus_6.textContent = boolToHasNoString(DeviceStatus.MSW & 1);
		elems.tdDeviceStatus_7.textContent = boolToHasNoString(DeviceStatus.MSW & 2);
		elems.tdDeviceStatus_8.textContent = boolToHasNoString(DeviceStatus.MSW & 4);
		elems.tdDeviceStatus_9.textContent = boolToHasNoString(DeviceStatus.MSW & 8);
		elems.tdDeviceStatus_10.textContent = boolToHasNoString(DeviceStatus.MSW & 16);
		elems.tdDeviceStatus_11.textContent = boolToHasNoString(DeviceStatus.MSW & 32);
		elems.tdDeviceStatus_12.textContent = boolToHasNoString(DeviceStatus.MSW & 64);
		elems.tdDeviceStatus_13.textContent = (DeviceStatus.MSW >> 8).toString();
	}
	
	
	function updateObjStatus(j)
	{
		if (j.raw !== null) {
			elems.tdObjStatus_0.textContent = j.formatted;
			elems.tdObjStatus_1.textContent = LOBYTE(j.raw).toString();
			elems.tdObjStatus_2.textContent = HIBYTE(j.raw).toString();
		} else {
			elems.tdObjStatus_0.textContent = emptyValueString;
			elems.tdObjStatus_1.textContent = emptyValueString;
			elems.tdObjStatus_2.textContent = emptyValueString;
		}
	}
	
	
	function updateSearchStatus(j)
	{
		if (j.raw !== null) {
			let raw = j.raw;

			elems.tdSearchStatus_0.textContent = j.formatted;
			elems.tdSearchStatus_1.textContent = boolToYNString(raw & 1);
			elems.tdSearchStatus_2.textContent = ((raw >> 1) & 0x7F).toString();
			elems.tdSearchStatus_3.textContent = ((raw >> 8) & 0xFF).toString();
			elems.tdSearchStatus_4.textContent = ((raw >> 16) & 0xFF).toString();
			elems.tdSearchStatus_5.textContent = ((raw >> 24) & 0xFF).toString();
		} else {
			elems.tdSearchStatus_0.textContent = emptyValueString;
			elems.tdSearchStatus_1.textContent = emptyValueString;
			elems.tdSearchStatus_2.textContent = emptyValueString;
			elems.tdSearchStatus_3.textContent = emptyValueString;
			elems.tdSearchStatus_4.textContent = emptyValueString;
			elems.tdSearchStatus_5.textContent = emptyValueString;
		}
	}
	
	
	function updateQStatus(j)
	{
		if (j.raw !== null) {
			const raw = j.raw;

			elems.tdQStatus_0.textContent = j.formatted;
			elems.tdQStatus_1.textContent = boolToYNString((raw >> 0) & 1 == 1);
			elems.tdQStatus_2.textContent = boolToYNString((raw >> 1) & 1 == 1);
			elems.tdQStatus_3.textContent = boolToYNString((raw >> 2) & 1 == 1);
			elems.tdQStatus_4.textContent = boolToYNString((raw >> 3) & 1 == 1);
			elems.tdQStatus_5.textContent = boolToYNString((raw >> 4) & 1 == 1);
			elems.tdQStatus_6.textContent = ((raw >> 5) & 15).toString();
		} else {
			elems.tdQStatus_0.textContent = emptyValueString;
			elems.tdQStatus_1.textContent = emptyValueString;
			elems.tdQStatus_2.textContent = emptyValueString;
			elems.tdQStatus_3.textContent = emptyValueString;
			elems.tdQStatus_4.textContent = emptyValueString;
			elems.tdQStatus_5.textContent = emptyValueString;
			elems.tdQStatus_6.textContent = emptyValueString;
		}
	}
	
	
	function updateUndetectedObjects(j)
	{
		elems.tdUndetectedObjects_0.textContent = j.raw !== null ? j.formatted : emptyValueString;
	
		for (var i = 0; i < 7; ++i) {
			var elemName = sprintf("tdUndetectedObjects_%d", i + 1);
			elems[elemName].textContent = j.raw !== null ? ((j.raw >> (i * 4)) & 0x0F).toString() : emptyValueString;
		}
	}
	
	
	function updateElemTextByJsonRPCResponse(elem, j, useRaw = false)
	{
		if (j.raw !== null) {
			elem.textContent = useRaw ? j.raw.toString() : j.formatted;
		}
	}


	let loop = new AsyncLoopFunc(function()
	{
		var batch = new JsonRPCBatch();

		let updateAllPart1 = () => // нет condition_variable или yield return, чтоб приостановить поток, приходится выкручиваться
		{
			batch.pushRequest("zd.cmd.can.get_last_preambule", { array_type: 0xFC }, updateWorldTime);
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "FrameNumber" },
				function (j) { updateElemTextByJsonRPCResponse(elems.tdFrameNumber, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "PowerUpCnt" },
				function (j) { updateElemTextByJsonRPCResponse(elems.tdPowerUpCnt, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "ErPowerUpCnt" }, (j) =>
			{
				if (j.raw !== null) {
					let value = j.raw;
					
					elems.tdErPowerUpCnt_user.textContent = LOBYTE(value).toString();
					elems.tdErPowerUpCnt_self.textContent = HIBYTE(value).toString();
				} else {
					elems.tdErPowerUpCnt_user.textContent = emptyValueString;
					elems.tdErPowerUpCnt_self.textContent = emptyValueString;
				}
			});
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "PowerUpTimer" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdPowerUpTimer, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "CUR_FLASH_page" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdCUR_FLASH_page, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "SHPR_MAP_CNT" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdSHPR_MAP_CNT, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "SHPR_STREAM_CNT" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdSHPR_STREAM_CNT, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM0" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM0, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM1" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM1, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM2" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM2, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VMEM3" },
				(j) => { updateVMEM_or_VADC(elems.tdVMEM3, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VADC0" },
				(j) => { updateVMEM_or_VADC(elems.tdVADC0, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "VADC1" },
				(j) => { updateVMEM_or_VADC(elems.tdVADC1, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "TCCD" },
				(j) => { updateTCCD(elems.tdTCCD, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "TDSP" },
				(j) => { updateTDSP(elems.tdTDSP, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "RecPassCounter" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdRecPassCounter, j); });

			batch.pushRequest("zd.cmd.get_field_value", { name: "CntBst" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdCntBst, j, true); });

			batch.pushRequest("zd.cmd.get_field_value", { name: "sec_point_counter" },
				(j) => { updateElemTextByJsonRPCResponse(elems.tdsec_point_counter, j); });
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "ObjStatus" 		}, updateObjStatus);
			batch.pushRequest("zd.cmd.get_field_value", { name: "SearchStatus" 		}, updateSearchStatus);
			batch.pushRequest("zd.cmd.get_field_value", { name: "QStatus" 			}, updateQStatus);
			batch.pushRequest("zd.cmd.get_field_value", { name: "UndetectedObjects" }, updateUndetectedObjects);
		
			batch.flush(updateAllPart2);
		}

		let updateAllPart2 = () =>
		{
			batch.pushRequest("zd.cmd.get_field_value", { name: "DeviceStatus_MSW" }, (j) =>
			{
				DeviceStatus.MSW = j.raw;
			});
		
			batch.pushRequest("zd.cmd.get_field_value", { name: "DeviceStatus_LSW" }, (j) =>
			{
				DeviceStatus.LSW = j.raw;
			});

			batch.flush(() =>
			{
				updateDeviceStatus();
				this.continueTimeout(global.telemUpdateInterval);
			});
		}

		updateAllPart1();
	});

	let texboxSet0xFCPeriod_value = NaN;

	function set0xFCPeriod(period)
	{
		if (!checkNumberValue(period))
			return;

		Pilv.can.setArrayTypePeriod(0xFC, period);
	}


	function updateButtonSet0xFCPeriodState()
	{
		elems.buttonSet0xFCPeriod.disabled = !checkNumberValue(texboxSet0xFCPeriod_value, false);
	}


	elems.buttonSet0xFCPeriod.onclick = (e) => 
	{
		set0xFCPeriod(texboxSet0xFCPeriod_value);
	};


	elems.textboxSet0xFCPeriod.oninput = (e) =>
	{
		texboxSet0xFCPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSet0xFCPeriodState();
	};


	elems.textboxSet0xFCPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		if (!checkNumberValue(texboxSet0xFCPeriod_value, false))
			return;
	
		set0xFCPeriod(texboxSet0xFCPeriod_value);
	};


	elems.buttonOff0xFC.onclick 			= (e) => { Pilv.can.off0xFC(); 				};
	elems.buttonSet0xFCPeriod1s.onclick 	= (e) => { Pilv.can.set0xFCPeriod1s(); 		};
	elems.buttonSet0xFCPeriod10s.onclick 	= (e) => { Pilv.can.set0xFCPeriod10s(); 	};
	elems.buttonSet0xFCPeriod1min.onclick 	= (e) => { Pilv.can.set0xFCPeriod1min(); 	};
	elems.buttonSet0xFCPeriod10min.onclick 	= (e) => { Pilv.can.set0xFCPeriod10min(); 	};
	texboxSet0xFCPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSet0xFCPeriod);
	updateButtonSet0xFCPeriodState();
	elems.fieldsetSet0xFCPeriod.hidden = videoOnly;
	loop.run();
}