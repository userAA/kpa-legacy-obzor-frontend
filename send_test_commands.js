function main()
{
	const elems = getAJAXElemsWithID();
	let prevZD = null;
	let toSendTestCommands = false;
	let DeviceStatus = null;
	let rawDeviceStatus = null;
	//const zds = [56, 57];
	const zds = [56, 57, 58];
	//const zds = [56];
	let zdi = 0;
	let isLastZD = zdi === zds.length - 1;
	let DeviceStatusTimeoutHandle = null;
	let wasDeviceStatusError = false;
	let cyclesCount = 0;
	let enableURV = true;
	let doDisableOthersTO = zds.length === 1;
	const toWait = 100;
	const soprInfo = new SoprInfo();
	const flashParams = new FlashParams();
	let APICounters = [];


	for (const key in flashParams) {
		flashParams[key] = 0;
	}

	flashParams.FL_MASK = 0xFFFF;

	zds.forEach((v) => { APICounters.push(0); });


	for (const key in soprInfo) {
		soprInfo[key] = 0;
	}

	function changeZD(onResult, send0xfcCommands = true)
	{
		++zdi;
		zdi %= zds.length;
		isLastZD = zdi === zds.length - 1;
		elems.labelZDID.textContent = zds[zdi].toString();

		const trySetup = () =>
		{
			let wasError = false;
			const setWasError = () => { wasError = true; };
			const batch = new JsonRPCBatch();

			if (!send0xfcCommands && !doDisableOthersTO) batch.pushRequest("pilv.can.off_0xfc", null, null, setWasError);
			batch.pushRequest("zd.cmd.set_tx_receiver", [zds[zdi]]);
			batch.pushRequest("zd.cmd.reset_fields_value", 
				{ names: ["DeviceStatus_LSW", "DeviceStatus_MSW"] }, (j) =>
			{
				DeviceStatus = null;
				rawDeviceStatus = null;
			});

			if (!send0xfcCommands && !doDisableOthersTO) batch.pushRequest("pilv.can.set_0xfc_period_1s", null, null, setWasError);
	
			batch.flush(() => 
			{
				if (wasError) {
					trySetup()
				} else {
					setTimeout(onResult, toWait); 
				}
			});
		};
		
		trySetup();
	}

	//pred, onSuccess)
	const checkDeviceStatusParams = {
		pred: null,
		onSuccess: null
	};


	const checkDeviceStatus = new AsyncLoopFunc(function() 
	{
		const checkError = () =>
		{
			if (wasDeviceStatusError) {
				elems.trError.hidden = false;
				elems.buttonSwitchSendTestCommandsState.onclick();
				DeviceStatusTimeoutHandle = null;
				wasDeviceStatusError = false;
				checkDeviceStatus.stop();
			} else {
				checkDeviceStatus.continueTimeout(50);
			}
		};

		getDeviceStatus(() =>
		{
			if (!checkDeviceStatusParams.pred || (checkDeviceStatusParams.pred && checkDeviceStatusParams.pred())) {
				if (DeviceStatusTimeoutHandle !== null) {
					clearTimeout(DeviceStatusTimeoutHandle);
					DeviceStatusTimeoutHandle = null;
				}
	
				checkDeviceStatusParams.onSuccess();
			} else {
				if (DeviceStatusTimeoutHandle === null) {
					DeviceStatusTimeoutHandle = setTimeout(() => { wasDeviceStatusError = true; }, 30 * 1000);
				}
	
				checkError();
			}
		});
	});

	function compareAPICounter(v) 
	{
		return v < APICounters[zdi] || ((APICounters[zdi] === 0 || APICounters[zdi] === 1)  && v >= 254);
	}


	function sendAPI(onResult) 
	{
		let wasError = false;
		const setErr = () => { wasError = true; }
		const batch = new JsonRPCBatch();
		
		batch.pushRequest("pilv.send_sopr_info", soprInfo, null, setErr);
		batch.pushRequest("pilv.flash_video.send_params", flashParams, null, setErr);
		
		batch.flush(() =>
		{
			if (wasError) {
				sendAPI(onResult);
			} else {
				onResult();
			}
		});
	}


	const sendCommands = new AsyncLoopFunc(function()
	{
		elems.labelCyclesCount.textContent = cyclesCount.toString();
		cyclesCount++;
		elems.trError.hidden = true;

		if (DeviceStatusTimeoutHandle !== null) {
			clearTimeout(DeviceStatusTimeoutHandle);
			DeviceStatusTimeoutHandle = null;
		}

		const cont = () => { this.continueTimeout();};
		function sendStop()
		{
			elems.labelStatus.textContent = "СТОП";
			const prevAPICounter = APICounters[zdi];

			sendAPI(() =>
			{
				Pilv.flashVideo.stopOperation(() => 
				{
					checkDeviceStatusParams.pred = () => { return DeviceStatus === 1 
						&& compareAPICounter(prevAPICounter); 
					};
					checkDeviceStatusParams.onSuccess = () => 
					{ 
						changeZD(isLastZD ? () => { setTimeout(cont, toWait); } : sendStop); 
					};
	
					checkDeviceStatus.run();  
				}, sendStop);
			});
		}


		function sendStartRecord() 
		{
			//sendStop();
			
			elems.labelStatus.textContent = "ЧТЕНИЕ";
			const prevAPICounter = APICounters[zdi];

			sendAPI(() =>
			{
				sendJsonRPCRequest("zd.cmd.can.send_direct", { command: 0x66, data: [] }, () => 
				{ 
					checkDeviceStatusParams.pred = () => { return DeviceStatus === 6 
						&& compareAPICounter(prevAPICounter);
					};
					checkDeviceStatusParams.onSuccess = () => 
					{ 
						changeZD(isLastZD ? () => { setTimeout(sendStop, toWait); } : sendStartRecord); 
					};

					checkDeviceStatus.run();  
				}, sendStartRecord);
			});
		}


		function sendClear()
		{
			sendStartRecord();
			return;
			elems.labelStatus.textContent = "ОЧИСТКА";
			const prevAPICounter = APICounters[zdi];
			
			sendAPI(() =>
			{
				Pilv.flashVideo.clearMemory(() => 
				{ 
					checkDeviceStatusParams.pred = () => { return DeviceStatus === 2
						&& compareAPICounter(prevAPICounter);
					};

					checkDeviceStatusParams.onSuccess = () => 
					{
						changeZD(isLastZD ? () => { setTimeout(sendStartRecord, toWait); } : sendClear); 
					};

					checkDeviceStatus.run(); 
				}, sendClear);
			});
		}


		function setMask()
		{
			elems.labelStatus.textContent = "МАСКА";
			Pilv.flashVideo.setMask(0xFFFF, 0, (j) => { changeZD(isLastZD ? sendClear : setMask, false); }, setMask);
		}

		zdi = 0;
		sendJsonRPCRequest("zd.cmd.set_tx_receiver", [zds[zdi]], () =>
		{
			sendJsonRPCRequest("pilv.can.set_0xfc_period_1s");
			setMask();
		});
	}, false, () =>
	{
		sendJsonRPCRequest("zd.cmd.disable_ignore_nopower");
		sendJsonRPCRequest("zd.cmd.set_tx_receiver", [prevZD]);
	});


	function initialize()
	{
		const prepareZD = new AsyncLoopFunc(function() 
		{
			const cont = () => { this.continueTimeout(1000); };
			const next = isLastZD ? () => 
			{ 
				elems.buttonSwitchSendTestCommandsState.disabled = false; 
			} : cont;

			setIRQ_MASK_REQto0And(() =>
			{
				const batch = new JsonRPCBatch();
				let wasError = false;
				const setErrorTrue = () => { wasError = true; };
				batch.pushRequest("pilv.can.off_0xfc", null, null, setErrorTrue);
				//batch.pushRequest("pilv.can.set_0xfc_period_1s", null, null, setErrorTrue);
				batch.pushRequest("zd.cmd.send_write_request", { name: "StarDetectON", value: 0 }, null, setErrorTrue)
				batch.pushRequest("pilv.setup_urv", [enableURV ? 1 : 0]); // 1 для включения УРВ
				batch.pushRequest("pilv.setup_oap", [zds[(zdi + 1) % zds.length], zds.length > 2 ? zds[(zdi + 2) % zds.length] : 0]);
				batch.flush(() => 
				{
					if (wasError) {
						cont();
					} else {
						changeZD(next, false);
					}
				});
			}, cont);
		});

		sendJsonRPCRequest("zd.cmd.get_tx_receiver", null, (j) =>
		{
			prevZD = j;
			zdi = 0;
			sendJsonRPCRequest("zd.cmd.set_tx_receiver", [zds[zdi]], () => { prepareZD.run(); });
		});
	}


	function getDeviceStatus(onResult)
	{
		sendJsonRPCRequest("zd.cmd.get_fields_value", { names: ["DeviceStatus_LSW", "DeviceStatus_MSW"] }, (j) =>
		{
			const cnt = HIBYTE(j[1].raw);
			//if (cnt !== 0) 
			APICounters[zdi] = cnt;
			rawDeviceStatus = MAKELONG(j[0].raw, j[1].raw);
			DeviceStatus = (rawDeviceStatus >> 1) & 15;
			elems.labelDeviceStatus.textContent = DeviceStatus_2_toString(rawDeviceStatus);
			elems.labelAPICounter.textContent = APICounters[zdi] === null ? "null" : APICounters[zdi].toString();
			onResult();
		});
	}


	elems.buttonSwitchSendTestCommandsState.onclick = (e) =>
	{
		toSendTestCommands = !toSendTestCommands;
		elems.labelSendTestCommandsState.textContent = toSendTestCommands ? "Остановить" : "Начать";
		if (toSendTestCommands)
			cyclesCount = 0;

		toSendTestCommands ? sendCommands.run() : sendCommands.stop();

	};


	elems.sendTestData.onclick = (e) =>
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A0054, data: [01, 00, 03, 00, 00, 00, 04, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A4154, data: [00, 00, 05, 00, 00, 00, 06, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A4254, data: [00, 00, 07, 00, 00, 00, 08, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A0060, data: [01, 00, 0xFF, 0xFF, 00, 00, 00, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A4160, data: [00, 00, 00, 00, 00, 00, 00, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A4260, data: [00, 00, 00, 00, 00, 00, 00, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173AC360, data: [00, 00, 00, 00, 00, 00, 00, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A4354, data: [00, 00, 09, 00, 00, 00, 0x0A, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173A4454, data: [00, 00, 0x0B, 00, 00, 00, 0x0C, 00] });
		batch.pushRequest("zd.cmd.can.send_direct_msg", { id: 0x173AC554, data: [00, 00, 0x0D, 00, 00, 00, 00, 00] });

		batch.flush();
	};

	sendJsonRPCRequest("zd.cmd.enable_ignore_nopower");
	//initialize();
	//getDeviceStatus.run();
}