function main()
{
	const elems = getAJAXElemsWithID();
	const telemName = "foundObject";
	let textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);
	let textboxSearchON_value = textboxIntValueGuard(elems.textboxSearchON, 2);

	function updateButtonSearchONState() 
	{
		elems.buttonSearchON.disabled = !checkNumberValue(textboxSearchON_value);
	}

	function setTelemPeriod(period)
	{
		if (!checkNumberValue(period, true))
			return;

		Pilv.mko.setTelemPeriod(telemName, period);
	}


	function updateButtonSetTelemPeriodState()
	{
		elems.buttonSetTelemPeriod.disabled = !checkNumberValue(textboxSetTelemPeriod_value, false);
	}


	elems.textboxSetTelemPeriod.oninput = (e) =>
	{
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTelemPeriodState();
	};

	elems.textboxSetTelemPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	elems.buttonSetTelemPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTelemPeriod_value, true))
			return;
			
		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	const elemsByFieldNames = (() =>
	{
		let result = {};

		for (let key in elems) {
			if (key.substring(0, 2) == "td") {
				let fieldName = key.substring(2);
				result[fieldName] = elems[key];
			}
		}

		return result;
	})();

	//const fieldNames = objectToKeyArray(elemsByFieldNames);

	function updateDNStatus(j)
	{
		if (j.raw !== null) {
			const raw = j.raw;

			elems.tdDNStatus_0.textContent = j.formatted;
			elems.tdDNStatus_1.textContent = boolToYNString((raw >> 0) & 1 == 1);
			elems.tdDNStatus_2.textContent = boolToYNString((raw >> 1) & 1 == 1);
			elems.tdDNStatus_3.textContent = boolToYNString((raw >> 2) & 1 == 1);
			elems.tdDNStatus_4.textContent = boolToYNString((raw >> 3) & 1 == 1);
			elems.tdDNStatus_5.textContent = boolToYNString((raw >> 4) & 1 == 1);
			elems.tdDNStatus_6.textContent = boolToYNString((raw >> 5) & 1 == 1);
			elems.tdDNStatus_7.textContent = boolToYNString((raw >> 6) & 1 == 1);
		} else {
			elems.tdDNStatus_0.textContent = emptyValueString;
			elems.tdDNStatus_1.textContent = emptyValueString;
			elems.tdDNStatus_2.textContent = emptyValueString;
			elems.tdDNStatus_3.textContent = emptyValueString;
			elems.tdDNStatus_4.textContent = emptyValueString;
			elems.tdDNStatus_5.textContent = emptyValueString;
			elems.tdDNStatus_6.textContent = emptyValueString;
			elems.tdDNStatus_7.textContent = emptyValueString;
		}
	}

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();

		//batch.pushRequest("zd.cmd.can.get_last_preambule", { array_type: 0xFC }, updateWorldTime);
		const fieldNames = [
			"Y_dn1",
			"Z_dn1",
			"Val_dn1",
			"Size_dn1",
			"AV_dn1",
			"AVx_dn1",
			"AVy_dn1",
			"AVz_dn1",
			"p0_dn1",
			"p1_dn1",
			"ID_dn1",
			"QFrameNumber_dn1",
			"DELTIME_dn1",
		];

		batch.pushRequest("zd.cmd.get_fields_value", { names: fieldNames }, j => {
			j.forEach((v, index) => elems[`td${fieldNames[index]}`].textContent = v.raw !== null ? v.formatted : emptyValueString);
		});

		batch.pushRequest("zd.cmd.get_fields_value", { names: ["QTime_INT_dn1", "QTime_FR_dn1"] }, j => {
			elems.tdWorldTime.textContent = j[0].raw === null || j[1].raw === null ? emptyValueString : getWorldTimeString(j[0].raw, j[1].raw, clockPageOpened);
		});

		batch.pushRequest("zd.cmd.get_field_value", { name: "DNStatus" 			}, updateDNStatus);
	
		batch.pushRequest("zd.cmd.mko.is_telem_enabled", [telemName], j => {
			elems.checkboxTelem.checked = j;
			elems.buttonSetTelemPeriod.disabled = !(j && checkNumberValue(textboxSetTelemPeriod_value));
			elems.textboxSetTelemPeriod.disabled = !j;
		});

		const cont = () => this.continueTimeout(global.telemUpdateInterval);

		batch.flush(cont, cont);
	});

	Pilv.mko.getTelemPeriod(telemName, period => {
		const seconds = period / 1000;
		elems.textboxSetTelemPeriod.value = seconds.toString();
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);
	});

	elems.checkboxTelem.oninput = e => (e.currentTarget.checked ? Pilv.mko.enableTelem : Pilv.mko.disableTelem)(telemName);

	updateButtonSetTelemPeriodState();
	loop.run();

	elems.textboxSearchON.oninput = ({ currentTarget }) => {
		textboxSearchON_value = textboxIntValueGuard(currentTarget, 2);
		updateButtonSearchONState();
	};

	elems.buttonSearchON.onclick = ({ currentTarget }) => {
		const en = () => currentTarget.disabled = false;
		currentTarget.disabled = true;
		sendJsonRPCRequest("pilv.mko.set_search_on", [textboxSearchON_value], en, en);
	}

	updateButtonSearchONState();
}