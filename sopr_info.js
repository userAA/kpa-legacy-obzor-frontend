function main()
{
	const elems = getAJAXElemsWithID();
	let systemTime = new Date();
	let manualTime = zeroDate;
	let autoSetSytemTime = elems.checkboxAutoSetSystemTime.checked;
	let sendAPIWithPeriod = !videoOnly && elems.checkboxSendAPIWithPeriod.checked;
	let sendAPIPeriod = textboxIntValueGuard(elems.textboxSendAPIPeriod, 4);
	const soprInfo = new SoprInfo();
	let Timer_sec = textboxIntValueGuard(elems.textboxTimer_sec_ADDR, 4);
	let Timer_float = textboxIntValueGuard(elems.textboxTimer_float_ADDR, 4);
	let isAllDataCorrect = true;

	const fieldNamesByTextboxIDs = (() => 
	{
		let result = {};
		const prefix = "textbox";
		for (let key in soprInfo) {
			const elemID = sprintf("%s%s", prefix, key);
			result[elemID] = key;
		}

		return result;
	})();

	const textboxIDsByFieldNames = (() => 
	{
		let result = {};

		for (let key in fieldNamesByTextboxIDs) {
			result[fieldNamesByTextboxIDs[key]] = key;
		}

		return result;
	})();

	for (let key in soprInfo) {
		soprInfo[key] = 0;
		elems[textboxIDsByFieldNames[key]].value = soprInfo[key].toString();
	}

	elems.buttonSetMyTime.disabled = autoSetSytemTime;
	elems.buttonSetSystemTime.disabled = autoSetSytemTime;
	elems.buttonSetBSHV.disabled = sendAPIWithPeriod;
	elems.buttonSendAPI.disabled = sendAPIWithPeriod;

	function updateBSHVValues(date)
	{
		const clocks = (date) / 1000;
		Timer_sec = Math.floor(clocks);
		Timer_float = Math.floor((clocks - Timer_sec) * Math.pow(2, 32));

		elems.textboxTimer_sec_ADDR.value = sprintf("0x%08X", Timer_sec);
		elems.textboxTimer_float_ADDR.value = sprintf("0x%08X", Timer_float);
		textboxIntValueGuard(elems.textboxTimer_sec_ADDR, 4, false);
		textboxIntValueGuard(elems.textboxTimer_float_ADDR, 4, false);
		updateButtonSetBSHVState();
		updateTrSendAPIWithPeriodState();
	}


	const updateSystemTime = new AsyncLoopFunc(function()
	{
		systemTime = new Date(new Date().getTime() + new Date(0).getHours() * 60 * 60 * 1000);
		elems.labelSystemTime.textContent = dateToString(systemTime);

		if (autoSetSytemTime)
			updateBSHVValues(systemTime);

		this.continueTimeout(30);
	});


	const sendAPIWithPeriodLoop = new AsyncLoopFunc(function()
	{
		const cont = () => 
		{ 
			this.continueTimeout(sendAPIPeriod); 
		};
		
		if (isAllDataCorrect) {
			sendAPI(() => 
			{ 
				setBSHV(cont, cont); 
			}, cont);
		} else {
			cont();
		}
	});


	function setBSHV(onResult = null, onError = null)
	{
		if (!checkNumberValue(Timer_sec)) {
			return;
		}

		Pilv.setBSHV(Timer_sec, onResult, onError);
	}


	function sendAPI(onResult = null, onError = null)
	{
		for (let key in soprInfo) {
			if (!checkNumberValue(soprInfo[key])) {
				return;
			}
		}

		Pilv.sendSorpInfo(soprInfo, onResult, onError);
	}


	elems.buttonGetSystemTime.onclick = (e) => 
	{
		manualTime = new Date(systemTime);
		elems.textboxHours.value = manualTime.getUTCHours();
		elems.textboxMinutes.value = manualTime.getUTCMinutes();
		elems.textboxSeconds.value = manualTime.getUTCSeconds();
		elems.textboxMilliseconds.value = manualTime.getUTCMilliseconds();
		elems.textboxDay.value = manualTime.getUTCDate();
		elems.selectMonth.selectedIndex = manualTime.getUTCMonth();
		elems.textboxYear.value = manualTime.getUTCFullYear();
	};


	elems.checkboxAutoSetSystemTime.oninput = (e) =>
	{
		autoSetSytemTime = e.target.checked;
		elems.buttonSetMyTime.disabled = autoSetSytemTime;
		elems.buttonSetSystemTime.disabled = autoSetSytemTime;
	};


	elems.checkboxSendAPIWithPeriod.oninput = (e) => 
	{
		sendAPIWithPeriod = e.target.checked;
		elems.buttonSetBSHV.disabled = sendAPIWithPeriod;
		elems.buttonSendAPI.disabled = sendAPIWithPeriod;

		if (sendAPIWithPeriod) {
			sendAPIWithPeriodLoop.run();
		} else {
			sendAPIWithPeriodLoop.stop();
		}
	};


	elems.textboxSendAPIPeriod.oninput = (e) =>
	{
		const value = textboxIntValueGuard(e.target, 4, false, (v) => { return v >= 30; });

		if (checkNumberValue(value, false)) {
			sendAPIPeriod = value;
		}
	}


	elems.buttonSetMyTime.onclick = (e) => 
	{
		updateBSHVValues(manualTime);
	};


	elems.buttonSetSystemTime.onclick = (e) => 
	{
		updateBSHVValues(systemTime);
	};


	function updateButtonSendAPIState()
	{
		if (videoOnly) {
			elems.buttonSendAPI.disabled = true;
			return;	
		}

		let isCorrect = true;

		for (let key in soprInfo) {
			if (!checkNumberValue(soprInfo[key], false)) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonSendAPI.disabled = !isCorrect;
	}


	function updateButtonSetBSHVState()
	{
		elems.buttonSetBSHV.disabled = videoOnly || (!checkNumberValue(Timer_sec, false) || !checkNumberValue(Timer_float, false));
	}


	function updateTrSendAPIWithPeriodState()
	{
		let isCorrect = true;

		for (let key in soprInfo) {
			if (!checkNumberValue(soprInfo[key], false)) {
				isCorrect = false;
				break;
			}
		}

		if (isCorrect) {
			isCorrect = (checkNumberValue(Timer_sec, false) && checkNumberValue(Timer_float, false));
		}

		isAllDataCorrect = isCorrect;
		elems.trSendAPIWithPeriod.style.background = isCorrect ? "white" : "lightcoral";
	}


	elems.buttonSendAPI.onclick = (e) => { sendAPI(); };
	elems.buttonSetBSHV.onclick = (e) => { setBSHV(); };

	function checkUint32Textbox(e) 
	{
		const value = textboxIntValueGuard(e.target, 4, false);
		const fieldName = fieldNamesByTextboxIDs[e.target.id];
		soprInfo[fieldName] = value;
		updateButtonSendAPIState();
		updateTrSendAPIWithPeriodState();
	}


	function checkUint8Textbox(e) 
	{
		const value = textboxIntValueGuard(e.target, 1, false);
		const fieldName = fieldNamesByTextboxIDs[e.target.id];
		soprInfo[fieldName] = value;
		updateButtonSendAPIState();
		updateTrSendAPIWithPeriodState();
	}


	function checkInt8Textbox(e) 
	{
		const value = textboxIntValueGuard(e.target, 1, true);
		const fieldName = fieldNamesByTextboxIDs[e.target.id];
		soprInfo[fieldName] = value;
		updateButtonSendAPIState();
		updateTrSendAPIWithPeriodState();
	}

	elems.textboxQTime_INT.oninput = checkUint32Textbox;
	elems.textboxQTime_FR.oninput = checkUint32Textbox;
	elems.textboxRx.oninput = checkUint32Textbox;
	elems.textboxRy.oninput = checkUint32Textbox;
	elems.textboxRz.oninput = checkUint32Textbox;
	elems.textboxVx.oninput = checkUint32Textbox;
	elems.textboxVy.oninput = checkUint32Textbox;
	elems.textboxVz.oninput = checkUint32Textbox;
	elems.textboxQ0_API.oninput = checkUint32Textbox;
	elems.textboxQ1_API.oninput = checkUint32Textbox;
	elems.textboxQ2_API.oninput = checkUint32Textbox;
	elems.textboxQ3_API.oninput = checkUint32Textbox;
	elems.textboxIS_Q_STATUS.oninput = checkUint8Textbox;
	elems.textboxwx.oninput = checkInt8Textbox;
	elems.textboxwy.oninput = checkInt8Textbox;
	elems.textboxwz.oninput = checkInt8Textbox;

	elems.textboxTimer_sec_ADDR.oninput = (e) =>
	{
		Timer_sec = textboxIntValueGuard(e.target, 4, false);
		updateButtonSetBSHVState();
		updateTrSendAPIWithPeriodState();
	};


	elems.textboxTimer_float_ADDR.oninput = (e) =>
	{
		Timer_float = textboxIntValueGuard(e.target, 4, false);
		updateButtonSetBSHVState();
		updateTrSendAPIWithPeriodState();
	};

	for (let key in textboxIDsByFieldNames) {
		const elem = elems[textboxIDsByFieldNames[key]];
		elem.oninput({target: elem});
	}

	updateButtonSetBSHVState();
	updateTrSendAPIWithPeriodState();
	updateSystemTime.run();

	if (sendAPIWithPeriod) {
		sendAPIWithPeriodLoop.run();
	}

	elems.checkboxSendAPIWithPeriod.disabled = videoOnly;
}	