function main() {
	const elems = getAJAXElemsWithID();
	const telemName = "regularInfo";
	const clockPageOpened = (new Date()) - (new Date(1970, 0, 1, 0, 0, 0, 0));
	let textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);

	function setTelemPeriod(period)
	{
		if (!checkNumberValue(period, true))
			return;

		Pilv.mko.setTelemPeriod(telemName, period);
	}


	function updateButtonSetTelemPeriodState()
	{
		elems.buttonSetTelemPeriod.disabled = !checkNumberValue(textboxSetTelemPeriod_value, false);
	}


	elems.textboxSetTelemPeriod.oninput = (e) =>
	{
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(e.target);
		updateButtonSetTelemPeriodState();
	};

	elems.textboxSetTelemPeriod.onkeyup = (e) =>
	{
		if (e.key != "Enter")
			return;

		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	elems.buttonSetTelemPeriod.onclick = (e) =>
	{
		if (!checkNumberValue(textboxSetTelemPeriod_value, true))
			return;
			
		setTelemPeriod(textboxSetTelemPeriod_value);
	};

	const elemsByFieldNames = (() =>
	{
		let result = {};

		for (let key in elems) {
			if (key.substring(0, 2) == "td") {
				let fieldName = key.substring(2);
				result[fieldName] = elems[key];
			}
		}

		return result;
	})();

	const fieldNames = objectToKeyArray(elemsByFieldNames);

	const loop = new AsyncLoopFunc(function()
	{
		const batch = new JsonRPCBatch();
		
		//batch.pushRequest("zd.cmd.get_fields_value", { names: ["QTime_INT", "QTime_FR"] }, j => {
		//	elems.tdWorldTime.textContent = j[0].raw === null || j[1].raw === null ? emptyValueString : getWorldTimeString(j[0].raw, j[1].raw, clockPageOpened);
		//});

		batch.pushRequest("zd.cmd.get_fields_value", { names: ["Q0", "Q1", "Q2", "Q3", "DELTIME", "QFrameNumber"] }, (j) =>
		{
			elems.tdQ0.textContent = j[0].raw !== null ? j[0].formatted : emptyValueString;
			elems.tdQ1.textContent = j[1].raw !== null ? j[1].formatted : emptyValueString;
			elems.tdQ2.textContent = j[2].raw !== null ? j[2].formatted : emptyValueString;
			elems.tdQ3.textContent = j[3].raw !== null ? j[3].formatted : emptyValueString;
			elems.tdDELTIME.textContent = j[4].raw !== null ? j[4].formatted : emptyValueString;
			elems.tdQFrameNumber.textContent = j[5].raw !== null ? j[5].formatted : emptyValueString;
		});

		batch.pushRequest("zd.cmd.get_fields_value", { names: ["QTime_INT", "QTime_FR"] }, (j) =>
		{
			elems.tdWorldTime.textContent = j[0].raw === null || j[1].raw === null ? emptyValueString : getWorldTimeString(j[0].raw, j[1].raw, clockPageOpened);
		});

		batch.pushRequest("zd.cmd.get_field_value", { name: "QStatus" }, (j) => 
		{
			if (j.raw !== null) {
				const raw = j.raw;

				elems.tdQStatus_0.textContent = j.formatted;
				elems.tdQStatus_1.textContent = boolToYNString((raw >> 0) & 1 == 1);
				elems.tdQStatus_2.textContent = boolToYNString((raw >> 1) & 1 == 1);
				elems.tdQStatus_3.textContent = boolToYNString((raw >> 2) & 1 == 1);
				elems.tdQStatus_4.textContent = boolToYNString((raw >> 3) & 1 == 1);
				elems.tdQStatus_5.textContent = boolToYNString((raw >> 4) & 1 == 1);
				elems.tdQStatus_6.textContent = ((raw >> 5) & 15).toString();
				elems.tdQStatus_7.textContent = ((raw >> 9) & 15).toString();
			} else {
				elems.tdQStatus_0.textContent = emptyValueString;
				elems.tdQStatus_1.textContent = emptyValueString;
				elems.tdQStatus_2.textContent = emptyValueString;
				elems.tdQStatus_3.textContent = emptyValueString;
				elems.tdQStatus_4.textContent = emptyValueString;
				elems.tdQStatus_5.textContent = emptyValueString;
				elems.tdQStatus_6.textContent = emptyValueString;
				elems.tdQStatus_7.textContent = emptyValueString;
			}
		});


		batch.pushRequest("zd.cmd.get_field_value", { name: "RSpeed" }, (j) =>
		{
			if (j.raw === null) {
				elems.tdRSpeed_0.textContent = emptyValueString;
				elems.tdRSpeed_1.textContent = emptyValueString;
				elems.tdRSpeed_2.textContent = emptyValueString;
				elems.tdRSpeed_3.textContent = emptyValueString;
				return;
			}
			
			const raw = j.raw;
			const getInt = (input) =>
			{
				let input0 = 0;
				
				if ((input >> 9) == 1) {
					input0 = input & 0x1FF;
					input0 ^= 0x1FF;
					input0 = -1.0 * (input0 + 1);
				} else {
					input0 = 1.0 * input;
				}

				input0 = input0 * Math.pow(2.0,-12) * 180.0 / 3.14;
				return input0.toFixed(3);
			};

			const getInt2 = (input) =>
			{
				let input0 = 0;

				if ((input >> 10) == 1) {
				    input0 = input & 0x3FF;
				    input0 ^= 0x3FF;
				    input0 = -1.0 * (input0 + 1);
				} else {
				    input0 = 1.0 * input;
				}

				input0 = input0 * Math.pow(2.0, -13) * 180.0 / 3.14;
				return input0.toFixed(3);
			};

			elems.tdRSpeed_0.textContent = j.formatted;
			elems.tdRSpeed_1.textContent = getInt((raw >> 0) & 1023);
			elems.tdRSpeed_2.textContent = getInt2((raw >> 10) & 2047);
			elems.tdRSpeed_3.textContent = getInt2((raw >> 21) & 2047);
		});

		batch.pushRequest("zd.cmd.mko.is_telem_enabled", [telemName], j => {
			elems.checkboxTelem.checked = j;
			elems.buttonSetTelemPeriod.disabled = !(j && checkNumberValue(textboxSetTelemPeriod_value));
			elems.textboxSetTelemPeriod.disabled = !j;
		});

		batch.flush(() =>
		{
			this.continueTimeout(global.telemUpdateInterval);
		});
	});

	Pilv.mko.getTelemPeriod(telemName, period => {
		const seconds = period / 1000;
		elems.textboxSetTelemPeriod.value = seconds.toString();
		textboxSetTelemPeriod_value = textboxSetArrayTypePeriodValueGuard(elems.textboxSetTelemPeriod);
	});

	elems.checkboxTelem.oninput = e => (e.currentTarget.checked ? Pilv.mko.enableTelem : Pilv.mko.disableTelem)(telemName);

	updateButtonSetTelemPeriodState();
	loop.run();
}