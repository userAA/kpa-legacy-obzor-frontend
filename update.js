function main()
{
	const elems = getAJAXElemsWithID();
	let is_runningPrev = false;
	let deviceFileMaxSize;
	let PMOBoardFileMaxSize;
	let kpaFileMaxSize;

	function setKPALoadingProgress(value)
	{
		setLabelPercentageValue(elems.labelKPALodingPercentage, value);
		setProgressValue(elems.progressKPALoding, value);
	}


	

	// todo: как-то обобщить эту функцию
	function setKPALoadingProgressSmooth(value)
	{
		const prevValue = elems.progressKPALoding.value / elems.progressKPALoding.max;
		const changeTotal = value - prevValue;
		const steps = 5;
		const changeStep = changeTotal / steps;
		let stepCounter = 0;
		let currentValue = prevValue;

		let update = () =>
		{
			if (stepCounter < steps) {
				setLabelPercentageValue(elems.labelKPALodingPercentage, currentValue);
				setProgressValue(elems.progressKPALoding, currentValue);
				currentValue += changeStep;
				++stepCounter;
				setTimeout(update, 1000 / 60);
			} else {
				setLabelPercentageValue(elems.labelKPALodingPercentage, value);
				setProgressValue(elems.progressKPALoding, value);
			}
		};

		update();
	}

	function setDeviceLoadingProgress(value)
	{
		setLabelPercentageValue(elems.labelDeviceLodingPercentage, value);
		setProgressValue(elems.progressDeviceLoding, value);
	}


	function setDeviceLoadingProgressSmooth(value)
	{
		const prevValue = elems.progressDeviceLoding.value / elems.progressDeviceLoding.max;
		const changeTotal = value - prevValue;
		const steps = 5;
		const changeStep = changeTotal / steps;
		let stepCounter = 0;
		let currentValue = prevValue;
		let update = () =>
		{
			setLabelPercentageValue(elems.labelDeviceLodingPercentage, currentValue);
			setProgressValue(elems.progressDeviceLoding, currentValue);
			currentValue += changeStep;

			if (stepCounter < steps) {
				++stepCounter;
				setTimeout(update, 1000 / 60)
			}
		};

		update();
	}


	function setProgressValue(elemProgress, value)
	{
		elemProgress.value = Math.floor(value * elemProgress.max);
	}


	function setLabelPercentageValue(elemLabel, value)
	{
		elemLabel.textContent = sprintf("%d", Math.floor(value * 100));
	}


	function updateIsRunningPrevVar(v)
	{
		is_runningPrev = v;
	}
	

	function onDeviceUpdateStart()
	{
		let wasServerConnectError = false;

		sendJsonRPCRequest("zd.update.get_progress", null, (j) =>
		{
			if (j.is_running) {
				elems.filePMOBoard.disabled = true;
				elems.labelDeviceLoadingStage.textContent = 
				j.bytes_total != 0 ? sprintf("%s, записано байт %d из %d", j.stage_name, j.bytes_transferred, j.bytes_total) : 
				j.stage_name;
				setDeviceLoadingProgress(j.completed);
				setTimeout(onDeviceUpdateStart, global.telemUpdateInterval);
			} else if (j.hasOwnProperty("error") || wasServerConnectError) {
				elems.labelDeviceLoadingStage.textContent = 
					wasServerConnectError ? "Работа сервера была прервана" :
					sprintf("%s на этапе \"%s\"", j.error, j.stage_name);

				wasServerConnectError = false;
				sendJsonRPCRequest("zd.update.get_validation_status", null, (j) =>
				{
					elems.fileDevice.disabled = false;
					elems.buttonFileDevice.disabled = false;

					if (j) {
						elems.buttonFileDevice.textContent = "Попробовать ещё раз";
						elems.buttonFileDevice.onclick = function()
						{
							elems.fileDevice.disabled = true;
							elems.buttonFileDevice.disabled = true;
							onDeviceFileLoad();
						};

						enableTopMenu();
						enableBottomMenu();
					} else {
						elems.fileDevice.disabled = false;
						elems.fileDevice.value = "";
					}
				});
			} else if (is_runningPrev) {
				onDeviceUpdateEnd();
			} else {
				update_onload();
			}

			updateIsRunningPrevVar(j.is_running);
		}, null, () => { wasServerConnectError = true; });
	}


	function onDeviceUpdateEnd()
	{
		elems.labelDeviceLoadingStage.textContent = "Обновление завершено";
		elems.buttonFileDevice.disabled = true;
		elems.buttonFileDevice.onclick = buttonFileDevice_onclick;
		elems.buttonFileDevice.textContent = "Обновить";
		elems.fileDevice.disabled = false;
		elems.fileDevice.value = "";

		elems.fileKPA.disabled = false;

		setDeviceLoadingProgress(1.0);
		enableTopMenu();
		enableBottomMenu();
	}


	function onDeviceFileLoad()
	{
		showProgressDevice();
		sendJsonRPCRequest("zd.update.start", null, onDeviceUpdateStart, function()
		{
			elems.fileKPA.disabled = false;
			elems.fileDevice.disabled = false;
			elems.buttonFileDevice.onclick = buttonFileDevice_onclick;
			elems.buttonFileDevice.textContent = "Обновить";
		});
	}


	function showProgressDevice()
	{
		elems.trDeviceLoadingInfo.hidden = false;
		buttonFileDevice.disabled = true;
		elems.fileDevice.disabled = true;
		disableTopMenu();
		//disableBottomMenu();

		disableBottomMenuZDRequests();
	}


	function checkIfDeviceUpdateIsRunning()
	{
		sendJsonRPCRequest("zd.update.get_validation_status", null, 
			function (j)
			{
				if (j) {
					Popup.showOKCancel("Внимание!", 
						"Предыдущее обновление ПО прибора не было завершено. Начать обновление заново, используя ранее загруженный файл?", 
						null, function(j)
						{
							showProgressDevice();
							onDeviceFileLoad();
						}, "Нет", "Да"
					);
				}
			}
		);
	}


	// elems.buttonFileKPA.onclick = (e) =>
	// {
	// 	let sender = e.target;
	// 	let kpaFakeProgress = null;

	// 	if (elems.fileKPA.value.length == 0) {
	// 		Popup.showInformation("Ошибка!", "Файл не был выбран.");
	// 		return;
	// 	}

	// 	var fr = new FileReader();
	// 	fr.onloadend = function(e)
	// 	{
	// 		if(e.target.readyState != FileReader.DONE) {
	// 			switch (e.target.readyState) {
	// 				case FileReader.EMPTY:
	// 					Popup.showInformation("Ошибка!", "Выбран пустой файл.");
	// 				break;
	// 			}

	// 			return;
	// 		}

	// 		sendJsonRPCRequest("update.prepare", null, (j) => 
	// 		{
	// 			elems.trKPALoadingInfo.hidden = false;
	// 			sendHttpPostRequest(global.cmdEP, e.target.result, (r) =>
	// 			{
	// 				elems.labelKPALoadingStage.textContent = "Обновление. Не выключайте компьютер, идёт перезагрузка ПО";
	// 				kpaFakeProgress = new FakeProgress(elems.progressKPALoding);
	// 				kpaFakeProgress.start(setKPALoadingProgress, setKPALoadingProgressSmooth);
	// 				//disableConnectionErrorPopups();

	// 				sendJsonRPCRequest("update.get_validation_status", null, (j) =>
	// 				{
	// 					if (j) {
	// 						sendJsonRPCRequest("update.start");
	// 					} else {
	// 						Popup.showInformation("Ошибка обновления.", "Неизвестная ошибка...");
	// 						enableTopMenu();
	// 						enableBottomMenu();
	// 						enableConnectionErrorPopups();
	// 						sender.disabled = false;
	// 						elems.fileKPA.disabled = false;
	// 						kpaFakeProgress.end();
	// 						elems.trKPALoadingInfo.hidden = true;
	// 					}
	// 				});
	// 				// todo: проверить, началось ли обновление...
	// 				setTimeout(() =>// проверка на работу сервера через 1 секунду
	// 				{
	// 					const checkServer = new AsyncLoopFunc(function()
	// 					{
	// 						checkPing(() =>
	// 						{
	// 							disableConnectionErrorPopups();
							
	// 							if (kpaFakeProgress) {
	// 								kpaFakeProgress.end();
	// 							}
	
	// 							enableTopMenu();
	// 							enableBottomMenu();
	// 							enableConnectionErrorPopups();
	// 							sender.disabled = false;
	// 							elems.fileKPA.disabled = false;
	// 							elems.labelKPALoadingStage.textContent = "Обновление завершено";
	// 						},
						
	// 						() => { this.continueTimeout(1000); });
	// 					});

	// 					checkServer.run();
	// 				}, 1000);
	// 			}, // !function(r)

	// 			[new HttpHeader(j.http_header_name, j.http_header_value)], null, 

	// 			(e) => 
	// 			{
	// 				if (!e.lengthComputable || e.total == 0) 
	// 					return;
	// 				var value = e.total / e.loaded;
	// 				setKPALoadingProgressSmooth(value);
	// 			}); // !sendHttpPostRequest(cmdEP, e.target.result,...
	// 		}); // !sendJsonRPCRequest("update_kpa_software", null, function(j) 
	// 	} // !fr.onloadend = function(e)

	// 	let files = elems.fileKPA.files;
	// 	fr.readAsArrayBuffer(files[0]);
	// 	sender.disabled = true;
	// 	elems.fileKPA.disabled = true;
	// 	disableTopMenu();
	// 	disableBottomMenu();
	// };


	function prepareDeviceFile(e)
	{
		sendJsonRPCRequest("zd.update.prepare", null, (j) => 
		{
			showProgressDevice();
			sendHttpPostRequest(global.cmdEP, e.target.result, (r) =>
			{
				sendJsonRPCRequest("zd.update.get_validation_status", null, (j) =>
				{
					if (j) {
						onDeviceFileLoad();
					} else {
						Popup.showInformation("Ошибка обновления.", "Неизвестная ошибка...");
					}
				});
				// todo: проверить, началось ли обновление...
			}, // !function(r)

			[new HttpHeader(j.http_header_name, j.http_header_value)], null, 

			(e) =>
			{
				if (!e.lengthComputable || e.total == 0) 
					return;

				var value = e.total / e.loaded;
				setDeviceLoadingProgressSmooth(value);
			});
		});
	}


	function buttonFileDevice_onclick(e)
	{
		if (elems.fileDevice.files.length == 0) {
			Popup.showInformation("Ошибка!", "Файл не был выбран.");
			return;
		}

		var fr = new FileReader();
		fr.onloadend = (e) =>
		{
			if(e.target.readyState != FileReader.DONE) {
				switch (e.target.readyState) {
					case FileReader.EMPTY:
						Popup.showInformation("Ошибка!", "Выбран пустой файл.");
					break;
				}

				return;
			}

			prepareDeviceFile(e);
		}; // !onloadend...

		let files = elems.fileDevice.files;
		fr.readAsArrayBuffer(files[0]);
		showProgressDevice();
	}

	elems.buttonFileDevice.onclick = buttonFileDevice_onclick;



	/////////////////////

	function setPMOBoardLoadingProgress(value)
	{
		setLabelPercentageValue(elems.labelPMOBoardLodingPercentage, value);
		setProgressValue(elems.progressPMOBoardLoding, value);
	}


	function setPMOBoardLoadingProgressSmooth(value)
	{
		const prevValue = elems.progressPMOBoardLoding.value / elems.progressPMOBoardLoding.max;
		const changeTotal = value - prevValue;
		const steps = 5;
		const changeStep = changeTotal / steps;
		let stepCounter = 0;
		let currentValue = prevValue;
		let update = () =>
		{
			setLabelPercentageValue(elems.labelPMOBoardLodingPercentage, currentValue);
			setProgressValue(elems.progressPMOBoardLoding, currentValue);
			currentValue += changeStep;

			if (stepCounter < steps) {
				++stepCounter;
				setTimeout(update, 1000 / 60)
			}
		};

		update();
	}

	function onPMOBoardUpdateStart()
	{
		let wasServerConnectError = false;

		sendJsonRPCRequest("kpa.update.get_progress", null, (j) =>
		{
			if (j.is_running) {
				elems.filePMOBoard.disabled = true;
				elems.labelPMOBoardLoadingStage.textContent = 
				j.bytes_total != 0 ? sprintf("%s, записано байт %d из %d", j.stage_name, j.bytes_transferred, j.bytes_total) : 
				j.stage_name;
				setPMOBoardLoadingProgress(j.completed);
				setTimeout(onPMOBoardUpdateStart, global.telemUpdateInterval);
			} else if (j.hasOwnProperty("error") || wasServerConnectError) {
				elems.labelPMOBoardLoadingStage.textContent = 
					wasServerConnectError ? "Работа сервера была прервана" :
					sprintf("%s на этапе \"%s\"", j.error, j.stage_name);

				wasServerConnectError = false;
				sendJsonRPCRequest("kpa.update.get_validation_status", null, (j) =>
				{
					elems.filePMOBoard.disabled = false;
					elems.buttonFilePMOBoard.disabled = false;

					if (j) {
						elems.buttonFilePMOBoard.textContent = "Попробовать ещё раз";
						elems.buttonFilePMOBoard.onclick = function()
						{
							elems.filePMOBoard.disabled = true;
							elems.buttonFilePMOBoard.disabled = true;
							onPMOBoardFileLoad();
						};

						enableTopMenu();
						enableBottomMenu();
					} else {
						elems.filePMOBoard.disabled = false;
						elems.filePMOBoard.value = "";
					}
				});
			} else if (is_runningPrev) {
				onPMOBoardUpdateEnd();
			} else {
				onPMOBoardFileLoad();
			}

			updateIsRunningPrevVar(j.is_running);
		}, null, () => { wasServerConnectError = true; });
	}


	function onPMOBoardUpdateEnd()
	{
		elems.labelPMOBoardLoadingStage.textContent = "Обновление завершено";
		elems.buttonFilePMOBoard.disabled = true;
		elems.buttonFilePMOBoard.onclick = buttonFilePMOBoard_onclick;
		elems.buttonFilePMOBoard.textContent = "Обновить";
		elems.filePMOBoard.disabled = false;
		elems.filePMOBoard.value = "";

		setPMOBoardLoadingProgress(1.0);
		enableTopMenu();
		enableBottomMenu();
	}

	function onPMOBoardFileLoad()
	{
		showProgressPMOBoard();
		sendJsonRPCRequest("kpa.update.start", null, onPMOBoardUpdateStart, function()
		{
			elems.filePMOBoard.disabled = false;
			elems.buttonFilePMOBoard.onclick = buttonFilePMOBoard_onclick;
			elems.buttonFilePMOBoard.textContent = "Обновить";
		});
	}


	function showProgressPMOBoard()
	{
		elems.trPMOBoardLoadingInfo.hidden = false;
		buttonFilePMOBoard.disabled = true;
		elems.filePMOBoard.disabled = true;
		disableTopMenu();
		//disableBottomMenu();

		disableBottomMenuZDRequests();
	}

	function preparePMOBoardFile(e)
	{
		sendJsonRPCRequest("kpa.update.prepare", null, (j) => 
		{
			showProgressPMOBoard();
			sendHttpPostRequest(global.cmdEP, e.target.result, (r) =>
			{
				sendJsonRPCRequest("kpa.update.get_validation_status", null, (j) =>
				{
					if (j) {
						onPMOBoardFileLoad();
					} else {
						Popup.showInformation("Ошибка обновления.", "Неизвестная ошибка...");
					}
				});
				// todo: проверить, началось ли обновление...
			}, // !function(r)

			[new HttpHeader(j.http_header_name, j.http_header_value)], null, 

			(e) =>
			{
				if (!e.lengthComputable || e.total == 0) 
					return;

				var value = e.total / e.loaded;
				setPMOBoardLoadingProgressSmooth(value);
			});
		});
	}


	function buttonFilePMOBoard_onclick(e)
	{
		if (elems.filePMOBoard.files.length == 0) {
			Popup.showInformation("Ошибка!", "Файл не был выбран.");
			return;
		}

		var fr = new FileReader();
		fr.onloadend = (e) =>
		{
			if(e.target.readyState != FileReader.DONE) {
				switch (e.target.readyState) {
					case FileReader.EMPTY:
						Popup.showInformation("Ошибка!", "Выбран пустой файл.");
					break;
				}

				return;
			}

			preparePMOBoardFile(e);
		}; // !onloadend...

		let files = elems.filePMOBoard.files;
		fr.readAsArrayBuffer(files[0]);
		showProgressPMOBoard();
	}

	elems.buttonFilePMOBoard.onclick = buttonFilePMOBoard_onclick;

	function checkIfPMOBoardUpdateIsRunning()
	{
		sendJsonRPCRequest("zd.update.get_validation_status", null, 
			function (j)
			{
				if (j) {
					Popup.showOKCancel("Внимание!", 
						"Предыдущее обновление ПМО платы не было завершено. Начать обновление заново, используя ранее загруженный файл?", 
						null, function(j)
						{
							showProgressPMOBoard();
							onPMOBoardFileLoad();
						}, "Нет", "Да"
					);
				}
			}
		);
	}

	///////////////////////




	// elems.fileKPA.onchange = (e) =>
	// {
	// 	let guardResult = inputFileMaxSizeGuard(e.target, kpaFileMaxSize);
	// 	elems.buttonFileKPA.disabled = !guardResult;

	// 	if (guardResult === null) 
	// 		return;

	// 	elems.buttonFileKPA.textContent = "Обновить";
	// }


	elems.fileDevice.onchange = (e) =>
	{
		let guardResult = inputFileMaxSizeGuard(e.target, deviceFileMaxSize);
		elems.buttonFileDevice.disabled = !guardResult;

		if (guardResult === null) 
			return;

		elems.buttonFileDevice.textContent = "Обновить";
	}


	elems.filePMOBoard.onchange = (e) =>
	{
		let guardResult = inputFileMaxSizeGuard(e.target, PMOBoardFileMaxSize);
		elems.buttonFilePMOBoard.disabled = !guardResult;

		if (guardResult === null) 
			return;

		elems.buttonFilePMOBoard.textContent = "Обновить";
	}


	//elems.trKPALoadingInfo.hidden = true;
	elems.trDeviceLoadingInfo.hidden = true;
	elems.trPMOBoardLoadingInfo.hidden = true;

	// sendJsonRPCRequest("update.get_max_file_size", null, function(j)
	// {
	// 	kpaFileMaxSize = j;
	// 	elems.labelFileKpaMax.textContent = sprintf("%.2f", j / (1024 * 1024));
	// });

	sendJsonRPCRequest("zd.update.get_max_file_size", null, function(j)
	{
		deviceFileMaxSize = j;
		elems.labelFileDeviceMax.textContent = sprintf("%.2f", j / (1024 * 1024));
	});

	sendJsonRPCRequest("kpa.update.get_max_file_size", null, function(j)
	{
		PMOBoardFileMaxSize = j;
		elems.labelFilePMOBoardMax.textContent = sprintf("%.2f", j / (1024 * 1024));
	});
	
	sendJsonRPCRequest("zd.update.get_progress", null, (j) =>
	{
		if (j.is_running) {
			showProgressDevice();
			onDeviceUpdateStart();
			return;
		}
		
		checkIfDeviceUpdateIsRunning();
	});

	sendJsonRPCRequest("kpa.update.get_progress", null, (j) =>
	{
		if (j.is_running) {
			showProgressPMOBoard();
			onPMOBoardUpdateStart();
			return;
		}
		
		checkIfPMOBoardUpdateIsRunning();
	});
}

	




