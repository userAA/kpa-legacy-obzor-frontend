const basicElems = getElemsWithID();
const elemsAvailableOnUpdate = document.getElementsByClassName("available-on-update");

let global = {
	cmdPort: null,
	isConnectionErrorPopupsEnabled: true,
	areHttpRequestsEnabled: true,
	isTopMenuEnabled: true,
	wasConnectionError: false,

	callbacks: {
		onserverreload: null
	},

	bottomMenuUpdateInterval: 200,
	telemUpdateInterval: 1000 / 20,
	isPageChanged: false,
	isPowerEnabled: false,
	beforeZDBoardAvailableCount: 0,
	ipConfig: null,
	hostname: "",
	isMKO: true,
};

const asyncSendJSONRPCRequest = async (method, params = null) => (
	new Promise((resolve, reject) => {
		sendJsonRPCRequest(method, params, resolve, reject, reject);
	})
);

const clientIDInt = generateIntID();
const clientIDString = clientIDInt.toString();
const zeroDate = new Date(1970, 0, 1, 0, 0, 0, 0);
const videoOnly = false;
const emptyValueString = "...";

function enableConnectionErrorPopups()
{
	global.isConnectionErrorPopupsEnabled = true;
}


function disableConnectionErrorPopups()
{
	global.isConnectionErrorPopupsEnabled = false;
}


function enableHttpRequests()
{
	global.areHttpRequestsEnabled = true;	
	enableBottomMenu();
}


function disableHttpRequests()
{
	global.areHttpRequestsEnabled = false;
	disableBottomMenu();
}


function AJAXLoadScript(elem) 
{
	var newElem = document.createElement("script");
	newElem.src = elem.src;
	elem.appendChild(newElem);
	newElem.onload = elem.onload;
}

// !!!!! скрипты автоматически не загружаются, пришлось писать костыль !!!!!!
function AJAXLoadScripts()
{
	var scripts = basicElems.AJAXContent.querySelectorAll("script");

	for (var i = 0; i < scripts.length; ++i) {
		AJAXLoadScript(scripts[i]);
	}
}


function AJAXOnResponse(response)
{
	basicElems.AJAXContent.innerHTML = response;
	AJAXLoadScripts();
}


class AsyncLoopFunc {
	static db = {};
	static prevPauseStates = {};

	constructor(func, isGlobal = false, onStop = null)
	{
		this.shouldStop = false;
		this.isRunning = false;
		this.func = func;
		this.id = generateStringID();
		this.isPause = false;
		this.isContinueCalled = false;
		this.isGlobal = isGlobal;
		this.onStop = onStop;
		AsyncLoopFunc.db[this.id] = this;
	}

	run()
	{
		//if (this.isRunning && !this.shouldStop) 
		//	throw new Error("Циклическая асинхронная функция уже запущена.");

		if (!this.func)
			throw new Error("Функция для запуска не установлена.");

		this.shouldStop = false;
			
		//if (!this.isRunning) {
			this.isRunning = true;
			this.func.call(this);
	//	}

		this.isContinueCalled = false;
	}

	stop()
	{
		this.shouldStop = true;
		delete AsyncLoopFunc.db[this.id];
	}

	pause()
	{
		//if (!this.isRunning) 
		//	throw new Error("Циклическая асинхронная функция не была запущена.");

		this.isPause = true;
	}

	continueRunning()
	{
		//if (!this.isRunning) 
		//	throw new Error("Циклическая асинхронная функция не была запущена.");
//
		//if (!this.isPause)
		//	throw new Error("Циклическая асинхронная функция не была приостановлена.");
		
		this.isPause = false;
	}

	static stopAll()
	{
		for (let key in AsyncLoopFunc.db) {
			const obj = AsyncLoopFunc.db[key]; 
			
			if (!obj.isGlobal) { 
				obj.stop();
			}
		}
	}

	static pauseAll()
	{
		for (let key in AsyncLoopFunc.db) {
			let instance = AsyncLoopFunc.db[key];
			AsyncLoopFunc.prevPauseStates[key] = instance.isPause;

			if (!instance.isPause) {
				instance.pause();
			}
		}
	}

	static continueRunningAll()
	{
		for (let key in AsyncLoopFunc.db) {
			let instance = AsyncLoopFunc.db[key];

			if (!AsyncLoopFunc.prevPauseStates[key]) {
				instance.continueRunning();
			}
		}

		AsyncLoopFunc.prevPauseStates = {};
	}

	continueTimeout(timeout = null)
	{
		if (this.shouldStop) {
			this.isRunning = false;

			if (this.onStop) {
				this.onStop();
			}

			return; 
		}
		
		if (this.isPause) {
			setTimeout(this.continueTimeout.bind(this));
			return;
		}

		this.isContinueCalled = true;
		setTimeout(this.func.bind(this), timeout);
	}
};



const beforeZDBoardAvailableCounter = new AsyncLoopFunc(function()
{
	const secs = (global.beforeZDBoardAvailableCount % 60);
	const mins = Math.trunc(global.beforeZDBoardAvailableCount / 60);

	if (mins >= 5) {
		basicElems.labelUSBNotAvailableTimeCounter.textContent = "> 5:00";
		this.stop();
	} else {
		basicElems.labelUSBNotAvailableTimeCounter.textContent = sprintf("%d:%02d", mins, secs);
	}

	global.beforeZDBoardAvailableCount++;
	this.continueTimeout(1000);
}, true);



function AJAXLoadPage(link) 
{
	sendJsonRPCRequest("zd.update.get_progress", null, (j) =>
	{
		j.is_running ? disableTopMenu() : enableTopMenu();
	});

	global.isPageChanged = true;
	AsyncLoopFunc.stopAll();
	sendHttpGetRequest(`http://${location.host}/${link}`, AJAXOnResponse);
}


let zdInitializationErrorShowed = false;
const checkZD = new AsyncLoopFunc(function()
{
	const cont = () =>
	{
		this.continueTimeout(5000);
	}

	const checkZDStream = () =>
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("zd.is_stream_enabled", null, (j) =>
		{
			if (!j) {
				sendJsonRPCRequest("zd.start_stream", null, videoOnly ? null : (j) =>
				{
					if (!global.isMKO) {
						sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: 0x44 }, cont, cont);
					}
				}, cont);
			} else {
				cont();
			}
		});

		if (global.isMKO) {
			batch.pushRequest("zd.ensure_mko_active");
		}

		batch.flush();
	};

	sendJsonRPCRequest("zd.is_initialized", null, (j) =>
	{
		if (!j) {
			sendJsonRPCRequest("zd.initialize", null, (j) =>
			{
				checkZDStream();
			}, () => 
			{ 
				zdInitializationErrorShowed = true;
				cont();
			}, null, !zdInitializationErrorShowed);
		} else {
			checkZDStream();
		}
	}, cont);
}, true);


function startCheckPasswordForTestPage()
{
	const pass = "апроль";
	let enteredSymbols = "";

	const onkeyupPrev = document.onkeyup;

	document.onkeyup = (e) =>
	{
		if (e.key.length > 1 || e.key === " ")
			return;

		enteredSymbols += e.key;

		if (enteredSymbols.length === pass.length) {
			if (enteredSymbols === pass) {
				AJAXLoadPage("debug_mode.html");
			}

			document.onkeyup = onkeyupPrev;
			return;
		}
	};

}


function loadStartPage()
{
	sendJsonRPCRequest("zd.update.get_progress", null, (j) =>
	{
		AJAXLoadPage(j.is_running ? "update.html" : global.isMKO ? "control_info.html" : "telem_basic.html");
		global.isPageChanged = false;
	});
}


const ipGetter = new AsyncLoopFunc(function()
{
	//global.cmdEP.substring(0, global.cmdEP.indexOf(":", 5));
	sendHttpGetRequest(`http://${location.host}:8999/__get_ep`, (response) =>
	{
		const j = JSON.parse(response);
		global.ipConfig = j[0];
		global.hostname = j[1];

		global.cmdEP = `http://${location.host}:${global.cmdPort}`;
		basicElems.labelBottomIP.textContent = `IP: ${global.ipConfig.ip}/${global.ipConfig.subnetMask} (${global.hostname})`;

		this.continueTimeout(1000);
	});
});


function preInit()
{
	Popup.initialize(() =>
	{
		basicElems.buttonBottomMenu.disabled 	= true;
		basicElems.buttonTopMenu.disabled 		= true;
	}, () =>
	{
		basicElems.buttonBottomMenu.disabled 	= false;
		basicElems.buttonTopMenu.disabled 		= false;
	});

	sendHttpGetRequest(`http://${location.hostname}:8999/__get_ep`, (response) =>
	{
		const j = JSON.parse(response);
		global.ipConfig = j[0];
		global.hostname = j[1];

		
		sendHttpGetRequest("/__get_cmd_ep", (response) =>
		{
			global.cmdPort = response.substring(response.indexOf(":") + 1);
			global.cmdEP = `http://${location.hostname}:${global.cmdPort}`;
			global.iznEP = `http://${location.hostname}:${9998}`;

			const onResult = (j) => {
				global.isMKO = j === "mko";
				
				const pages = new Map([
					[ 
						"can", [
							[ "telem_basic.html", "Телеметрия основная (ТО)", false ],
							[ "telem_astro.html", "Телеметрия астроориентации (ТА)", false ],
							[ "astro_data.html", "Данные астроориентации (ДА)", false ],
							[ "telem_special.html", "Телеметрия специальная (ТС)", false ],
							[ "special_data.html", "Специальные данные (ДС)", false ],
							[ "telem_config.html", "Телеметрия конфигурационная (ТК)", false ],
							[ "sopr_info.html", "Сопроводительная информация (ИС)", false ],
							[ "camera_settings.html", "Настройки камеры (КН)", false ],
							[ "zd_video.html", "Видео", false ],
							[ "izn.html", "Имитатор звёздного неба (ИЗН)", false ],
							[ "video_record.html", "Управление видеозаписью", false ],
							[ "flash_video.html", "Просмотр считанных изображений", false ],
							[ "test_info.html", "Тестовая информация", false ],
							[ "kpa_settings.html", "Настройки КПА", false ],
							[ "update.html", "Обновление", true ],
							[ "telem_pmo.html", "Телеметрия смены ПМО (ТП)", true ],
						]
					],
					
					[
						"mko", [
							[ "control_info.html", "Контрольная информация (КИ)", false ],
							[ "regular_info_mko.html", "Данные астроориентации (ДА)", false ],
							[ "priority_info.html", "Априорная информация (АПИ)", false ],
							[ "telem_special_mko.html", "Данные об одиночном объекте (ДО)", false ],
							[ "sopr_info_mko.html", "Синхронизация", false ],
							[ "zd_video_mko.html", "Видео", false ],
							[ "video_record_mko.html", "Управление видеозаписью", false ],
							[ "flash_video.html", "Просмотр считанных изображений", false ],
							[ "test_info_mko.html", "Тестовая информация", false ],
							[ "izn.html", "Имитатор звёздного неба (ИЗН)", false ],
							[ "kpa_settings_mko.html", "Настройки КПА", false ],
							[ "update.html", "Обновление", true ],
							[ "telem_pmo_mko.html", "Диагностическая информация смены ПМО (ДИП)", true ],
						]
					]
				]);

				if (!pages.has(j)) {
					Popup.showLocked("Ошибка", `Используется неизвестный коммандный интерфейс: ${j}`);
					return;
				}

				pages.get(j).forEach(([pageFileName, pageName, availableOnUpdate]) => {
					const aElem = document.createElement("a");

					aElem.name = pageFileName;
					aElem.innerText = pageName;

					if (availableOnUpdate) {
						aElem.classList.add("available-on-update");
					}

					aElem.onclick = (e) => {
						//if (!global.isTopMenuEnabled)
						//	return;

						AJAXLoadPage(e.currentTarget.name);
						basicElems.buttonTopMenu.onclick(null);
					};
					
					basicElems.topmenuPages.appendChild(aElem);
				});

				if (!checkZD.isRunning) {
					checkZD.run();
				}
	
				setTimeout(() => {
					ipGetter.run();
				}, 1000);
	
				initializeMenus();
				loadStartPage();
				startCheckPasswordForTestPage();
			};

			const send_ = () => {
				sendJsonRPCRequest("zd.cmd.get_interface_type", null, onResult, () => {
					setTimeout(send_, 1000);
				});
			};
			
			send_();
		});
	});

	
}


function initializeMenus()
{
	bottomMenuLoad();
}


basicElems.buttonTopMenu.onclick = (e) => 
{
	basicElems.checkboxTopMenuState.checked = !basicElems.checkboxTopMenuState.checked;
};


basicElems.buttonBottomMenu.onclick = function() 
{
	basicElems.checkboxBottomMenuState.checked = !basicElems.checkboxBottomMenuState.checked;
	basicElems.brsBottomMenu.hidden = !basicElems.checkboxBottomMenuState.checked;
};


basicElems.checkboxQuittance.oninput = (e) =>
{
	sendJsonRPCRequest(e.target.checked ? "zd.cmd.can.enable_quittance" : "zd.cmd.can.disable_quittance");
};


basicElems.buttonZDSwitchPower.onclick = (e) =>
{
	e.target.disabled = true;
	const en = () => { e.target.disabled = false; };

	if (global.isPowerEnabled) {
		Popup.showOKCancel("Внимание!", "Отключить питание прибора?", () => 
		{
			if (videoOnly) {
				sendJsonRPCRequest("zd.disable_power", null, en, en);
			} else {
				Popup.showOKCancel("Внимание!", "Подать команду подготовки к отключению питания?", () =>
				{
					const batch = new JsonRPCBatch();
					batch.pushRequest("pilv.prepare_for_power_off");
					batch.pushRequest("zd.disable_power");
					batch.flush(en);
				}, () => { sendJsonRPCRequest("zd.disable_power", null, en, en); });
			}
		}, en);
	} else {
		sendJsonRPCRequest("zd.enable_power", null, en, en);

		if (basicElems.checkboxSafeMode.checked) {
			basicElems.checkboxQuittance.disabled = true;

			sendJsonRPCRequest("zd.cmd.can.disable_quittance", null, (j) => {
				const isQuittanceEnabledPrev = basicElems.checkboxQuittance.checked;
	
				if (isQuittanceEnabledPrev) {
					basicElems.checkboxQuittance.checked = false;
				}
	
				const sendStartPMO = () => {
					sendJsonRPCRequest("zd.cmd.can.send_direct", { command: 0x0A, data: [] });
				};
	
				setTimeout(() => {
					const intervalHandle = setInterval(sendStartPMO, 50);
	
					setTimeout(() => {
						clearInterval(intervalHandle);

						if (isQuittanceEnabledPrev) {
							basicElems.checkboxQuittance.checked = true;
							basicElems.checkboxQuittance.disabled = false;
							sendJsonRPCRequest("zd.cmd.can.enable_quittance");
						}
					}, 5000);	
				}, 200);
			});
		}
	}
};


basicElems.buttonPrepareForPowerOff.onclick = (e) =>
{
	e.target.disabled = true;
	const en = () => { e.target.disabled = false; };
	sendJsonRPCRequest("pilv.prepare_for_power_off", null, en, en);
};


function bottomMenuLoad()
{
	const batch = new JsonRPCBatch();
	basicElems.radioCmdChannel0.disabled = videoOnly;
	basicElems.radioCmdChannel1.disabled = videoOnly;
	basicElems.checkboxQuittance.disabled = videoOnly;

	if (global.isMKO) {
		basicElems.trQuittance.hidden = true;

		batch.pushRequest("zd.get_mko_channel_no", null, (j) =>
		{
			switch (j) {
				case 0:
					basicElems.radioCmdChannel0.checked = true;
				break;
	
				case 1:
					basicElems.radioCmdChannel1.checked = true;
				break;
			}
		});
	} else {
		batch.pushRequest("zd.cmd.can.get_command_channel_no", null, (j) =>
		{
			switch (j) {
				case 0:
					basicElems.radioCmdChannel0.checked = true;
				break;
	
				case 1:
					basicElems.radioCmdChannel1.checked = true;
				break;
			}
		});
	
		batch.pushRequest("zd.cmd.can.is_quittance_enabled", null, (j) =>
		{
			basicElems.checkboxQuittance.checked = j;
		});
	}
	

	batch.flush(() => 
	{
		if (!updateBottomMenu.isRunning) {
			updateBottomMenu.run(); 
			beforeZDBoardAvailableCounter.run();
		}
	});
}


function disableBottomMenu()
{
	//if (updateBottomMenu.isRunning) {
		updateBottomMenu.pause();
	//}

	basicElems.radioCmdChannel0.disabled = true;
	basicElems.radioCmdChannel1.disabled = true;
	basicElems.buttonServerExit.disabled = true;
	basicElems.buttonZDSwitchPower.disabled = true;
	basicElems.checkboxSafeMode.disabled = true;
	basicElems.checkboxQuittance.disabled = true;
}


function enableBottomMenu()
{
	if (updateBottomMenu.isPause) {
		updateBottomMenu.continueRunning();
	}

	basicElems.radioCmdChannel0.disabled = false;
	basicElems.radioCmdChannel1.disabled = false;
	basicElems.buttonServerExit.disabled = false;
	basicElems.buttonZDSwitchPower.disabled = false;
	basicElems.checkboxSafeMode.disabled = global.isPowerEnabled;
	basicElems.checkboxQuittance.disabled = false;
}

function disableTopMenu()
{
	//global.isTopMenuEnabled = false;
	//basicElems.topmenuPages.
	
	for (let i = 0; i < basicElems.topmenuPages.children.length; i++) {
		const element = basicElems.topmenuPages.children[i];
		element.hidden = true;
	}

	for (let i = 0; i < elemsAvailableOnUpdate.length; i++) {
		const element = elemsAvailableOnUpdate[i];
		element.hidden = false;
	}
}


function enableTopMenu()
{
	//global.isTopMenuEnabled = true;
	for (let i = 0; i < basicElems.topmenuPages.children.length; i++) {
		const element = basicElems.topmenuPages.children[i];
		element.hidden = false;
	}
}


function buttonZDPowerToString(value)
{
	return value ? "Выключить питание прибора" : "Включить питание прибора";
}


const updateBottomMenu = new AsyncLoopFunc(function()
{
	const cont = () => 
	{ 
		this.continueTimeout(global.bottomMenuUpdateInterval); 
	};

	const clock = new Date();

	checkPing(() => 
	{
		basicElems.labelPing.textContent = (new Date() - clock).toString();
	});

	sendJsonRPCRequest("zd.is_initialized", null, (j) =>
	{
		if (!j) {
			cont();
			return;
		}

		beforeZDBoardAvailableCounter.stop();
		basicElems.labelUSBNotAvailableTimeCounter.hidden = true;

		const batch = new JsonRPCBatch();
		let volt = 0;
		let curr = 0;
		batch.pushRequest("zd.cmd.flush_tx_fifo", { id: clientIDInt }, (j) =>
		{
			if (j.length == 0)
				return;

			for (let i = 0; i < j.length; ++i) {
				zdCmdTableAddRow(j[i]);
			}

			basicElems.zdCmdTable.scrollIntoView(0);
		});

		batch.pushRequest("zd.get_temperature", null, (j) => 
		{
			basicElems.labelZDTemperature.textContent = j.temperature >= 32767 ? "---" : sprintf("%.2f", j.temperature);
			basicElems.labelZDTemperatureSensorID.textContent = j.sensor_id;
		});	

		batch.pushRequest("zd.get_volt", null, (j) => 
		{
			volt = j < 1 ? 0 : j;
			basicElems.labelZDVolt.textContent = sprintf("%.2f", volt);

			global.isPowerEnabled = volt > 7;

			basicElems.buttonPrepareForPowerOff.disabled = !global.isPowerEnabled || videoOnly;
			basicElems.buttonZDSwitchPower.style.backgroundColor = global.isPowerEnabled ? "darkseagreen" : "black"
			basicElems.buttonZDSwitchPower.style.color = global.isPowerEnabled ? "black" : "whitesmoke";
			basicElems.buttonZDSwitchPower.style.borderColor = global.isPowerEnabled ? "darkgreen" : "black";
			basicElems.labelZDPowerState.textContent = global.isPowerEnabled ? "Выключить" : "Включить";
			basicElems.checkboxSafeMode.disabled = global.isPowerEnabled;
		});

		batch.pushRequest("zd.get_curr", null, (j) => 
		{
			curr = j < 0.01 ? 0 : j;
			basicElems.labelZDCurr.textContent = sprintf("%.2f", curr);
			basicElems.labelZDWattage.textContent = sprintf("%.2f", volt * curr);
			
		});

		batch.pushRequest("zd.cmd.can.was_io_event", { id: clientIDInt }, (j) =>
		{
			const elemsByCanChannelNames = {
				"Основной": basicElems.tdCanChannel0,
				"Резервный": basicElems.tdCanChannel1
			};

			j.forEach((v) =>
			{
				const elem = elemsByCanChannelNames[v.channel_name];

				if (!elem) 
					return;

				elem.style.backgroundColor 	= v.state ? "darkseagreen" 	: "black"
				elem.style.color 			= v.state ? "black" 		: "whitesmoke";
				elem.style.borderColor 		= v.state ? "darkgreen" 	: "black";
			});
		});

		batch.flush(cont);
	}, cont);
}, true);

 
function zdCmdTableAddRow(txInfo)
{
	if (global.isMKO) {
		if (basicElems.zdCmdTable.rows.length > 256) {
			basicElems.zdCmdTable.deleteRow(0);
		}
	
		let row = basicElems.zdCmdTable.insertRow();
		let tdData = row.insertCell(0);

		tdData.textContent = txInfo;
	} else {
		let chNoAndID = txInfo.split(" ", 2);
		let data = txInfo.substring(chNoAndID[0].length + chNoAndID[1].length + 2);
	
		if (basicElems.zdCmdTable.rows.length > 256) {
			basicElems.zdCmdTable.deleteRow(0);
		}
	
		let row = basicElems.zdCmdTable.insertRow();
		let tdChannelNo = row.insertCell(0);
		let tdID = row.insertCell(1);
		let tdData = row.insertCell(2);
	
		tdChannelNo.textContent = chNoAndID[0];
		tdID.textContent = chNoAndID[1];
		tdData.textContent = data;
	}
}


function cmdChannelNoOnClick(elem) 
{
	let channelNo = null;

	if (elem === basicElems.radioCmdChannel0) {
		channelNo = 0;
	} else if (elem === basicElems.radioCmdChannel1) {
		channelNo = 1;
	}

	if (channelNo === null)
		return;

	if (global.isMKO) {
		switch (channelNo) {
			case 0: sendJsonRPCRequest("zd.set_mko_channel_basic"); break;
			case 1: sendJsonRPCRequest("zd.set_mko_channel_reserve"); break;
		}
	} else {
		sendJsonRPCRequest("zd.cmd.can.set_command_channel_no", [ channelNo ]);
	}
}


function onServerConnectionRestored()
{
	if (!checkZD.isRunning) {
		checkZD.run();
	}

	if (global.callbacks.onserverreload) {
		global.callbacks.onserverreload();
	}
}


function sendHttpGetRequest(uri, onResponse, onConnectError = null)
{
	sendHttpRequest(uri, "GET", null, onResponse, generateIntID().toString(), null, onConnectError);
}


function sendHttpPostRequest(uri, data, onResponse, additionalHeaders = null, onConnectError = null, onProgress = null)
{
	sendHttpRequest(uri, "POST", data, onResponse, generateIntID().toString(), additionalHeaders, onConnectError, onProgress);
}

var xhrConnectErrorPrinted = {};

function sendHttpRequest(uri, method, data, onResponse, requestID, additionalHeaders = null, onConnectError = null, onProgress = null)
{
	if (!global.areHttpRequestsEnabled) {
		setTimeout(() => 
		{ 
			sendHttpRequest(uri, method, data, onResponse, requestID, additionalHeaders, onConnectError, onProgress); 
		}, 1000);

		return;
	}

	let onXhrError = () =>
	{
		if (global.isConnectionErrorPopupsEnabled) {
			if (!xhrConnectErrorPrinted[requestID]) {
				Popup.showLocked("Ошибка сервера.", "Сервер выключен или не отвечает.");
				xhrConnectErrorPrinted[requestID] = true;
			}
		}

		global.wasConnectionError = true;

		if (onConnectError) {
			onConnectError();
		}

		setTimeout(() =>
		{ 
			sendHttpRequest(uri, method, data, onResponse, requestID, additionalHeaders, null, onProgress); 
		}, 1000);
	}

	let xhr = new XMLHttpRequest();

	xhr.timeout = 30000;
	xhr.open(method, uri, true);
	xhr.setRequestHeader("Cache-Control", "no-cache");
	xhr.setRequestHeader("Access-Control-Allow-Origin", "*");

	if (additionalHeaders) {
		for (let i = 0; i < additionalHeaders.length; ++i) {
			let header = additionalHeaders[i];
			xhr.setRequestHeader(header.name, header.value);
		}
	}
	
	xhr.onerror = onXhrError;
	xhr.ontimeout = onXhrError;
	xhr.upload.onprogress = onProgress;

	xhr.onload = () =>
	{
		if (global.wasConnectionError) {
			global.wasConnectionError = false;
			onServerConnectionRestored();
		}

		let response = xhr.response;

		if (xhrConnectErrorPrinted[requestID]) {
			delete xhrConnectErrorPrinted[requestID];
			Popup.close();
		}
		

		if (onResponse) {
			onResponse(response);
		}
	}

	xhr.send(data);
}


function generateIntID()
{
	return Number(Math.floor((Math.random() / 2.0) * 0xFFFFFFFF));
}


function generateStringID()
{
	return generateIntID().toString();
}


function MAKEWORD(lobyte, hibyte)
{
	return ((lobyte & 0xFF) | ((hibyte & 0xFF) << 8)) & 0xFFFF;
}


function MAKELONG(loword, hiword)
{

	return loword === null || hiword === null ? null : (loword & 0xFFFF) | ((hiword & 0xFFFF) << 16) & 0xFFFFFFFF;
}


function MAKELONG_b(b0, b1, b2, b3) {
	return MAKELONG(MAKEWORD(b0, b1), MAKEWORD(b2, b3));
}


function LOBYTE(word) {
	return word & 0xFF;
}

function HIBYTE(word) {
	return (word >> 8) & 0xFF;
}


function BYTENOLE(sourseValue, byteNo)
{
	return sourseValue >> (8 * (i >> (byteNo)));
}


var jsonPRCErrorCodeStrings = {
	"-32700": "Parse error",
	"-32600": "Invalid Request",
	"-32601": "Method not found",
	"-32602": "Invalid params",
	"-32603": "Internal error"
};


class JsonRPC {
	constructor(method, id, params = null) {
		this.jsonrpc = "2.0",
		this.id = id;
		this.method = method;

		if (params != null) {
			this.params = params;
		}
	}
};


class JsonRPCBatch {
	constructor(ep = global.cmdEP) {
		this.requestFIFO = [];
		this.FIFOcallbacks = {};
		this.ep = ep;
	}


	pushRequest(method, params = null, onResult = null, onError = null, showError = true)
	{
		const id = generateStringID();
		const rpc = new JsonRPC(method, id, params);
		this.requestFIFO.push(rpc);
		this.FIFOcallbacks[rpc.id] = { method: rpc.method, onResult: onResult, onError: onError, showError: showError };
	}


	flush(onComplete, onConnectError = null) 
	{
		if (this.requestFIFO.length == 0) {
			if (onComplete) {
				onComplete();
			}

			return;
		}

		sendHttpPostRequest(this.ep, JSON.stringify(this.requestFIFO), (response) =>
		{
			let j = null;

			try {
				j = JSON.parse(response);
			} catch (e) {
				Popup.showInformation("Ошибка!", sprintf("Неверный ответ от сервера. %s", e.toString()));
				return;
			}

			for (let i = 0; i < j.length; ++i) {
				const request = j[i];
				const requestInfo = this.FIFOcallbacks[request.id];
				processJsonRPCRequest(request, requestInfo.onResult, requestInfo.onError, requestInfo.showError);
				delete this.FIFOcallbacks[request.id];
			}

			this.requestFIFO.length = 0;

			if (onComplete) {
				onComplete();
			}
		}, [new HttpHeader("Content-Type", "application/json")], onConnectError);
	}
};


function stringIsHex(string)
{
	if (string.length < 2)
		return false;
	
	return string[0] == '0' && (string[1] == 'x' || string[1] == 'X');
}


function stringToInt(string)
{
	const isHex = stringIsHex(string);
	const valid = stringIsInt(string, isHex);
	return valid ? Number.parseInt(string, isHex ? 16 : 10) : null;
}


function textboxIntValueGuard(textBox, numberSizeBytes = null, isSigned = false, pred = null, maxValue = null, minValue = null)
{
	const text = textBox.value;
	let value = stringToInt(text);
	const isHex = stringIsHex(text);

	if (numberSizeBytes != null) {
		maxValue = (maxValue === null) ? ((isSigned && !isHex) ?  Math.pow(2, numberSizeBytes * 8 - 1) - 1 : Math.pow(2, numberSizeBytes * 8) - 1) : maxValue;
		minValue = (minValue === null) ? ((isSigned && !isHex) ? -Math.pow(2, numberSizeBytes * 8 - 1) : 0) : minValue;
	}

	if (checkNumberValue(value, false)) {
		if (value > maxValue || value < minValue) {
			value = null
		}
	}

	if (value != null && pred != null) {
		value = pred(value) ? value : null; 
	}

	textBox.style.background = value != null ? "white" : "lightcoral";
	return value;
}


function textboxFloatValueGuard(textBox, pred = null, maxValue = null, minValue = null, maxValueEx = null, minValueEx = null)
{
	const text = textBox.value;
	let value = Number.parseFloat(text);

	if (checkNumberValue(value, false)) {
		if (maxValue != null && value > maxValue 
			|| minValue != null && value < minValue 
			|| maxValueEx != null && value >= maxValueEx 
			|| minValueEx != null && value <= minValueEx) {
			value = null
		} 
	}

	const isOK = checkNumberValue(value, false);

	if (isOK && pred != null) {
		value = pred(value) ? value : null; 
	}

	textBox.style.background = isOK ? "white" : "lightcoral";
	return value;
}


function stringIsInt(string, isHex)
{
	let result = false;

	for (let i = isHex ? 2 : (string[0] == '-' ? 1 : 0); i < string.length; ++i) {
		const _char = string[i];
		let valid = false;

		valid = (_char >= '0' && _char <= '9');
		
		if (isHex) {
			valid |= (_char >= 'a' && _char <= 'f') ||
					 (_char >= 'A' && _char <= 'F');
		}

		if (!valid) {
			result = false;
			break;
		} else {
			result = true;
		}
	}

	return result;
}


function processJsonRPCRequest(j, onResult = null, onError = null, showErrorPopup = true)
{
	if (!j) {
		Popup.showInformation("Ошибка!", "Неверный ответ от сервера, ожидался JsonRPC 2.0");
		return;
	}

	let hasError = j.hasOwnProperty("error");

	if (hasError) {
		let err = j.error;

		if (showErrorPopup) {
			let errCodeString = err.code.toString();
			let predefinedErrorString = null;
			let message = null;

			if (jsonPRCErrorCodeStrings.hasOwnProperty(errCodeString)) {
				predefinedErrorString = jsonPRCErrorCodeStrings[errCodeString];
			}

			message = sprintf("%s, %s", predefinedErrorString ? predefinedErrorString : err.code.toString(), err.data);

			printErrorMessage("Ошибка запроса!", message);
//			Popup.showInformation("Ошибка запроса!", message);
		}

		if (onError) {
			onError(err);
		}
	}
	
	if (onResult && !hasError && j.hasOwnProperty("result")) {
		onResult(j.result);
	}
}


function sendJsonRPCRequest(method, params = null, onResult = null, onError = null, onConnectError = null, showErrorPopup = true) {
	var id = generateIntID().toString();
	var rpc = new JsonRPC(method, id, params);

	sendHttpPostRequest(global.cmdEP, JSON.stringify(rpc), (response) =>
	{
		processJsonRPCRequest(JSON.parse(response), onResult, onError, showErrorPopup);
	}, [new HttpHeader("Content-Type", "application/json")], onConnectError);
}

function sendJsonRPCRequestIzn(method, params = null, onResult = null, onError = null, onConnectError = null, showErrorPopup = true) {
	var id = generateIntID().toString();
	var rpc = new JsonRPC(method, id, params);

	sendHttpPostRequest(global.iznEP, JSON.stringify(rpc), (response) =>
	{
		processJsonRPCRequest(JSON.parse(response), onResult, onError, showErrorPopup);
	}, [new HttpHeader("Content-Type", "application/json")], onConnectError);
}

const defualtBottomMenuColor = basicElems.buttonBottomMenu.style.backgroundColor;

function printErrorMessage(header, message)
{
	const now = new Date();
	basicElems.thErrorHeader.textContent = header;
	basicElems.labelErrorMessageDate.textContent = dateToString(now);
	basicElems.tdErrorMessage.textContent = message;
	basicElems.buttonBottomMenu.style.backgroundColor = "darkred";
	setTimeout(() => { basicElems.buttonBottomMenu.style.backgroundColor = defualtBottomMenuColor; }, 100);
}


function clearErrorMessage()
{
	basicElems.thErrorHeader.textContent = "Ошибок нет";
	basicElems.tdErrorMessage.textContent = "";
	basicElems.labelErrorMessageDate.textContent = "";
}


class SearchParams {
	constructor(SearchON = null, MinVal = null, MaxVal = null,
		MinSize = null, MaxSize = null, MinAV = null, MaxAV = null,
		MinY = null, MaxY = null, MinZ = null, MaxZ = null)
	{
		this.SearchON = SearchON;
		this.MinVal = MinVal;
		this.MaxVal = MaxVal;
		this.MinSize = MinSize;
		this.MaxSize = MaxSize;
		this.MinAV = MinAV;
		this.MaxAV = MaxAV;
		this.MinY = MinY;
		this.MaxY = MaxY;
		this.MinZ = MinZ;
		this.MaxZ = MaxZ;
	}
}


class SoprInfo {
	constructor(
		QTime_INT = null,
		QTime_FR = null,
		Rx = null,
		Ry = null,
		Rz = null,
		Vx = null,
		Vy = null,
		Vz = null,
		Q0_API = null,
		Q1_API = null,
		Q2_API = null,
		Q3_API = null,
		IS_Q_STATUS = null,
		wx = null,
		wy = null,
		wz = null) {
		this.QTime_INT = QTime_INT;
		this.QTime_FR = QTime_FR;
		this.Rx = Rx;
		this.Ry = Ry;
		this.Rz = Rz;
		this.Vx = Vx;
		this.Vy = Vy;
		this.Vz = Vz;
		this.Q0_API = Q0_API;
		this.Q1_API = Q1_API;
		this.Q2_API = Q2_API;
		this.Q3_API = Q3_API;
		this.IS_Q_STATUS = IS_Q_STATUS;
		this.wx = wx;
		this.wy = wy;
		this.wz = wz;
	}
};


class CameraSettings {
	constructor(
		FlgFPU = null,
		Al = null,
		Ah = null,
		Fl = null,
		Fh = null, 
		Ml = null, 
		Mh = null,
		Gna_Cdsa = null,
		Gnb_Cdsb = null,
		Blk = null, 
		Cpb = null, 
		Cpe = null,
		on_TLK = null)
	{
		this.FlgFPU = FlgFPU;
		this.Al = Al;
		this.Ah = Ah;
		this.Fl = Fl;
		this.Fh = Fh;
		this.Ml = Ml;
		this.Mh = Mh;
		this.Gna_Cdsa = Gna_Cdsa;
		this.Gnb_Cdsb = Gnb_Cdsb;
		this.Blk = Blk;
		this.Cpb = Cpb;
		this.Cpe = Cpe;
		this.on_TLK = on_TLK;
	}
};


class FlashParams {
	constructor	(
		FL_MASK = null,
		FL_FRAMES = null,
		FL_START_PAGE = null,
		FL_NUM_PAGE = null,
		FL_FRAME_DEC = null,
		FL_FRAME_REC = null,
		Exp_frame_sel = null,
		compr_params_Addr = null,
		FL_LVDS_MPI = null) 
	{
		this.FL_MASK = FL_MASK;
		this.FL_FRAMES = FL_FRAMES;
		this.FL_START_PAGE = FL_START_PAGE;
		this.FL_NUM_PAGE = FL_NUM_PAGE;
		this.FL_FRAME_DEC = FL_FRAME_DEC;
		this.FL_FRAME_REC = FL_FRAME_REC;
		this.Exp_frame_sel = Exp_frame_sel;
		this.compr_params_Addr = compr_params_Addr;
		this.FL_LVDS_MPI = FL_LVDS_MPI;
	}
};


class Pilv {
	static can = {
		set0xFCPeriod1s: () =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;

			sendJsonRPCRequest("pilv.can.set_0xfc_period_1s");
		},


		set0xFCPeriod10s: () =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;

			sendJsonRPCRequest("pilv.can.set_0xfc_period_10s");
		},


		set0xFCPeriod1min: () =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;

			sendJsonRPCRequest("pilv.can.set_0xfc_period_1min");
		},


		set0xFCPeriod10min: () =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;

			sendJsonRPCRequest("pilv.can.set_0xfc_period_10min");
		},


		off0xFC: () =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;

			sendJsonRPCRequest("pilv.can.off_0xfc");
		},


		setArrayTypePeriod: (arrayType, period) => 
		{
			if (!global.areHttpRequestsEnabled) 
				return;
			
			sendJsonRPCRequest("pilv.can.set_array_type_period", { array_type: arrayType, period: Math.trunc(period * 1000) });
		}
	};


	static mko = {
		getTelemPeriod: (name, onResult = null, onError = null) => sendJsonRPCRequest("zd.cmd.mko.get_telem_period", [name], onResult, onError),
		setTelemPeriod: (name, period, onResult = null, onError = null) => sendJsonRPCRequest("zd.cmd.mko.set_telem_period", [name, Math.trunc(period * 1000)], onResult, onError),
		enableTelem: (name, onResult = null, onError = null) => sendJsonRPCRequest("zd.cmd.mko.enable_telem", [name], onResult, onError),
		disableTelem: (name, onResult = null, onError = null) => sendJsonRPCRequest("zd.cmd.mko.disable_telem", [name], onResult, onError),
		isTelemEnabled: (name, onResult = null, onError = null) => sendJsonRPCRequest("zd.cmd.mko.is_telem_enabled", [name], onResult, onError),
		getDeviceAddress: (onResult = null) => sendJsonRPCRequest("zd.cmd.mko.get_device_address", null, onResult),
		setDeviceAddress: (address, onResult = null, onError = null) => sendJsonRPCRequest("zd.cmd.mko.set_device_address", [address], onResult, onError),
	};

	static speedLine = {
		sendParams: (params, onResult = null, onError = null) => {
			sendJsonRPCRequest("pilv.speed_line.send_params", params, onResult, onError);
		}
	};


	static search = {
		sendParams: (searchParams) =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;

			sendJsonRPCRequest("pilv.search.send_params", searchParams);
		},


		modeOn: (SearchON, MinVal) =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;
				
			sendJsonRPCRequest("pilv.search.mode_on", [SearchON, MinVal]);
		},


		modeOff: () =>
		{
			if (!global.areHttpRequestsEnabled) 
				return;
				
			sendJsonRPCRequest("pilv.search.mode_off");
		}
	}

	static flashVideo = {
		startRecord: (onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.start_record", null, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		stopOperation: (onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.stop_operation", null, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		pauseRecord: (onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.pause_record", null, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		resumeRecord: (onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.resume_record", null, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		clearMemory: (onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.clear_memory", null, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		readMemory: (onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.read_memory", null, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		setMask: (FL_MASK, FL_FRAMES, onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.set_mask", 
				{ FL_MASK: FL_MASK, FL_FRAMES: FL_FRAMES }, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		setStartPage: (FL_START_PAGE, onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.set_start_page", 
				{ FL_START_PAGE: FL_START_PAGE}, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		setFrameDecRec: (FL_FRAME_DEC, FL_FRAME_REC, 
			onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.set_frame_dec_rec", 
				{ FL_FRAME_DEC: FL_FRAME_DEC, FL_FRAME_REC: FL_FRAME_REC }, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		setCompression: (Exp_frame_sel, compr_params_Addr, 
			onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.set_compression", 
				{ Exp_frame_sel: Exp_frame_sel, compr_params_Addr: compr_params_Addr }, 
				onResult, onError, onConnectError, showErrorPopup);
		},


		sendParams: (flashParams, onResult = null, onError = null, onConnectError = null, showErrorPopup = true) =>
		{
			sendJsonRPCRequest("pilv.flash_video.send_params", 
				flashParams, onResult, onError, onConnectError, showErrorPopup);
		}
	}

	static setBSHV(BST, onResult = null, onError = null, onConnectError = null, showErrorPopup = true) 
	{
		sendJsonRPCRequest("pilv.set_bshv", [ BST ], 
			onResult, onError, onConnectError, showErrorPopup);
	}

	static sendSorpInfo(params, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.send_sopr_info", params, 
			onResult, onError, onConnectError, showErrorPopup);
	}

	static setRE(FlgFPU, Al, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.set_re", { FlgFPU: FlgFPU, Al: Al }, 
			onResult, onError, onConnectError, showErrorPopup);
	}

	static sendCameraSettings(params, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.send_camera_settings", params, 
			onResult, onError, onConnectError, showErrorPopup);
	}

	
	static setFramePeriod(Fl, Fh, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.set_frame_period", { Fl: Fl, Fh: Fh }, 
			onResult, onError, onConnectError, showErrorPopup);
	}


	static setExposureTime(Al, Ah, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.set_exposure_time", { Al: Al, Ah: Ah }, 
			onResult, onError, onConnectError, showErrorPopup);
	}


	static sendShutter(shutter, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.send_shutter", shutter, 
			onResult, onError, onConnectError, showErrorPopup);
	}


	static sendTestData(testData, onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.send_test_data", testData, 
			onResult, onError, onConnectError, showErrorPopup);
	}


	static prepareForPowerOff(onResult = null, onError = null, onConnectError = null, showErrorPopup = true)
	{
		sendJsonRPCRequest("pilv.prepare_for_power_off", null, 
			onResult, onError, onConnectError, showErrorPopup);
	}
}


function boolToYNString(value)
{
	return value ? "Да" : "Нет";
}


function boolToOnOffString(value)
{
	return value ? "Вкл" : "Выкл";
}


function boolToHasNoString(value) 
{
	return value ? "Есть" : "Нет";
}


function checkNumberValue(value, showErrorPopup = false)
{
	if (value === null || value === undefined) {
		if (showErrorPopup) {
			Popup.showInformation("Внимание!", "Введено неверное значение.");
		}

		return false;
	} else if (isNaN(value)) {
		if (showErrorPopup) {
			Popup.showInformation("Внимание!", "Значение не введено.");
		}

		return false;
	}

	return true;
}


class Checkable {
	checkValue()
	{
		let result = true;

		for (let key in this) {
			const obj = this[key];

			if (obj && obj.checkValue !== undefined) {
				result = obj.checkValue();

				if (!result) {
					break;
				}
			} else if (!checkNumberValue(this[key])) {
				result = false;
				break;
			}
		}

		return result;
	}
};


class Vector2 extends Checkable {
	constructor(x = 0, y = 0) 
	{
		super();
		this.x = x;
		this.y = y;
	}
}


class Vector3 extends Checkable {
	constructor(x = 0, y = 0, z = 0)
	{
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}
}




class RDR extends Checkable {
	constructor(ra = 0, dec = 0, roll = 0)
	{
		super();
		this.ra = ra;
		this.dec = dec;
		this.roll = roll;
	}
}


class RD extends Checkable {
	constructor(ra = 0, dec = 0)
	{
		super();
		this.ra = ra;
		this.dec = dec;
	}
}


class Quaternion extends Checkable {
	constructor(w = 0, x = 0, y = 0, z = 0)
	{
		super();
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
}


class AdjObj extends Checkable {
	constructor(position = new RD(), mag = 0, vel = 0)
	{
		super();
		this.position = position;
		this.mag = mag;
		this.vel = vel;
	}
}


function arrayBufferToString(arrayBuffer)
{
	var bytes = new Uint8Array(arrayBuffer);
	var result = "";

	for (var i = 0; i < bytes.length; ++i) {
		result += String.fromCharCode(bytes[i]);
	}

	return result;
}

class HttpHeader {
	constructor(name, value) 
	{
		this.name = name;
		this.value = value;
	}
}


class FakeProgress {
	constructor() {
		this.value = 0.0;
		this._started = false;
		this.onProgress = null;
		this.onEnd = null;
	}

	start(onProgress, onEnd = null, interval = 1000 / 60)
	{
		this.value = 0.0;
		this._started = true;
		this.onProgress = onProgress;
		this.onEnd = onEnd;
		this.interval = interval;
		this._update();
	};

	_update()
	{
		if (this.value < 0.99) {
			this.value += (0.003 - (this.value * 0.0029)) * Math.cos(((Math.PI / 2) * this.value));
			this.onProgress(this.value);
		
			if (this._started) {
				setTimeout(this._update.bind(this), this.interval);
			}
		}
	}

	end()
	{
		this.value = 1.0;
		this._started = false;

		if (this.onEnd) {
			this.onEnd(this.value);
		} else {
			this.onProgress(this.value);
		}
	}
}


function checkPing(onResponse, onConnectError = null)
{
	sendHttpGetRequest("/__ping", onResponse, onConnectError);
}


basicElems.buttonServerExit.onclick = (e) =>
{
	Popup.showOKCancel("Внимание!", "Выключить сервер?", () =>
	{
		terminate = true;
		sendJsonRPCRequest("exit");
	});
}


function inputFileMaxSizeGuard(elem, maxFileSize)
{
	if (elem.files.length == 0)
		return null;

	if (elem.files[0].size > maxFileSize) {
		Popup.showInformation("Ошибка!", sprintf("Выбран слишком большой файл. Максимальный размер: %.2f МБ", maxFileSize / (1024 * 1024)));
		elem.value = "";
		return false;
	}

	return true;
}


function disableBottomMenuZDRequests()
{
	basicElems.radioCmdChannel0.disabled = true;
	basicElems.radioCmdChannel1.disabled = true;
	basicElems.buttonZDSwitchPower.disabled = true;
}


function objectToKeyArray(struct)
{
	let result = [];
		
	for (let key in struct) {
		result.push(key);
	}

	return result;
}


function getElemsWithIDRecursive(rootNode, result)
{
	for (let i = 0; i < rootNode.childElementCount; ++i) {
		let elem = rootNode.children[i];

		if (elem.id.length != 0) {
			result[elem.id] = elem;
		}

		if (elem.childElementCount != 0) {
			getElemsWithIDRecursive(elem, result);
		}
	}
}


function getElemsWithID(rootNode = null)
{
	let result = {};

	if (rootNode === null) {
		rootNode = document.body;
	}

	getElemsWithIDRecursive(rootNode, result);

	return result;
}


function getAJAXElemsWithID()
{
	return getElemsWithID(basicElems.AJAXContent);
}


function dateToString(date)
{
	return sprintf("%02d:%02d:%02d.%03d    %02d/%02d/%d", 
		date.getUTCHours(), 
		date.getUTCMinutes(),
		date.getUTCSeconds(), 
		date.getUTCMilliseconds(), 
		date.getUTCDate(),
		date.getUTCMonth() + 1,
		date.getUTCFullYear()
	);
}


function getWorldTimeStringFromPreambule(preambule, clockPageOpened)
{
	if (!preambule)
		return null;

	return getWorldTimeString(preambule.WorldTime_int, preambule.WorldTime_float, clockPageOpened);
}


function getWorldTimeString(timeInt, timeFloat, clockPageOpened)
{
	if (timeInt === null || timeFloat === null || timeInt === undefined || timeFloat === undefined)
		return null;
		
	const zdClock = ((timeInt + (timeFloat / Math.pow(2, 24))) * 1000); 
		//- clockPageOpened; 

	const date = new Date();
	date.setTime(zdClock);
	return dateToString(date);
}


function elemEnableIf(elem, pred)
{
	elem.disabled = pred(elem);
}


function elemEnableIfTrue(elem, value) 
{
	elem.disabled = !value;
}


function createElemsArray(htmlTag, count) 
{
	let result = [];

	for (let i = 0; i < count; ++i) {
		result.push(document.createElement(htmlTag));
	}

	return result;
}


function textboxSetArrayTypePeriodValueGuard(elem)
{
	return textboxFloatValueGuard(elem, null, null, 0);
}


function setIRQ_MASK_REQto0And(func, onError = null)
{
	const batch = new JsonRPCBatch();
	let IRQ_MASK_REQPrev = 0;

	batch.pushRequest("zd.cmd.get_IRQ_MASK_REQ_default_value", null, (j) =>
	{
		IRQ_MASK_REQPrev = j;
	});

	batch.pushRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: 0x44 }, (j) =>
	{
		func((onError) => 
		{ 
			sendJsonRPCRequest("zd.cmd.send_write_request", { name: "IRQ_MASK_REQ", value: IRQ_MASK_REQPrev }, null, onError); 
		});
	}, onError);

	batch.flush();
}


function DeviceStatus_2_toString(DeviceStatus) {
	const value = (DeviceStatus >> 1) & 15;
	let result = null;
	
	switch (value) {
		case 1:
			result = "Ожидание";
			break;
		case 2:
			result = "Очистка";
			break;
		case 4:
			result = "Запись";
			break;
		case 6:
			result = "Чтение";
			break;
		case 10:
			result = "Пауза очистки";
			break;
		case 12:
			result = "Пауза записи";
			break;
		case 14:
			result = "Пауза чтения";
			break;
		default:
			result = sprintf("НЕВЕРНЫЙ СТАТУС: %d", value);
			break;
	}

	return result;
}