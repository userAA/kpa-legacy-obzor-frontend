function main()
{
	const elems = getAJAXElemsWithID();
	const filterParams = {
		brightness: 		0,
		contrast: 			0,
		sliderBrightness: 	255,
		sliderContrast: 	255,
		sliderGamma: 	50
	};
	let frameNumber = elems.sliderFrameNumber.valueAsNumber;
	let totalFramesCount = 0;

	let dataObj;
	let img;// = new PngImage();
	let buffer;
	const canvas = elems.canvasVideo;
	let imageData;
	let ctx;
	const colorTable = new Array(4096);

	function updateColorTable()
	{
		const GPow = (x, g) => g > 0 ? Math.pow(x, g) : Math.pow(1 / x, -g);
		
		const x1 = (255 - filterParams.sliderBrightness) * 16;
		const x2 = 256 * 128.0 / filterParams.sliderContrast + x1;
		const gamma = GPow(1.1, 1.0 * filterParams.sliderGamma - 50);
		const K = 255 / GPow(x2 - x1, gamma);

		for (let i = 0; i < colorTable.length; ++i)
		{
			if (i < x1)
				colorTable[i] = 0;
			else if (i > x2)
				colorTable[i] = 255;
			else
			{
				const Data = Math.trunc(K * GPow(i - x1, gamma));
				colorTable[i] = (Data < 256) ? Data : 255;
			}
		}
	}

	function applyFilters()
	{
		if (!imageData)
			return;

  		var data = imageData.data;

		for (let i = 0; i < buffer.length; i++) {
			const color = colorTable[Math.min(buffer[i], colorTable.length)];
			data[(i * 4) + 0] = color;
			data[(i * 4) + 1] = color;
			data[(i * 4) + 2] = color;
			data[(i * 4) + 3] = 255;
		}

		ctx.putImageData(imageData, 0, 0);
	}


	function updateVideoSrc()
	{
		img = new PngImage();

		img.onload = function() {

			var pngtoy = this.pngtoy;
		
			dataObj = pngtoy.decode().then(function(results) {
		
				buffer = new Uint16Array(results.bitmap);
		
				for(var i = 0, j; i < buffer.length; i++) {
		
				  j = buffer[i];
				  buffer[i] = ((j & 0xff) << 8) | ((j & 0xff00) >>> 8); // needed to swap bytes for correct unsigned integer values  
				}

				console.log("dataObj", dataObj);
				console.log("results", results);

				canvas.width = results.width;
				canvas.height = results.height;

				ctx = canvas.getContext('2d');
				imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
				applyFilters();
				//canvasImage = new Image(results.width, results.height);
				//console.log(buffer);
			});     
		};


		img.onerror = function(e) {
			console.log(e.message);
		};
		
		img.src = sprintf("%s/lvds.png?%d", global.cmdEP, frameNumber);
	}
	
	
	elems.textboxFrameNumber.value = frameNumber.toString();
	elems.textboxFrameNumber.oninput = (e) => 
	{
		const value = textboxIntValueGuard(e.target, 8, false, (v) =>
		{
			return v <= totalFramesCount;
		});

		if (value != null) {
			frameNumber = value;
			elems.sliderFrameNumber.valueAsNumber = value;
			updateVideoSrc();
		}
	};


	function createBCGString()
	{
		return sprintf("brightness(%.2f) contrast(%.2f)", 
			filterParams.brightness, 
			filterParams.contrast
		);
	}


	function updateVideoFilters()
	{
		const filterString = createBCGString();
		elems.videoEffectContainer.style.filter = filterString;
	}


	elems.sliderFrameNumber.oninput = (e) => 
	{
		frameNumber = e.target.valueAsNumber;

		elems.textboxFrameNumber.value = 
			sprintf(stringIsHex(elems.textboxFrameNumber.value) ? "0x%X" : "%d", frameNumber);

		updateVideoSrc();
	};


	elems.sliderBrightness.oninput = (e) => 
	{
		const val = e.target.valueAsNumber;
		filterParams.sliderBrightness = val;
		//filterParams.brightness = val < 127 ? val / 127.0 : Math.pow(2, val / 80) - 2;
		elems.tdBrightness.textContent = sprintf("%03d", val);
		updateColorTable();
		applyFilters();
	};

	elems.sliderContrast.oninput = (e) =>
	{
		const val = e.target.valueAsNumber;
		filterParams.sliderContrast = val;
		//filterParams.contrast = val < 127 ? val / 127.0 : Math.pow(2, val / 80) - 2;
		elems.tdContrast.textContent = sprintf("%03d", val);
		updateColorTable();
		applyFilters();
	};

	elems.sliderGamma.oninput = (e) =>
	{
		const val = e.target.valueAsNumber;
		filterParams.sliderGamma = val;
		//filterParams.gamma = val < 127 ? val / 127.0 : Math.pow(2, val / 80) - 2;
		elems.tdGamma.textContent = sprintf("%03d", val);
		updateColorTable();
		applyFilters();
	};

	
	function getParams()
	{
		const batch = new JsonRPCBatch();

		batch.pushRequest("lvds.is_tiff_available", null, (j) =>
		{
			elems.buttonDownloadTiff.disabled = !j;
		});

		batch.pushRequest("lvds.is_raw_available", null, (j) => 
		{
			elems.buttonDownloadRaw.disabled = !j;
		});

		batch.pushRequest("lvds.is_telem_text_available", null, (j) => 
		{
			elems.buttonDownloadTelemText.disabled = !j;
		});

		batch.pushRequest("lvds.get_jpgs_count", null, (j) => 
		{
			const isZero = j === 0;
			const maxNumber = j - 1;
			const stringValue = maxNumber.toString();
			elems.labelFramesTotal.textContent = stringValue;
			totalFramesCount = maxNumber;
			
			elems.textboxFrameNumber.disabled = isZero;
			elems.sliderFrameNumber.disabled = isZero;
			elems.sliderFrameNumber.max = stringValue;
			//elems.video.hidden = isZero;
			elems.videoError.hidden = !isZero;
			elems.trFrameNumber.hidden = isZero;
			elems.sliderBrightness.disabled = isZero;
			elems.sliderContrast.disabled = isZero;
			elems.sliderGamma.disabled = isZero;

			if (!isZero) {
				updateVideoSrc();
			}
		});


		batch.flush(null, getParams);
	}

	elems.formDownloadTiff.action = sprintf("%s/lvds.tiff", global.cmdEP);
	elems.formDownloadRaw.action = sprintf("%s/lvds.bin", global.cmdEP);
	elems.formDownloadTelemText.action = sprintf("%s/lvds_telem.txt", global.cmdEP);

	const checkLvdsDecodingProgress = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("lvds.decoder.get_progress", null, (j) =>
		{
			const hasError = j.hasOwnProperty("error");

			if (!j.is_running && !hasError) {
				elems.trLvdsProgress.hidden = true;
				elems.trControls.hidden = false;
				getParams();
				return;
			} else {
				elems.trLvdsProgress.hidden = false;
				elems.trControls.hidden = true;
			}

			if (hasError) {
				elems.tdLvdsDecodingState.textContent = sprintf("Ошибка при декодировании flash видео: %s", j.error);
			} else {
				elems.tdLvdsDecodingState.textContent = "Запись flash-видео декодируется...";
			}

			elems.progressLvdsDecoding.value = j.completed * elems.progressLvdsDecoding.max;
			elems.tdLvdsChunksCounter.textContent = j.counter.chunk;
			elems.tdLvdsPkgVideoCounter.textContent = j.counter.video;
			elems.tdLvdsTelemCounter.textContent = j.counter.telem;
			elems.tdLvdsPkgCounter.textContent = j.counter.total_packages;
			elems.tdLvdsFramesCounter.textContent = j.counter.frames;
			this.continueTimeout(global.telemUpdateInterval);
		});
	});


	elems.buttonClearCache.onclick = (e) => { sendJsonRPCRequest("clear_cache"); };
	
	elems.sliderBrightness.valueAsNumber = filterParams.sliderBrightness;
	elems.sliderBrightness.oninput({ target: elems.sliderBrightness });

	elems.sliderContrast.valueAsNumber = filterParams.sliderContrast;
	elems.sliderContrast.oninput({ target: elems.sliderContrast });

	elems.sliderGamma.valueAsNumber = filterParams.sliderGamma;
	elems.sliderGamma.oninput({ target: elems.sliderGamma });

	checkLvdsDecodingProgress.run();
}