function main()
{
	let elems = getAJAXElemsWithID();
	let control = null;

	const namesMap = {
		"Al": "Al (exp_time)",
		"Ah": "Ah (cam_reserve_0)",
		"Fl": "Fl (frame_period)",
		"Fh": "Fh (frame_period_H)",
		"Ml": "Ml (cnt_bot)",
		"Gna_Cdsa": "Gna_Cdsa (train_pattern)",
		"Gnb_Cdsb": "Gnb_Cdsb (pga_gain_top)",
		"Blk": "Blk (cam_reserve_1)",
		"Cpb": "Cpb (cam_reserve_2)",
		"Cpe": "Cpe (cam_reserve_3)"
	};

	const fieldGroups = {
		"ВЕРСИИ": [
			"DNUMBER", 
			"FACTNUMBER", 
			"FWVER0", 
			"FWVER1"
		],

		"СЧЕТЧИКИ ВКЛЮЧЕНИЙ/ВЫКЛЮЧЕНИЙ": [
			"PowerUpCnt", 
			"ErPowerUpCnt"
		],

		"НАСТРОЙКИ ПОИСКА ОБЪЕКТА": [
			"SearchON",
			"MinVal",
			"MaxVal",
			"MinSize",
			"MaxSize",
			"MinAV",
			"MaxAV",
			"MinY",
			"MaxY",
			"MinZ",
			"MaxZ",
			//"Object_TRH"
		],

		"УПРАВЛЕНИЕ ВЫКАЛЫВАТЕЛЕМ": [
			"SHPR_OUT_SETTINGS_ADDR",
			"SHPR_MAP_SETTINGS_ADDR",
			"SHPR_MAP_STATUS_ADDR",
			"SHPR_VAL_MULT_ADDR",
			"SHPR_BG_TRH_ADDR",
			"SHPR_MAP_CNT_ADDR_L",
			"SHPR_MAP_CNT_ADDR_H",
			"SHPR_STREAM_CNT_ADDR_L",
			"SHPR_STREAM_CNT_ADDR_H",
			"SHPR_SQ0_YS_ADDR",
			"SHPR_SQ0_YE_ADDR",
			"SHPR_SQ0_XS_ADDR",
			"SHPR_SQ0_XE_ADDR",
			"SHPR_SQ1_YS_ADDR",
			"SHPR_SQ1_YE_ADDR",
			"SHPR_SQ1_XS_ADDR",
			"SHPR_SQ1_XE_ADDR",
			"SHPR_SQ2_YS_ADDR",
			"SHPR_SQ2_YE_ADDR",
			"SHPR_SQ2_XS_ADDR",
			"SHPR_SQ2_XE_ADDR",
			"SHPR_SQ3_YS_ADDR",
			"SHPR_SQ3_YE_ADDR",
			"SHPR_SQ3_XS_ADDR",
			"SHPR_SQ3_XE_ADDR",
			"SHPR_SQ4_YS_ADDR",
			"SHPR_SQ4_YE_ADDR",
			"SHPR_SQ4_XS_ADDR",
			"SHPR_SQ4_XE_ADDR",
			"SCC_MODE_ADDR",
			"SCC_THR_ADDR",
			"SCC_CNT_ADDR",
			"SCC_NUM_ADDR",
			"SCC_SENSTIMER_ADDR",
			"SCC_FRSKIP_ADDR"
		],

		"НАСТРОЙКИ": [
			"StarDetectON",
			"FocalLength",
			"PixelStepH",
			"PixelStepV",
			"PixelError",
			"Indent",
			"FoundPorog",
			"FoundPorogFinal",
			"Width",
			"Height",
			"MaxStarsSpeed",
			"flash_CRC_copy",
			"flash_CRC_actual",
			"Stars_OFF_ADDR",
			"clear_frame_num",
			"moon_level",
			"moon_limit",
			"Default_settings",
			"reprogram_flag",
			"sectors_copy",
			"Stars_Shift",
			"IRQ_MASK_REQ",
			"hcounter_cell_Addr",
			"vcounter_cell_Addr",
			"bin_cell_Addr",
			"state_cell_Addr",
			"error_counter_Addr",
			"timeout_counter_Addr",
			"StarsOutputType",
			"PowerUpTimer"
		],

		"ДИСТОРСИЯ": [
			"XDistX0Y0",
			"XDistX1Y0",
			"XDistX0Y1",
			"XDistX2Y0",
			"XDistX1Y1",
			"XDistX0Y2",
			"XDistX3Y0",
			"XDistX2Y1",
			"XDistX1Y2",
			"XDistX0Y3",
			"YDistX0Y0",
			"YDistX1Y0",
			"YDistX0Y1",
			"YDistX2Y0",
			"YDistX1Y1",
			"YDistX0Y2",
			"YDistX3Y0",
			"YDistX2Y1",
			"YDistX1Y2",
			"YDistX0Y3"
		],

		//"ФЛЕШЬ": [
		//	"FL_KEY_0",
		//	"FL_KEY_1",
		//	"FL_KEY_2",
		//	"FL_KEY_3"
		//],

		"БИТЫЕ ПИКСЕЛИ": [
			"NumOfHP",
			"HPx_0",
			"HPy_0",
			"HPx_1",
			"HPy_1",
			"HPx_2",
			"HPy_2",
			"HPx_3",
			"HPy_3",
			"HPx_4",
			"HPy_4",
			"HPx_5",
			"HPy_5",
			"HPx_6",
			"HPy_6",
			"HPx_7",
			"HPy_7",
			"HPx_8",
			"HPy_8",
			"HPx_9",
			"HPy_9",
			"HPx_10",
			"HPy_10"
		],

		"БИТЫЕ ПОЛОСКИ": [
			"NumOfHL",
			"HLx_0",
			"HLx_1",
			"HLx_2",
			"HLx_3",
			"HLx_4",
			"HLx_5",
			"HLx_6",
			"HLx_7",
			"HLx_8",
			"HLx_9",
			"HLx_10",
			"HLx_11",
			"HLx_12",
			"HLx_13",
			"HLx_14",
			"HLx_15"
		],

		"СТАТУС ПРОШИВКИ": [
			"STATUS",
			"PartCheckFlags_0",
			"PartCheckFlags_1",
			"PartCheckFlags_2",
			"PartCheckFlags_3",
			"PartCheckFlags_4",
			"PartCheckFlags_5",
			"PartCheckFlags_6",
			"PartCheckFlags_7"
		],

		"КАМЕРА": [
			//"FlgFPU",
			"Al",
			"Ah",
			"Fl",
			"Fh",
			"Ml",
			//"Mh",
			"Gna_Cdsa",
			"Gnb_Cdsb",
			"Blk",
			"Cpb",
			"Cpe",
			"on_TLK",
			"X_centre_addr",
			"Y_centre_addr",
			"R_circle_addr",
			"cam_shutter_0",
			"cam_shutter_1",
			"cam_shutter_2",
			"cam_shutter_3",
			"cam_shutter_4",
			"cam_shutter_5",
			"cam_shutter_6",
			"cam_shutter_7",
			"cam_shutter_8",
			"cam_shutter_9",
			"cam_shutter_10",
			"cam_shutter_11",
			"cam_shutter_12",
			"cam_shutter_13",
			"cam_shutter_14",
			"cam_shutter_15"
		],

		"Изменение всех cam_shutter":  (tbodyRoot) =>
		{
			const mainTable = document.createElement("table");
			const mainTbody = document.createElement("tbody");
			const mainTr = document.createElement("tr");
			const mainTds = createElemsArray("td", 3);

			const trRoot = document.createElement("tr");
			const tdRoot = document.createElement("td");
			trRoot.appendChild(tdRoot);
			tdRoot.appendChild(mainTable);
			tbodyRoot.appendChild(trRoot);
			
			mainTable.className = "table-nobordered table-kv-pair";
			mainTds.forEach((v, i) => 
			{
				mainTr.appendChild(v); 
			});
			
			mainTbody.appendChild(mainTr); 
			mainTable.appendChild(mainTbody);

			mainTds[0].textContent = "cam_shutter_all";
			const buttonSend = document.createElement("button");
			mainTds[2].appendChild(buttonSend);
			const textbox = document.createElement("input");
			textbox.type = "text";
			mainTds[1].appendChild(textbox);

			let cam_shutter = textboxIntValueGuard(textbox, 1, false, null, 63, 0);
			buttonSend.disabled = !checkNumberValue(cam_shutter);
			buttonSend.style.width = "15ch";
			buttonSend.textContent = "Отправить";
			
			textbox.oninput = (e) =>
			{
				cam_shutter = textboxIntValueGuard(e.target, 2);
				buttonSend.disabled = !checkNumberValue(cam_shutter);
			};

			buttonSend.onclick = (e) =>
			{
				let wasError = false;
				const en = () => { e.target.disabled = false; }
				//const setErrorTrue = () => {wasError = true; };  
	
				e.target.disabled = true;
				const batch  = new JsonRPCBatch();

				for (let i = 0; i < 16; i++) {
					batch.pushRequest("zd.cmd.send_write_request", { name: `cam_shutter_${i}`, value: cam_shutter });
				}

				batch.flush(en);
			};
		},

		"НАСТРОЙКИ ЭКСПОЗИЦИИ": (tbodyRoot) =>
		{
			const trFormula = document.createElement("tr");
			const tdFromula = document.createElement("td");
			trFormula.appendChild(tdFromula);

			const mainTable = document.createElement("table");
			const mainTbody = document.createElement("tbody");
			const mainTr = document.createElement("tr");
			const mainTds = createElemsArray("td", 5);

			const trRoot = document.createElement("tr");
			const tdRoot = document.createElement("td");
			trRoot.appendChild(tdRoot);
			tdRoot.appendChild(mainTable);
			tbodyRoot.appendChild(trFormula);
			tbodyRoot.appendChild(trRoot);
			
			mainTable.className = "table-nobordered";
			mainTds.forEach((v) => { mainTr.appendChild(v); });
			//mainTr.appendChild(mainTd); 
			mainTbody.appendChild(mainTr); 
			
			mainTable.appendChild(mainTbody);

			mainTds[0].textContent = "Al";
			mainTds[2].textContent = "Ah";

			const textboxAl = document.createElement("input");
			textboxAl.type = "text";
			const textboxAh = document.createElement("input");
			textboxAh.type = "text";
			mainTds[1].appendChild(textboxAl);
			mainTds[3].appendChild(textboxAh);

			const buttonSend = document.createElement("button");
			buttonSend.style.width = "15ch";
			buttonSend.textContent = "Отправить";
			mainTds[4].appendChild(buttonSend);

			const batch = new JsonRPCBatch();
			const textboxes = [textboxAl, textboxAh];
			const names = ["Al", "Ah"];
			
			buttonSend.onclick = (e) =>
			{
				e.target.disabled = true;
				const en = () => { e.target.disabled = false; };

				Pilv.setExposureTime(fieldValues["КАМЕРА"][names[0]], fieldValues["КАМЕРА"][names[1]], (j) =>
				{
					if (j !== null) {
						tds[1].textContent = j.formatted;
					}
					
					en();
				}, en);
			};

			batch.pushRequest("zd.cmd.get_fields_value", { names: names }, (j) =>
			{
				j.forEach((v, i) => 
				{
					if (v === null || v.raw === null)
						return;

					textboxes[i].value = v.formatted;
					fieldValues["КАМЕРА"][names[i]] = j.raw;
				});

				
			}, null, false);

			names.forEach((v, i) => 
			{
				batch.pushRequest("zd.cmd.get_field_info", { name: v }, (j) => 
				{
					const textbox = textboxes[i];
					const textboxCheckValue = (elem) => 
					{
						fieldValues["КАМЕРА"][v] = textboxIntValueGuard(elem, j.raw_size * 2, j.is_signed);
						const Al = fieldValues["КАМЕРА"]["Al"];
						const Ah = fieldValues["КАМЕРА"]["Ah"];
						tdFromula.textContent = sprintf("%s мс", !checkNumberValue(Al) || !checkNumberValue(Ah) ? "[неверное значение]" : (((560 * Al + 402) * 60) / 1000000).toString());
						buttonSend.disabled = false;
						names.forEach((name) => { buttonSend.disabled |= !checkNumberValue(fieldValues["КАМЕРА"][name]); });
					};

					textboxCheckValue(textbox);
					textbox.oninput = (e) => { textboxCheckValue(e.target); };
				}, null, false);
			});
			
			
			batch.flush();
		},

		"НАСТРОЙКА УСИЛЕНИЯ": (tbodyRoot) =>
		{
			const mainTable = document.createElement("table");
			const mainTbody = document.createElement("tbody");
			const mainTr = document.createElement("tr");
			const mainTds = createElemsArray("td", 3);

			const trRoot = document.createElement("tr");
			const tdRoot = document.createElement("td");
			trRoot.appendChild(tdRoot);
			tdRoot.appendChild(mainTable);
			tbodyRoot.appendChild(trRoot);
			
			mainTable.className = "table-nobordered table-kv-pair";
			mainTds.forEach((v, i) => 
			{
				mainTr.appendChild(v); 
			});
			
			mainTbody.appendChild(mainTr); 
			mainTable.appendChild(mainTbody);

			mainTds[0].textContent = "pga_gain";
			const buttonSend = document.createElement("button");
			mainTds[2].appendChild(buttonSend);
			const textbox = document.createElement("input");
			textbox.type = "text";
			mainTds[1].appendChild(textbox);

			let pga_gain = textboxIntValueGuard(textbox, 1, false, null, 63, 0);
			buttonSend.disabled = !checkNumberValue(pga_gain);
			buttonSend.style.width = "15ch";
			buttonSend.textContent = "Отправить";
			
			textbox.oninput = (e) =>
			{
				pga_gain = textboxIntValueGuard(e.target, 1, false, null, 63, 0);
				buttonSend.disabled = !checkNumberValue(pga_gain);
			};

			buttonSend.onclick = (e) =>
			{
				let wasError = false;
				const en = () => { e.target.disabled = false; }
				const setErrorTrue = () => {wasError = true; };  
	
				e.target.disabled = true;
				const batch  = new JsonRPCBatch();
				const cs = new CameraSettings();
	
				batch.pushRequest("zd.cmd.send_read_request", { names: ["Al", "Ah"] }, (j) =>
				{
					for (const key in cs) {
						cs[key] = 0;
					}

					for (let index = 0; index < j.length; index++) {
						const element = j[index];

						if (element === null) {
							setErrorTrue();
							return;
						}
					}

					cs.FlgFPU = 4;
					cs.Al = j[0].raw;
					cs.Ah = j[1].raw;
					cs.Fl = cs.Al + 1;
					cs.Fh = 0;
					cs.Ml = 0xF3A0;
					cs.Mh = 0x88 | ((pga_gain << 9) & 63);
					cs.Gna_Cdsa = 0x98E;
					cs.on_TLK = 1;
				}, setErrorTrue);

				batch.flush(() =>
				{
					if (wasError) {
						en();
						return;
					}

					Pilv.sendCameraSettings(cameraSettings, en, en);
				});
			};
		},

		"ИЗН": (tbodyRoot) => 
		{
			const mainTable = document.createElement("table");
			const mainTbody = document.createElement("tbody");
			const mainTrs = createElemsArray("tr", 2);
			const mainTds = createElemsArray("td", 2);

			const trRoot = document.createElement("tr");
			const tdRoot = document.createElement("td");
			trRoot.appendChild(tdRoot);
			tdRoot.appendChild(mainTable);
			tbodyRoot.appendChild(trRoot);
			
			mainTable.className = "table-nobordered";
			mainTrs.forEach((v, i) => 
			{
				v.appendChild(mainTds[i]); 
				mainTbody.appendChild(v); 
			});
			
			mainTable.appendChild(mainTbody);

			const table = document.createElement("table");
			const tbody = document.createElement("tbody");
			const tr = document.createElement("tr");
			const tds = createElemsArray("td", 2);

			table.className = "table-nobordered table-kv-pair";
			tds.forEach((v) => { tr.appendChild(v); });
			tbody.appendChild(tr);
			table.appendChild(tbody);
			tds[0].textContent = "Яркость";
			
			const range = document.createElement("input");
			range.type = "range";
			range.max = 100;
			range.min = -100;
			range.value = 0;

			const checkIzn = new AsyncLoopFunc(function()
			{
				sendJsonRPCRequest("izn.is_available", null, (j) =>
				{
					range.disabled = !j;
					this.continueTimeout(1000);
				});
			});

			range.oninput = (e) =>
			{
				const real = (e.target.valueAsNumber - e.target.min) / (e.target.max - e.target.min);
				tds[1].textContent = e.target.value;
				sendJsonRPCRequest("izn.set_brightness", [real]);
			};

			tds[1].textContent = range.value;
			mainTds[0].appendChild(table);
			mainTds[1].appendChild(range);
			checkIzn.run();
		}
	};


	let fieldValues = {};
	let fieldValueTds = {};
	let fieldsToSendRead = {};


	for (let key in fieldGroups) {
		const group = fieldGroups[key];
		const table = document.createElement("table");
		table.className = "zebra-table table-nobordered table-kv-pair table-kv-pair-with-textbox head-table";
		const tbody = document.createElement("tbody");
		table.appendChild(tbody);
		
		const details = document.createElement("details");
		const summary = document.createElement("summary");
		summary.textContent = key;
		details.appendChild(summary);
		const trRoot = document.createElement("tr");
		const tdRoot = document.createElement("td");
		
		tdRoot.appendChild(details);
		trRoot.appendChild(tdRoot);
		elems.tbodyMain.appendChild(trRoot);

		if (typeof(group) === "function") {
			details.appendChild(table);
			group(tbody);
			continue;
		} 

		fieldValues[key] = {};
		fieldValueTds[key] = {};
		fieldsToSendRead[key] = [];

		const buttonSendReadAll = document.createElement("button");
		buttonSendReadAll.textContent = "Запрос настроек";
		buttonSendReadAll.style.width = "40ch";
		buttonSendReadAll.style.marginBottom = "1ch";
		buttonSendReadAll.onclick = (e) =>
		{
			e.target.disabled = true;
			const en = () => { e.target.disabled = false; }
			sendJsonRPCRequest("zd.cmd.send_read_request", { names: fieldsToSendRead[key] }, (j) =>
			{

				j.forEach((v, i) => 
				{
					const fieldName = fieldsToSendRead[key][i];
					fieldValueTds[key][fieldName].textContent = v !== null ? v.formatted : null;
				});

				en();
			}, en);
		};


		details.appendChild(buttonSendReadAll);
		details.appendChild(document.createElement("br"));

		if (key === "СТАТУС ПРОШИВКИ") {
			const buttonTerminateZDUpdate = document.createElement("button");
			buttonTerminateZDUpdate.textContent = "Выйти из перепрошивки";

			buttonTerminateZDUpdate.onclick = (e) => 
			{
				sendJsonRPCRequest("zd.update.terminate");
			};

			details.appendChild(buttonTerminateZDUpdate);
			buttonTerminateZDUpdate.style.width = "40ch";
			buttonTerminateZDUpdate.style.marginBottom = "1ch";
		} else if (key === "НАСТРОЙКИ") {
			const [butOff, butOn] = createElemsArray("button", 2);

			butOff.textContent = "pause off";
			butOn.textContent = "pause on";

			butOff.onclick = e => {
				e.target.disabled = true;
				const en = () => e.target.disabled = false;
				sendJsonRPCRequest("zd.cmd.can.send_direct", {command: 0x20, data: [02, 00, 04, 00, 00, 00, 00, 00]}, en, en);
			};

			butOn.onclick = e => {
				e.target.disabled = true;
				const en = () => e.target.disabled = false;
				sendJsonRPCRequest("zd.cmd.can.send_direct", {command: 0x20, data: [02, 00, 0xB8, 0x0B, 00, 00, 00, 00]}, en, en);
			}

			details.appendChild(butOff);
			details.appendChild(butOn);
		} else if (key === "КАМЕРА") {
			const t = document.createElement("table");
			t.innerHTML = `
			<tr>
				<td>
					<table class="table-nobordered table-kv-pair table-equal-cells">
						<tbody>
							<tr>
								<td><button id="buttonReadFlgFPU">FlgFPU:</button></td>
								<td id="tdFlgFPU"></td>
								<td><input type="text" id="textboxFlgFPU" value="2" maxlength="10"></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table class="table-kv-pair table-equal-cells">
						<tbody>
							<tr>
								<td>
									Питание камеры
								</td>

								<td>
									<input type="checkbox" id="checkboxFlgFPU0">
								</td>
							</tr>

							<tr>
								<td>
									Мультиэкспозиция при съёмке
								</td>

								<td>
									<input type="checkbox" id="checkboxFlgFPU2">
								</td>
							</tr>

							<tr>
								<td>
									Передача служебных видеоданных
								</td>

								<td>
									<input type="checkbox" id="checkboxFlgFPU3" checked>
								</td>
							</tr>

							<tr>
								<td>
									Переключение между каналами с разными gain'ами
								</td>

								<td>
									<table class="table-kv-pair table-equal-cells table-nobordered">
										<tbody>
											<tr><td><label><input id="radioFlgFPU45_0" type="radio" name="radioFlgFPU45"></input>канал с высоким gain</label></td></tr>
											<tr><td><label><input id="radioFlgFPU45_1" type="radio" name="radioFlgFPU45"></input>канал с низким gain</label></td></tr>
											<tr><td><label><input id="radioFlgFPU45_2" type="radio" name="radioFlgFPU45"></input>HDR</label></td></tr>
											<tr><td><label><input id="radioFlgFPU45_3" type="radio" name="radioFlgFPU45"></input>смена канала от кадра к кадру</label></td></tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>

			</tr>

			<tr><td><button id="buttonApplyFlgFPU">Apply</button></td></tr>

			<tr>
				<td>
					<table class="table-nobordered table-kv-pair table-equal-cells">
						<tbody>
							<tr>
								<td><button id="buttonReadcontrol">Mh (control):</button></td>
								<td id="tdControl"></td>
								<td><input type="text" id="textboxControl" value="19592" maxlength="10"></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table class="table-kv-pair table-equal-cells">
						<tbody>
							<tr>
								<td>
									2-CMS
								</td>

								<td>
									<input type="checkbox" id="checkboxControl0">
								</td>
							</tr>

							<tr>
								<td>
									Режим training'а
								</td>

								<td>
									<input type="checkbox" id="checkboxControl1">
								</td>
							</tr>

							<tr>
								<td>
									ADC_gain
								</td>

								<td>
									<input type="text" id="textboxControl2_7">
								</td>
							</tr>

							<tr>
								<td>
									Направление считывания матрицы
								</td>

								<td>
									<input type="checkbox" id="checkboxControl8">
								</td>
							</tr>

							<tr>
								<td>
									PGA_gain_bot
								</td>

								<td>
									<input type="text" id="textboxControl9_14">
								</td>
							</tr>
						</tbody>
					</table>
				</td>

			</tr>

			<tr><td><button id="buttonApplyControl">Apply</button></td></tr>
			`;

			details.appendChild(t);

			elems = getAJAXElemsWithID();

			function updateButtonSetREState()
			{
				let isCorrect = true;
				const FlgFPU = fieldValues["КАМЕРА"]["FlgFPU"];

				elems.checkboxFlgFPU0.checked = (FlgFPU & (1 << 0)) === 0;
				elems.checkboxFlgFPU2.checked = (FlgFPU & (1 << 2)) !== 0;
				elems.checkboxFlgFPU3.checked = (FlgFPU & (1 << 3)) !== 0;

				elems.radioFlgFPU45_0.checked = ((FlgFPU >> 4) & 3) === 0;
				elems.radioFlgFPU45_1.checked = ((FlgFPU >> 4) & 3) === 1;
				elems.radioFlgFPU45_2.checked = ((FlgFPU >> 4) & 3) === 2;
				elems.radioFlgFPU45_3.checked = ((FlgFPU >> 4) & 3) === 3;

				isCorrect = checkNumberValue(FlgFPU, false);
				elems.buttonApplyFlgFPU.disabled = !isCorrect;
			}

			function updateFlgFPUValue()
			{
				elems.textboxFlgFPU.value = fieldValues["КАМЕРА"]["FlgFPU"].toString();
			}

			function FlgFPUSetBit(bitNo, value)
			{
				fieldValues["КАМЕРА"]["FlgFPU"] &= ~(1 << bitNo);
				fieldValues["КАМЕРА"]["FlgFPU"] |= (value ? 1 : 0) << bitNo;
				updateFlgFPUValue();
			}

		
			elems.checkboxFlgFPU0.onclick = (e) =>
			{
				FlgFPUSetBit(0, !e.target.checked);
			}
		

			elems.checkboxFlgFPU2.onclick = (e) =>
			{
				FlgFPUSetBit(2, e.target.checked);
			}
		
		
			elems.checkboxFlgFPU3.onclick = (e) =>
			{
				FlgFPUSetBit(3, e.target.checked);
			}


			elems.radioFlgFPU45_0.oninput = e => {
				if (!e.target.checked)
					return;
					
				fieldValues["КАМЕРА"]["FlgFPU"] &= ~0x30;
				updateFlgFPUValue();
				//fieldValues["КАМЕРА"]["FlgFPU"] |= 0 <<
			};


			elems.radioFlgFPU45_1.onclick = e => {
				if (!e.target.checked)
					return;

				fieldValues["КАМЕРА"]["FlgFPU"] &= ~0x30;
				fieldValues["КАМЕРА"]["FlgFPU"] |= 1 << 4;
				updateFlgFPUValue();
			};


			elems.radioFlgFPU45_2.onclick = e => {
				if (!e.target.checked)
					return;

				fieldValues["КАМЕРА"]["FlgFPU"] &= ~0x30;
				fieldValues["КАМЕРА"]["FlgFPU"] |= 2 << 4;
				updateFlgFPUValue();
			};


			elems.radioFlgFPU45_3.onclick = e => {
				if (!e.target.checked)
					return;

				fieldValues["КАМЕРА"]["FlgFPU"] &= ~0x30;
				fieldValues["КАМЕРА"]["FlgFPU"] |= 3 << 4;
				updateFlgFPUValue();
			};


			elems.textboxFlgFPU.oninput = e => {
				const value = textboxIntValueGuard(e.target, 2, false);
				fieldValues["КАМЕРА"]["FlgFPU"] = value;
				updateButtonSetREState();
			};

			const sendReadField = (onFlush = null, name, label) =>
			{
				const fieldValue = fieldValues["КАМЕРА"][name];

				if (!checkNumberValue(fieldValue, false))
					return;

				const batch = new JsonRPCBatch();
				batch.pushRequest("zd.cmd.send_write_request", { name, value: fieldValue });

				batch.pushRequest("zd.cmd.send_read_request", { name }, (j) => 
				{
					if (j !== null) {
						label.textContent = j.formatted;
					}
				});
				
				batch.flush(onFlush);
			};


			elems.buttonReadFlgFPU.onclick = e => {
				const en = () => e.target.disabled = false;
				e.target.disabled = true;
				sendJsonRPCRequest("zd.cmd.send_read_request", {name: "FlgFPU"}, j => {
					if (j !== null) {
						elems.tdFlgFPU.textContent = j.formatted;
					}

					en();
				}, en);
			};

			elems.buttonReadcontrol.onclick = e => {
				const en = () => e.target.disabled = false;
				e.target.disabled = true;
				sendJsonRPCRequest("zd.cmd.send_read_request", {name: "Mh"}, j => {
					if (j !== null) {
						elems.tdControl.textContent = j.formatted;
					}

					en();
				}, en);
			};


			buttonApplyFlgFPU.onclick = e => {
				e.target.disabled = true;
				sendReadField(() => { e.target.disabled = false; }, "FlgFPU", elems.tdFlgFPU); 
			}
		
			FlgFPUSetBit(0, !elems.checkboxFlgFPU0.checked);
			FlgFPUSetBit(2, elems.checkboxFlgFPU2.checked);
			FlgFPUSetBit(3, elems.checkboxFlgFPU3.checked);


			elems.textboxControl.oninput = e => {
				const value = textboxIntValueGuard(e.target, 2, false);
				fieldValues["КАМЕРА"]["Mh"] = value;
				elems.checkboxControl0.checked =  (value & (1 << 0)) !== 0;
				elems.checkboxControl1.checked =  (value & (1 << 1)) !== 0;
				elems.checkboxControl8.checked =  (value & (1 << 8)) !== 0;

				elems.textboxControl2_7.value = (value >> 2) & 63;
				elems.textboxControl9_14.value = (value >> 9) & 63;

				textboxIntValueGuard(elems.textboxControl2_7, 1, false, null, 63, 0);
				textboxIntValueGuard(elems.textboxControl9_14, 1, false, null, 63, 0);

				elems.buttonApplyControl.disabled = !checkNumberValue(value);
			};


			elems.textboxControl2_7.oninput = e => {
				const value = textboxIntValueGuard(e.target, 1, false, null, 63, 0);

				if (!checkNumberValue(value))
					return;
				
				fieldValues["КАМЕРА"]["Mh"] &= ~(63 << 2);
				fieldValues["КАМЕРА"]["Mh"] |= value << 2;
				elems.textboxControl.value = fieldValues["КАМЕРА"]["Mh"].toString();
			};


			elems.textboxControl9_14.oninput = e => {
				const value = textboxIntValueGuard(e.target, 1, false, null, 63, 0);

				if (!checkNumberValue(value))
					return;
				
				fieldValues["КАМЕРА"]["Mh"] &= ~(63 << 9);
				fieldValues["КАМЕРА"]["Mh"] |= value << 9;
				elems.textboxControl.value = fieldValues["КАМЕРА"]["Mh"].toString();
			};


			function ControlSetBit(bitNo, value)
			{
				fieldValues["КАМЕРА"]["Mh"] &= ~(1 << bitNo);
				fieldValues["КАМЕРА"]["Mh"] |= (value ? 1 : 0) << bitNo;
				elems.textboxControl.value = fieldValues["КАМЕРА"]["Mh"].toString();
			}

			elems.checkboxControl0.onclick = e => ControlSetBit(0, e.target.checked ? 1 : 0);
			elems.checkboxControl1.onclick = e => ControlSetBit(1, e.target.checked ? 1 : 0);
			elems.checkboxControl8.onclick = e => ControlSetBit(8, e.target.checked ? 1 : 0);


			elems.buttonApplyControl.onclick = e => {
				e.target.disabled = true;
				const en = () => e.target.disabled = false;

				sendJsonRPCRequest("zd.cmd.send_read_request", { name: "on_TLK" }, j => {
					sendJsonRPCRequest("zd.cmd.send_write_request", { name: "on_TLK", value: j.raw }, j => {
						sendReadField(en, "Mh", elems.tdControl); 
					}, en);
				}, en);
			};

			function updateButtonApplyControlState()
			{
				if (elems.textboxControl.oninput)
					elems.textboxControl.oninput({target: elems.textboxControl});
			}


			updateButtonApplyControlState();
		}

		details.appendChild(table);

		group.forEach((v) =>
		{
			const tr = document.createElement("tr");
			const tds = createElemsArray("td", 4);
			const buttonSendRead = document.createElement("button");
			
			buttonSendRead.textContent = namesMap[v] ?? v; // замена имени
			
			buttonSendRead.onclick = (e) =>
			{
				e.target.disabled = true;
				const en = () => { e.target.disabled = false; };
				
				sendJsonRPCRequest("zd.cmd.send_read_request", { "name": v }, (j) =>
				{
					if (j !== null) {
						tds[1].textContent = j.formatted;
					}
					
					en();
				}, en);
			};

			tds[0].appendChild(buttonSendRead);

			//tds[0].style.width = "100%";

			const textbox = document.createElement("input");
			textbox.type = "text";
			textbox.value = "0";
			textbox.style.float = "right";

			const sendReadField = (onFlush = null) =>
			{
				const fieldValue = fieldValues[key][v];

				if (!checkNumberValue(fieldValue, false))
					return;

				const batch = new JsonRPCBatch();
				
				batch.pushRequest("zd.cmd.send_write_request", { name: v, value: fieldValue });
				batch.pushRequest("zd.cmd.send_read_request", { name: v }, (j) => 
				{
					if (j !== null) {
						tds[1].textContent = j.formatted;
					}
				});
				
				batch.flush(onFlush);
			};

			textbox.onkeyup = (e) =>
			{
				if (e.key !== "Enter")
					return;
				
				const fieldValue = fieldValues[key][v];
	
				if (!checkNumberValue(fieldValue))
					return;

				sendReadField();
			};

			const buttonApply = document.createElement("button");
			buttonApply.textContent = "Apply"; // todo: другое название
			buttonApply.onclick = (e) => 
			{ 
				e.target.disabled = true;
				sendReadField(() => { e.target.disabled = false; }); 
			};

			const batch = new JsonRPCBatch();

			batch.pushRequest("zd.cmd.get_field_value", { name: v }, (j) =>
			{
				if (j === null || j.raw === null)
					return;

				textbox.value = j.formatted;
				tds[1].textContent = j.formatted;
				fieldValues[key][v] = j.raw;
			}, null, false);

			batch.pushRequest("zd.cmd.get_field_info", { name: v }, (j) => 
			{
				const textboxCheckValue = (elem) => 
				{
					fieldValues[key][v] = textboxIntValueGuard(elem, j.raw_size * 2, j.is_signed);
					buttonApply.disabled = !checkNumberValue(fieldValues[key][v]);
				};

				textboxCheckValue(textbox);

				textbox.oninput = (e) => { textboxCheckValue(e.target); };
				buttonSendRead.disabled = !j.readable;
				const sendReadGroup = fieldsToSendRead[key];

				if (j.readable) {
					sendReadGroup.push(v);
				} else {
					delete fieldValues[key][v];
				}

				textbox.disabled = !j.writable;
				buttonApply.disabled = !j.writable;
				buttonSendReadAll.disabled = sendReadGroup.length === 0;
			}, (j) =>
			{
				console.log("поля " + v + " не существует");
			}, false);
			
			batch.flush();
			fieldValueTds[key][v] = tds[1];
			tds[1].style.minWidth = "15ch";
			tds[2].style.minWidth = "15ch";
			tds[3].style.minWidth = "15ch";
			tds[1].style.width = "15ch";
			tds[2].style.width = "15ch";
			tds[3].style.width = "15ch";
			tds[2].appendChild(textbox);
			tds[3].appendChild(buttonApply);
			tds.forEach((td) => { tr.appendChild(td); });
			tbody.appendChild(tr);
		});
	}

	elems.buttonEnableVidVis.onclick = e => {
		const en = () => e.target.disabled = false;
		sendJsonRPCRequest("enableVidVis", null, en, en);
	};
}
	