function main()
{
	const elems = getAJAXElemsWithID();
	let deviceAddress = null;


	function updatebuttonApplyTxSettingsState()
	{
		elems.buttonApplyTxSettings.disabled = !checkNumberValue(deviceAddress);
	}

	sendJsonRPCRequest("zd.cmd.get_tx_receiver", null, j => {
		deviceAddress = j;
		elems.textboxDeviceAddress.value = deviceAddress ?? "";
		textboxIntValueGuard(elems.textboxDeviceAddress, 1);
		updatebuttonApplyTxSettingsState();
	});


	elems.textboxDeviceAddress.oninput = ({ currentTarget }) => {
		deviceAddress = textboxIntValueGuard(currentTarget, 1);
		updatebuttonApplyTxSettingsState();
	};
	
	elems.buttonApplyTxSettings.onclick = ({ currentTarget }) => { 
		const en = () => currentTarget.disabled = false;
		currentTarget.disabled = true;
		sendJsonRPCRequest("zd.cmd.set_tx_receiver", [deviceAddress], en, en);
	};

	elems.buttonClearCache.onclick = (e) => { sendJsonRPCRequest("clear_cache"); };
	
	
	elems.buttonSetFactorySettings.onclick = (e) => 
	{
		sendJsonRPCRequest("pilv.mko.set_factory_settings"); 
	};

	const getVersion = new AsyncLoopFunc(function()
	{
		sendJsonRPCRequest("zd.get_board_version", null, (j) =>
		{
			if (j !== null)
				elems.labelZDBoardVersion.textContent = sprintf("%02d.%02d.%d", j.year, j.month, j.version);

			this.continueTimeout(1000);
		});
	});

	const ipConfig = {
		ip: "",
		ipSplitted: [0, 0, 0, 0],
		subnetMask: 0,
		gateway: "",
		isDHCPEnabled: false
	};
	
	function updateButtonApplyIPConfigState()
	{
		let isCorrect = true;

		for (let i = 0; i < 4; i++) {
			if (!checkNumberValue(ipConfig.ipSplitted[i])) {
					isCorrect = false;
				break;
			}
		}

		isCorrect &= checkNumberValue(ipConfig.subnetMask);
		elems.buttonApplyIPConfig.disabled = !isCorrect && !ipConfig.isDHCPEnabled;
	}

	
	const ipTextboxes = (() =>
	{
		result = [];
		
		for (let i = 0; i < 4; i++) {
			const elem = elems[sprintf("textboxIP%d", i)];

			elem.oninput = e =>
			{
				if (!ipConfig.hasOwnProperty("ip"))
					return;

				ipConfig.ipSplitted[i] = textboxIntValueGuard(e.target, 1, false);
				updateButtonApplyIPConfigState();
			};

			result.push(elem);
		}

		return result;
	})();

	elems.textboxSubnetMask.oninput = e =>
	{
		ipConfig.subnetMask = textboxIntValueGuard(e.target, 1, false, null, 31, 0);
		updateButtonApplyIPConfigState();
	};


	elems.checkboxDHCP.oninput = e =>
	{
		if (!ipConfig.hasOwnProperty("isDHCPEnabled"))
			return;

		ipConfig.isDHCPEnabled = e.target.checked;
		elems.buttonApplyIPConfig.disabled = !ipConfig.isDHCPEnabled;

		for (let i = 0; i < 4; i++) {
			ipTextboxes[i].disabled = ipConfig.isDHCPEnabled;
			elems.textboxSubnetMask.disabled = ipConfig.isDHCPEnabled;
		}

		updateButtonApplyIPConfigState();
	};


	sendHttpGetRequest(`http://${location.host}:8999/__get_ep`, (response) => 
	{
		const j = JSON.parse(response);
		const hostname = j[1];
		ipConfig.ip = j[0].ip;
		ipConfig.isDHCPEnabled = j[0].isDHCPEnabled;
		ipConfig.subnetMask = j[0].subnetMask;
		ipConfig.gateway = j[0].gateway;
		ipConfig.name = j[0].name;
	
		elems.labelHostname.textContent = hostname;
		elems.labelIP.textContent = `${ipConfig.ip}/${ipConfig.subnetMask}`;
		elems.checkboxDHCP.checked = ipConfig.isDHCPEnabled;

		ipConfig.ip
			.split(".")
			.forEach((v, i) => 
			{ 
				ipTextboxes[i].value = v; 
				ipTextboxes[i].disabled = ipConfig.isDHCPEnabled;
				ipConfig.ipSplitted[i] = textboxIntValueGuard(ipTextboxes[i], 1); 
			});

		elems.textboxSubnetMask.value = ipConfig.subnetMask.toString();
		updateButtonApplyIPConfigState();
		//ipChecker.run();
	});

	elems.buttonApplyIPConfig.onclick = e =>
	{
		ipConfig.ip = sprintf("%d.%d.%d.%d", 
			ipConfig.ipSplitted[0], 
			ipConfig.ipSplitted[1], 
			ipConfig.ipSplitted[2], 
			ipConfig.ipSplitted[3]);

		disableConnectionErrorPopups();
		elems.buttonApplyIPConfig.disabled = true;

		sendHttpPostRequest(`http://${location.host}:8999/__set_ep`, JSON.stringify({ 
			ip: ipConfig.ip, 
			isDHCPEnabled: ipConfig.isDHCPEnabled,
			subnetMask: ipConfig.subnetMask,
			gateway: "0.0.0.0",
			name: ipConfig.name
		}), response =>
		{
			setTimeout(() => {
				enableConnectionErrorPopups();
				elems.buttonApplyIPConfig.disabled = false;
				AJAXLoadPage("kpa_settings.html");
			}, 5000);
		}, null, () =>
		{
			setTimeout(() => {
				elems.buttonApplyIPConfig.disabled = false;
				enableConnectionErrorPopups();
				AJAXLoadPage("kpa_settings.html");
			}, 5000);
		});
	};

	elems.fileIZN_KALIB.onchange = (e) =>
	{
		//let guardResult = inputFileMaxSizeGuard(e.target, kpaFileMaxSize);
		elems.buttonFileIZN_KALIB.disabled = false;
		//if (guardResult === null) 
		//	return;

		//elems.buttonFileIZN_KALIB.textContent = "Обновить";
	};

	elems.buttonFileIZN_KALIB.onclick = e => {
		if (elems.fileIZN_KALIB.files.length == 0) {
			Popup.showInformation("Ошибка!", "Файл не был выбран.");
			return;
		}

		var fr = new FileReader();
		fr.onloadend = (e) =>
		{
			if(e.target.readyState != FileReader.DONE) {
				switch (e.target.readyState) {
					case FileReader.EMPTY:
						Popup.showInformation("Ошибка!", "Выбран пустой файл.");
					break;
				}

				return;
			}

			sendJsonRPCRequest("izn.prepare_kalib_upload", null, (j) => 
			{
				sendHttpPostRequest(global.cmdEP, e.target.result, (r) =>
				{
					//sendJsonRPCRequest("zd.update.get_validation_status", null, (j) =>
					//{
					//	if (j) {
					//		onDeviceFileLoad();
					//	} else {
					//		Popup.showInformation("Ошибка обновления.", "Неизвестная ошибка...");
					//	}
					//});
					// todo: проверить, началось ли обновление...
				}, // !function(r)

				[new HttpHeader(j.http_header_name, j.http_header_value)], null, 

				(e) =>
				{
					//if (!e.lengthComputable || e.total == 0) 
					//	return;

					//var value = e.total / e.loaded;
					//setDeviceLoadingProgressSmooth(value);
				});
			});
		}; // !onloadend...

		let files = elems.fileIZN_KALIB.files;
		fr.readAsArrayBuffer(files[0]);
		showProgressDevice();
	};

	{
		const batch = new JsonRPCBatch();
		batch.pushRequest("is_kpa_log_available", null, j => elems.buttonDownloadKPALog.disabled = j);
		batch.flush();
	}


	elems.formDownloadIZN_KALIB.action = "KALIB.IZN";

	elems.formDownloadKPALog.action = `${global.cmdEP}/kpa.log`;
	getVersion.run();
}