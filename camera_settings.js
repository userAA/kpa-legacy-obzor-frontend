function main()
{
	const elems = getAJAXElemsWithID();
	const cameraSettings = new CameraSettings();

	const fieldNamesByTextboxIDs = (() => 
	{
		let result = {};

		for (let key in elems) {
			const prefix = "textbox";
	
			if (key.startsWith(prefix)) {
				result[key] = key.substring(prefix.length);
			}
		}

		return result;
	})();

	const textboxIDsByFieldNames = Object.entries(fieldNamesByTextboxIDs).reduce((a, [v, k]) => {
		a[k] = v
		return a;
	}, {});

	function updateFieldValues() {
		for (let key in fieldNamesByTextboxIDs) {
			const fieldName = fieldNamesByTextboxIDs[key];
			const elem = elems[key];
	
			cameraSettings[fieldName] = textboxIntValueGuard(elem, 2, false);
		}
	}


	updateFieldValues();


	function updateButtonSetREState()
	{
		let isCorrect = true;
		const FlgFPU = cameraSettings["FlgFPU"];

		elems.checkboxFlgFPU0.checked = (FlgFPU & (1 << 0)) === 0;
		elems.checkboxFlgFPU2.checked = (FlgFPU & (1 << 2)) !== 0;
		elems.checkboxFlgFPU3.checked = (FlgFPU & (1 << 3)) !== 0;

		isCorrect = checkNumberValue(FlgFPU, false)
			&& checkNumberValue(cameraSettings["Al"], false);

		elems.buttonSetRE.disabled = !isCorrect;
	}


	function updateButtonSendCameraSettingsState()
	{
		let isCorrect = true;

		for (let key in cameraSettings) {
			if (!checkNumberValue(cameraSettings[key], false)) {
				isCorrect = false;
				break;
			}
		}

		elems.buttonSendCameraSettings.disabled = !isCorrect;
	}


	function updateButtonSetExposureTimeState()
	{
		elems.buttonSetExposureTime.disabled = (!checkNumberValue(cameraSettings.Al, false) 
			|| !checkNumberValue(cameraSettings.Ah, false));
	}


	function updateButtonSetFramePeriodState()
	{
		elems.buttonSetFramePeriod.disabled = (!checkNumberValue(cameraSettings.Fl, false) 
			|| !checkNumberValue(cameraSettings.Fh, false));
	}

	
	function checkUint16Textbox(e)
	{
		const value = textboxIntValueGuard(e.target, 2, false);
		const fieldName = fieldNamesByTextboxIDs[e.target.id];
		cameraSettings[fieldName] = value;
		updateButtonSetREState();
		updateButtonSendCameraSettingsState();
		updateButtonSetExposureTimeState();
		updateButtonSetFramePeriodState();
	}

	for (let key in fieldNamesByTextboxIDs) {
		elems[key].oninput = checkUint16Textbox;
	}


	elems.buttonSetRE.onclick = (e) => 
	{
		Pilv.setRE(cameraSettings.FlgFPU, cameraSettings.Al);
	};


	elems.buttonSendCameraSettings.onclick = (e) =>
	{
		for (let key in cameraSettings) {
			if (!checkNumberValue(cameraSettings[key])) {
				return;
			}
		}

		Pilv.sendCameraSettings(cameraSettings);
	};


	elems.buttonSetExposureTime.onclick = (e) =>
	{
		if (!checkNumberValue(cameraSettings.Al) 
			|| !checkNumberValue(cameraSettings.Ah)) {
			return;
		}

		Pilv.setExposureTime(cameraSettings.Al, cameraSettings.Ah);
	};


	elems.buttonSetFramePeriod.onclick = (e) =>
	{
		if (!checkNumberValue(cameraSettings.Fl) 
			|| !checkNumberValue(cameraSettings.Fh)) {
			return;
		}
		
		Pilv.setFramePeriod(cameraSettings.Fl, cameraSettings.Fh);
	};


	function FlgFPUSetBit(bitNo, value)
	{
		cameraSettings["FlgFPU"] &= ~(1 << bitNo);
		cameraSettings["FlgFPU"] |= (value ? 1 : 0) << bitNo;
		elems.textboxFlgFPU.value = cameraSettings["FlgFPU"].toString();
	}


	elems.checkboxFlgFPU0.onclick = (e) =>
	{
		FlgFPUSetBit(0, !e.target.checked);
	}

	
	elems.checkboxFlgFPU2.onclick = (e) =>
	{
		FlgFPUSetBit(2, e.target.checked);
	}


	elems.checkboxFlgFPU3.onclick = (e) =>
	{
		FlgFPUSetBit(3, e.target.checked);
	}

	elems.buttonReadCameraSettings.onclick = (e) => {
		const fieldNames = Object.values(fieldNamesByTextboxIDs)

		e.target.disabled = true;

		en = () => e.target.disabled = false;

		sendJsonRPCRequest("zd.cmd.send_read_request", { names: fieldNames }, j => {
			en();
			
			j.forEach((v, i) => {
				const fieldName = fieldNames[i];
				const fieldValue = v.raw;
				elems[textboxIDsByFieldNames[fieldName]].value = fieldValue.toString();
				cameraSettings[fieldName] = textboxIntValueGuard(elems[textboxIDsByFieldNames[fieldName]], 2, false);
			});

			updateButtonSetREState();
			updateButtonSendCameraSettingsState();
			updateButtonSetExposureTimeState();
			updateButtonSetFramePeriodState();
		}, en);
	};


	FlgFPUSetBit(0, !elems.checkboxFlgFPU0.checked);
	FlgFPUSetBit(2, elems.checkboxFlgFPU2.checked);
	FlgFPUSetBit(3, elems.checkboxFlgFPU3.checked);

	updateButtonSetREState();
	updateButtonSendCameraSettingsState();
	updateButtonSetExposureTimeState();
	updateButtonSetFramePeriodState();
}